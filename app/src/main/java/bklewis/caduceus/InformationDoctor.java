package bklewis.caduceus;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class InformationDoctor extends ActionBarActivity implements View.OnClickListener {
    private Button ok;
    private Button delete;
    private Button edit;
    //variables to be displayed
    private String firstName = null;
    private String lastName = null;
    private String specialty = null;
    private String hospital = null;
    private String workPhone = null;
    private String personalPhone = null;
    private String fax = null;
    private String email = null;
    private String address = null;
    private String docNotes = null;
    private long id = -1l;
    Bundle b;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_doctor);
        getSupportActionBar().setTitle("Doctor Information");
        getSupportActionBar().setIcon(R.drawable.ic_launcher);
        b = getIntent().getExtras();
        firstName = b.getString("firstName");
        lastName = b.getString("lastName");
        specialty = b.getString("specialty");
        hospital = b.getString("hospital");
        workPhone = b.getString("workPhone");
        personalPhone = b.getString("personalPhone");
        fax = b.getString("fax");
        email = b.getString("email");
        address = b.getString("address");
        docNotes = b.getString("notes");
        id = b.getLong("id");
        String[] optionals = {specialty, hospital, workPhone, personalPhone, fax, email, address, docNotes};
        //get text views
        TextView nameF = (TextView)findViewById(R.id.name);
        if(firstName != null) {
            nameF.setText("Dr." + " " + firstName + " " + lastName);
        } else {
            nameF.setText("Dr. " + lastName);
        }
        //set buttons
        ok = (Button) findViewById(R.id.ok_button);
        ok.setOnClickListener(this);
        edit = (Button) findViewById(R.id.edit_button);
        edit.setOnClickListener(this);
        delete = (Button) findViewById(R.id.delete_button);
        delete.setOnClickListener(this);
        TextView one = (TextView)findViewById(R.id.text3);
        TextView two = (TextView)findViewById(R.id.text4);
        TextView three = (TextView)findViewById(R.id.text5);
        TextView four = (TextView)findViewById(R.id.text6);
        TextView five = (TextView)findViewById(R.id.text7);
        TextView six = (TextView)findViewById(R.id.text8);
        TextView seven = (TextView)findViewById(R.id.text9);
        TextView eight = (TextView)findViewById(R.id.text10);
        TextView[] ids = {one, two, three, four, five, six, seven ,eight};
        setOptionalEntries(ids, optionals);
    }

    public void setOptionalEntries(TextView[] ids, String[] optionals) {
        int j = 0;
        String text;
        for(int i = 0; i < optionals.length; i++) {
            if(optionals[i] != null && !(optionals[i].equals(""))) {
                TextView set = ids[j];
                set.setVisibility(View.VISIBLE);
                if(optionals[i].equals(workPhone)) {
                    set.setTextColor(Color.parseColor("#318CE7"));
                    set.setPaintFlags(set.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                    set.setText("Work Phone: " + optionals[i]);
                }
                else if(optionals[i].equals(personalPhone)) {
                    set.setTextColor(Color.parseColor("#318CE7"));
                    set.setPaintFlags(set.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                    set.setText("Personal Phone: "+ optionals[i]);
                } else if(optionals[i].equals(fax)) {
                    set.setText("Fax: "+ optionals[i]);
                } else if(optionals[i].equals(email)) {
                    set.setTextColor(Color.parseColor("#318CE7"));
                    set.setPaintFlags(set.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                    set.setText("Email: "+ optionals[i]);
                } else if(optionals[i].equals(address)) {
                    set.setText("Address: "+ optionals[i]);
                } else if(optionals[i].equals(specialty)) {
                    set.setText("Specialty: "+ optionals[i]);
                } else if(optionals[i].equals(hospital)) {
                    set.setText("Hospital: "+ optionals[i]);
                } else if(optionals[i].equals(docNotes)) {
                    set.setText("Notes: "+ optionals[i]);
                }
                if(optionals[i].equals(workPhone)) {
                    set.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            String uri = "tel:" + workPhone.trim() ;
                            Intent intent = new Intent(Intent.ACTION_CALL);
                            intent.setData(Uri.parse(uri));
                            startActivity(intent);
                            // TODO Auto-generated method stub

                        }
                    });
                }
                if(optionals[i].equals(personalPhone)) {
                    set.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            String uri = "tel:" + personalPhone.trim() ;
                            Intent intent = new Intent(Intent.ACTION_CALL);
                            intent.setData(Uri.parse(uri));
                            startActivity(intent);
                            // TODO Auto-generated method stub

                        }
                    });
                }
                if(optionals[i].equals(email)) {
                    set.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            Intent mailer = new Intent(Intent.ACTION_SEND);
                            mailer.setType("plain/text");
                            mailer.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
                            startActivity(Intent.createChooser(mailer, "Send email"));

                        }
                    });
                }
                j++;
            }
        }
        for(int c = j; c < ids.length; c++) {
            TextView set = ids[j];
            set.setVisibility(View.INVISIBLE);

        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_information_doctor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            //what occurs when ok button clicked
            case R.id.ok_button:
                //Intent iNew = new Intent(this, Doctors.class);
                //startActivity(iNew);
                finish();
                break;
            case R.id.edit_button:
                Intent iNew3 = new Intent(this, NewDoctor.class);
                b.putInt("type", 1);
                if(b != null) {
                    iNew3.putExtras(b);
                }
                startActivity(iNew3);
                finish();
                break;
            case R.id.delete_button:
                AlertDialog.Builder builder3 = new AlertDialog.Builder(InformationDoctor.this)
                        .setTitle("Are you sure you want to delete this doctor profile?")
                        .setMessage("This action is permanent.")

                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                DBdocAdapter dbd = MainActivity.dbd;
                                dbd.deleteDoc(id);
                                Intent iNew2 = new Intent(InformationDoctor.this, Doctors.class);
                                iNew2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(iNew2);
                                finish();

                            }
                        })
                        .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                builder3.show();
                break;

        }
    }
}
