package bklewis.caduceus;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi.DriveContentsResult;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFolder.DriveFileResult;
import com.google.android.gms.drive.MetadataChangeSet;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

/**
 * An activity to illustrate how to create a file.
 */
public class FileWriter extends BaseGoogleActivity {

    private static final String TAG = "CreateFileActivity";

    @Override
    public void onConnected(Bundle connectionHint) {
        super.onConnected(connectionHint);
        // create new contents resource
        Drive.DriveApi.newDriveContents(getGoogleApiClient())
                .setResultCallback(driveContentsCallback);
    }

    final private ResultCallback<DriveContentsResult> driveContentsCallback = new
            ResultCallback<DriveContentsResult>() {
                @Override
                public void onResult(DriveContentsResult result) {
                    if (!result.getStatus().isSuccess()) {
                        return;
                    }
                   Log.i("CAD", "MAKES IT HERE");
                    final DriveContents driveContents = result.getDriveContents();

                    // Perform I/O off the UI thread.
                    new Thread() {
                        @Override
                        public void run() {
                            // write content to DriveContents
                            OutputStream outputStream = driveContents.getOutputStream();
                            Writer writer = new OutputStreamWriter(outputStream);
                            try {
                                String header = "Name of Medicine, Profile, Description, Dosage, Prescriber, Taken At\n";
                                writer.write(header);
                                // Write the content
                                DBmedAdapter dbm = MainActivity.dbm;
                                final ArrayList<ItemMedicine> medicines;
                                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(FileWriter.this);
                                String profileName = "ALL";
                                long oldSelector = prefs.getLong("selectorId", -1l);
                                if (oldSelector == -1l) {
                                    medicines = dbm.getAllMeds();
                                } else {
                                    medicines = dbm.getMedsByProfile(oldSelector);
                                }
                                DBprofileAdapter dbp = MainActivity.dbp;
                                final ArrayList<ItemProfile> profiles = dbp.getAllProfiles();
                                String timeTaken;
                                String line;
                                for (int i = 0; i < medicines.size(); ++i) {
                                    ItemMedicine med = medicines.get(i);
                                    if(med.profileID != -1l) {
                                        for (ItemProfile profile : profiles) {
                                            if (med.profileID == profile.id) {
                                                profileName = profile.firstName;
                                            }
                                        }
                                    } else {
                                        profileName = "ALL";
                                    }
                                    if (med.timeTakenHour == 0 && med.timeTakenMinute == 0) {
                                        timeTaken = "";
                                    } else {
                                        if (med.timeTakenMinute == 0) {
                                            timeTaken = med.timeTakenHour + ":00";
                                        } else {
                                            timeTaken = med.timeTakenHour + ":" + med.timeTakenMinute;
                                        }
                                    }
                                    line=new String(med.title+","+ profileName + "," + med.description + "," + med.dosage + "," + med.prescriber + "," + timeTaken +"\n");
                                    writer.write(line);
                                }
                                writer.close();
                            } catch (IOException e) {
                                Log.e(TAG, e.getMessage());
                            }

                            Long tsLong = System.currentTimeMillis()/1000;
                            String ts = tsLong.toString();
                            MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
                                    .setTitle("CaduceusMedicines" + ts)
                                    .setMimeType("text/csv")
                                    .setStarred(true).build();

                            // create a file on root folder
                            Drive.DriveApi.getRootFolder(getGoogleApiClient())
                                    .createFile(getGoogleApiClient(), changeSet, driveContents)
                                    .setResultCallback(fileCallback);
                        }
                    }.start();
                }
            };

    final private ResultCallback<DriveFileResult> fileCallback = new
            ResultCallback<DriveFileResult>() {
                @Override
                public void onResult(DriveFileResult result) {
                   // Log.i("CAD", "MAKES IT HERE");
                    if (!result.getStatus().isSuccess()) {
                        Intent returnIntent = new Intent();
                        setResult(RESULT_CANCELED, returnIntent);
                        finish();
                    }
                    Intent returnIntent = new Intent();
                    setResult(RESULT_OK,returnIntent);
                    finish();
                }
            };


}