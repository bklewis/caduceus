package bklewis.caduceus;

/**
 * Created by axmangel on 3/31/15.
 */
public class ItemDoctor {
    protected long id = -1;
    protected String firstName;
    protected String lastName;
    protected String specialty;
    protected String phone1;
    protected String phone2;
    protected String email;
    protected String fax;
    protected String hospital;
    protected String address;
    protected String notes;

    public ItemDoctor(long id, String firstName, String lastName, String specialty, String phone1,  String phone2, String email, String fax, String hospital, String address, String notes) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.specialty = specialty;
        this.email = email;
        this.hospital = hospital;
        this.address = address;
        this.phone1 = phone1;
        this.phone2 = phone2;
        this.fax = fax;
        this.notes = notes;
    }

}
