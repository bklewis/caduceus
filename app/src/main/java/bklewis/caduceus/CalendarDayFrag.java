package bklewis.caduceus;

import android.content.Context;
import android.content.Intent;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by hp1 on 21-01-2015.
 */
//parts of code modified from WeekViewEvent (Almanak) https://github.com/alamkanak/Android-Week-View
public class CalendarDayFrag extends Fragment implements WeekView.MonthChangeListener,
        WeekView.EventClickListener, WeekView.EventLongPressListener{

    HashMap<WeekViewEvent, Long> weekViews = MainActivity.weekViews;
    DBeventAdapter dbe = MainActivity.dbe;
    MainActivity main;
    Context context;
    View view;

    private WeekView mWeekView;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.calendar_day_frag,container,false);
        main = (MainActivity) getActivity();
        context = main.getApplicationContext();

        mWeekView = (WeekView)view.findViewById(R.id.weekView);


        mWeekView.setOnEventClickListener(this);
        mWeekView.setMonthChangeListener(this);
        mWeekView.setEventLongPressListener(this);
        mWeekView.setNumberOfVisibleDays(1);

        // Lets change some dimensions to best fit the view.

        mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
        mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
        mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
        Calendar c = Calendar.getInstance();
        double hour = (double) c.get(Calendar.HOUR_OF_DAY);
        if(hour > 11) {
            mWeekView.goToHour(11);
        } else {
            mWeekView.goToHour(hour);
        }
        mWeekView.goToToday();
        return view;
    }




    //code from WeekViewEvent modified from https://github.com/alamkanak/Android-Week-View
    @Override
    public List<WeekViewEvent> onMonthChange(int newYear, int newMonth) {

        DBeventAdapter dbe = new DBeventAdapter(context);

        //List of Events
        ArrayList<ItemEvent> eventList;// = dbe.getAllEvents();

        long selectorId = main.getSelectorId();

        if (selectorId == -1L) {
            eventList = dbe.getAllEvents();
        } else {
            eventList = dbe.getEventsByProfile(selectorId);
        }

        // Populate the week view with some events.
        List<WeekViewEvent> events = new ArrayList<>();

        for(ItemEvent event : eventList){
            //Log.i("LOOP.EVENTS", event.id + " " + event.title + " " + event.day + " " + event.month + " " + event.year + " " + event.sHour + " " + event.eHour);
            if(newMonth-1 == event.month && newYear == event.year  ) {
                WeekViewEvent wvEvent = generateEvent(event);
                weekViews.put(wvEvent, event.id);
                    events.add(wvEvent);
            }
        }
        return events;
    }

    public WeekViewEvent generateEvent(ItemEvent theItem)  {
        Calendar startTime = Calendar.getInstance();
        Calendar endTime = (Calendar) startTime.clone();
        startTime.set(Calendar.HOUR_OF_DAY, theItem.sHour);
        startTime.set(Calendar.MINUTE, theItem.sMinute);
        startTime.set(Calendar.MONTH, theItem.month);
        startTime.set(Calendar.YEAR, theItem.year);
        startTime.set(Calendar.DAY_OF_MONTH, theItem.day);
        endTime.set(Calendar.HOUR_OF_DAY, theItem.eHour);
        endTime.set(Calendar.MINUTE, theItem.eMinute);
        endTime.set(Calendar.MONTH, theItem.month);
        endTime.set(Calendar.YEAR, theItem.year);
        endTime.set(Calendar.DAY_OF_MONTH, theItem.day);
        endTime.set(Calendar.MONTH, theItem.month);
        WeekViewEvent theEvent = new WeekViewEvent(1, theItem.title, startTime, endTime);
        if(theItem.color.equals("blue")) {
            theEvent.setColor(getResources().getColor(R.color.event_color_01));
        } else if (theItem.color.equals("pink")) {
            theEvent.setColor(getResources().getColor(R.color.event_color_02));
        } else if (theItem.color.equals("green")) {
            theEvent.setColor(getResources().getColor(R.color.event_color_03));
        } else {
            theEvent.setColor(getResources().getColor(R.color.event_color_04));
        }
        return theEvent;
    }

    private String getEventTitle(Calendar time) {
        return String.format("Event of %02d:%02d %s/%d", time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE), time.get(Calendar.MONTH)+1, time.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void onEventClick(WeekViewEvent wvEvent, RectF eventRect) {
        Intent i = new Intent(context, Information.class);
        Bundle b = new Bundle();
        //get event id
        long id = weekViews.get(wvEvent);
        ItemEvent event = dbe.readEvent(id);
        Toast.makeText(context, "Clicked " + event.title, Toast.LENGTH_SHORT).show();
        b.putString("name", event.title);
        b.putString("location", event.location);
        b.putString("notes", event.notes);
        b.putInt("month", event.month);
        b.putInt("year", event.year);
        b.putInt("day", event.day);
        b.putInt("sHour", event.sHour);
        b.putInt("eHour", event.eHour);
        b.putInt("sMinute", event.sMinute);
        b.putInt("eMinute", event.eMinute);
        b.putString("color", event.color);
        b.putLong("id", id);
        b.putLong("profileID", event.profileID);
        if(b != null) {
            i.putExtras(b);
        }
        startActivity(i);
    }

    @Override
    public void onEventLongPress(WeekViewEvent event, RectF eventRect) {
        long id = weekViews.get(event);
        ItemEvent events = dbe.readEvent(id);
        Toast.makeText(context, "Clicked " + events.title, Toast.LENGTH_SHORT).show();
    }

}