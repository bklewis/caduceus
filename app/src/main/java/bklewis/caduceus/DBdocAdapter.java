package bklewis.caduceus;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by bryce_000 on 4/2/2015.
 */
public class DBdocAdapter extends DBAdapter{

    protected static final String TAG = "DBdocAdapter";

    public DBdocAdapter(Context context){
        super(context);
    }

    // Create doctor
    public long createDoc(ItemDoctor doc){
        ContentValues args = new ContentValues();
        args.put(KEY_DOC_FIRSTNAME, doc.firstName);
        args.put(KEY_DOC_LASTNAME, doc.lastName);
        args.put(KEY_DOC_SPECIALTY, doc.specialty);
        args.put(KEY_DOC_PHONE1, doc.phone1);
        args.put(KEY_DOC_PHONE2, doc.phone2);
        args.put(KEY_DOC_EMAIL, doc.email);
        args.put(KEY_DOC_FAX, doc.fax);
        args.put(KEY_DOC_HOSPITAL, doc.hospital);
        args.put(KEY_DOC_ADDRESS, doc.address);
        args.put(KEY_DOC_NOTES, doc.notes);

        doc.id = db.insert(TABLE_DOCS, null, args);
        // "New doctor profile /"Dr. " + doc.lastName + "/" created"
        //Toast.makeText(context, "Doctor " + doc.lastName + " created, with id=" + doc.id, Toast.LENGTH_LONG).show();
        return doc.id;
    }

    // Read doctor
    public ItemDoctor readDoc(long rowID){
        ItemDoctor doc = null;
        try {
            Cursor c = db.query(TABLE_DOCS, KEYS_DOC, "_id = " + rowID, null, null, null, null, null);
            if (c == null) {
                //Toast.makeText(context, "readDoc Null Cursor Error: The doctor profile you tried to access could not be located in the database.", Toast.LENGTH_LONG).show();
                //Log.i(TAG, "readDoc Null Cursor Error: The doctor profile you tried to access could not be located in the database.");
            } else {
                c.moveToFirst();
                String firstName = c.getString(c.getColumnIndex(KEY_DOC_FIRSTNAME));
                String lastName = c.getString(c.getColumnIndex(KEY_DOC_LASTNAME));
                String specialty = c.getString(c.getColumnIndex(KEY_DOC_SPECIALTY));
                String phone1 = c.getString(c.getColumnIndex(KEY_DOC_PHONE1));
                String phone2 = c.getString(c.getColumnIndex(KEY_DOC_PHONE2));
                String email = c.getString(c.getColumnIndex(KEY_DOC_EMAIL));
                String fax = c.getString(c.getColumnIndex(KEY_DOC_FAX));
                String hospital = c.getString(c.getColumnIndex(KEY_DOC_HOSPITAL));
                String address = c.getString(c.getColumnIndex(KEY_DOC_ADDRESS));
                String notes = c.getString(c.getColumnIndex(KEY_DOC_NOTES));
                //long profileID = c.getLong(c.getColumnIndex(KEY_DOC_PROFILE_ID));

                //doc = new ItemDoctor(rowID, firstName, lastName, specialty, phone1, phone2, email, fax, hospital, address, notes);
                doc = new ItemDoctor(rowID, firstName, lastName, specialty, phone1,  phone2, email, fax, hospital, address, notes);
                //Toast.makeText(context, "Successfully accessed in readDoc doctor profile Dr. " + doc.lastName + " with id=" + doc.id, Toast.LENGTH_LONG).show();
                Log.i(TAG, "Successfully accessed in readDoc doctor profile Dr. " + doc.lastName + " with id=" + doc.id);
                c.close();
            }
        } catch(Exception e){
            //Toast.makeText(context, "Failed Cursor Retrieval in readDoc: The doctor you tried to access could not be located in the database.", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Failed Cursor Retrieval in readDoc: The doctor you tried to access could not be located in the database.");
        }
        return doc;
    }

    // Read doctor
    public ItemDoctor readDocFromCursor(Cursor c){
        ItemDoctor doc = null;
        if (c == null){
            //Toast.makeText(context, "readDocFromCursor Null Cursor Error: The doctor profile you tried to access could not be located in the database.", Toast.LENGTH_LONG).show();
            Log.i(TAG, "readDoc Null Cursor Error: The doctor profile you tried to access could not be located in the database.");
        }
        else {
            //c.moveToFirst();
            long id = c.getLong(c.getColumnIndex(KEY_ID));
            String firstName = c.getString(c.getColumnIndex(KEY_DOC_FIRSTNAME));
            String lastName = c.getString(c.getColumnIndex(KEY_DOC_LASTNAME));
            String specialty = c.getString(c.getColumnIndex(KEY_DOC_SPECIALTY));
            String phone1 = c.getString(c.getColumnIndex(KEY_DOC_PHONE1));
            String phone2 = c.getString(c.getColumnIndex(KEY_DOC_PHONE2));
            String email = c.getString(c.getColumnIndex(KEY_DOC_EMAIL));
            String fax = c.getString(c.getColumnIndex(KEY_DOC_FAX));
            String hospital = c.getString(c.getColumnIndex(KEY_DOC_HOSPITAL));
            String address = c.getString(c.getColumnIndex(KEY_DOC_ADDRESS));
            String notes = c.getString(c.getColumnIndex(KEY_DOC_NOTES));
            //long profileID = c.getLong(c.getColumnIndex(KEY_DOC_PROFILE_ID));

            doc = new ItemDoctor(id, firstName, lastName, specialty, phone1,  phone2, email, fax, hospital, address, notes);
            //Toast.makeText(context, "Successfully accessed in readDocFromCursor doctor profile Dr. " + doc.lastName + " with id=" + doc.id, Toast.LENGTH_LONG).show();
            Log.i(TAG, "Successfully accessed in readDocFromProfile doctor profile Dr. " + doc.lastName + " with id=" + doc.id);
        }
        return doc;
    }

    // Update doctor
    public boolean updateDoc(ItemDoctor doc) {
        long rowId = doc.id;
        Toast.makeText(context, "You just created a new doctor profile Dr. " + doc.id, Toast.LENGTH_LONG).show();
        boolean updated = false;
        if (rowId == -1) {
            // create a new doctor from argument
            createDoc(doc);
            //Toast.makeText(context, "You just created a new doctor profile Dr. " + doc.lastName + " with id " + doc.id + " rather than updating a current doctor profile!", Toast.LENGTH_LONG).show();
        } else {
            ContentValues args = new ContentValues();
            args.put(KEY_DOC_FIRSTNAME, doc.firstName);
            args.put(KEY_DOC_LASTNAME, doc.lastName);
            args.put(KEY_DOC_SPECIALTY, doc.specialty);
            args.put(KEY_DOC_PHONE1, doc.phone1);
            args.put(KEY_DOC_PHONE2, doc.phone2);
            args.put(KEY_DOC_EMAIL, doc.email);
            args.put(KEY_DOC_FAX, doc.fax);
            args.put(KEY_DOC_HOSPITAL, doc.hospital);
            args.put(KEY_DOC_ADDRESS, doc.address);
            args.put(KEY_DOC_NOTES, doc.notes);

            updated = db.update(TABLE_DOCS, args, KEY_ID + "=" + rowId, null) != 0;
        }
        if (updated){
            //Toast.makeText(context, "Doctor profile Dr. "+doc.lastName+" updated successfully", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Doctor profile Dr. "+doc.lastName+" updated successfully");
            return true;
        } else {
            //Toast.makeText(context, "Doctor profile Dr. "+doc.lastName+ " with id =" + doc.id + " update unsuccessful.", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Dr. "+doc.lastName+ "with id =" + doc.id + " update unsuccessful.");
            return false;
        }
    }

    // Delete doctor
    public boolean deleteDoc(ItemDoctor doc){
        boolean b = db.delete(TABLE_DOCS, KEY_ID + "=" + doc.id, null) > 0;
        if (b){
            //Toast.makeText(context, "Doctor profile Dr. " + doc.lastName + " with id=" + doc.id + " deleted successfully", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Doctor profile Dr. " + doc.lastName + " with id=" + doc.id + " deleted successfully");
        } else {
            //Toast.makeText(context, "Doctor delete unsuccessful.  We apologize for the issue", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Doctor profile Dr. " + doc.lastName + " with id=" + doc.id + " delete unsuccessful");

        }
        return b;
    }

    public boolean deleteDoc(long rowID){
        ItemDoctor doc = readDoc(rowID);
        boolean b = db.delete(TABLE_DOCS, KEY_ID + "=" + rowID, null) > 0;
        if (b){
            //Toast.makeText(context, "Doctor profile Dr. " + doc.lastName + " with id=" + doc.id + " deleted successfully", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Doctor profile Dr. " + doc.lastName + " with id=" + doc.id + " deleted successfully");
        } else {
            //Toast.makeText(context, "Doctor delete unsuccessful.  We apologize for the issue", Toast.LENGTH_LONG).show();
            //Log.i(TAG, "Doctor profile Dr. " + doc.lastName + " with id=" + doc.id + " delete unsuccessful");

        }
        return b;
    }

    public ArrayList<ItemDoctor> getAllDocs() {
        ArrayList<ItemDoctor> list = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_DOCS;
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);
            try {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        ItemDoctor d = readDocFromCursor(cursor);
                        list.add(d);
                    } while (cursor.moveToNext());
                }
            } finally {
                try { cursor.close(); } catch (Exception ignore) {}
            }
        } catch(Exception e){
            //Toast.makeText(context, "There are currently no docs to be loaded from the database.", Toast.LENGTH_SHORT).show();
        }
        return list;
    }


}
