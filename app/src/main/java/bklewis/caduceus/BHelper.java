package bklewis.caduceus;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by bryce_000 on 3/16/2015.
 */

//SQLiteAssetHelper
//SQLiteBrowser

public class BHelper /*extends SQLiteOpenHelper*/{
    /*
    // Tag string for log output
    protected static final String TAG = "DBHelper";

    // Database information
    protected static final String DATABASE_NAME = "CaduceusDB";
    protected static final int DATABASE_VERSION = 1;

    // -------------------- EVENTS TABLE --------------------
    protected static final String TABLE_EVENTS = "events";
    protected static final String KEY_EVENT_ID = "_id";
    protected static final String KEY_EVENT_TITLE = "title";
    protected static final String KEY_EVENT_COLOR = "color";
    protected static final String KEY_EVENT_APT = "appointment";
    protected static final String KEY_EVENT_DOC_ID = "doctor_id";
    protected static final String KEY_EVENT_DATE = "date";
    protected static final String KEY_EVENT_TIME_START = "start_time";
    protected static final String KEY_EVENT_TIME_END = "end_time";
    //protected static final String KEY_EVENT_PROFILE_ID = "profile_id";
    protected static final String KEY_EVENT_LOCATION = "location";
    protected static final String KEY_EVENT_NOTES = "notes";

    // -------------------- PROFILE TABLE --------------------
    protected static final String TABLE_PROFILES = "profiles";
    protected static final String KEY_PROFILE_ID = "_id";
    protected static final String KEY_PROFILE_FIRSTNAME = "first";
    protected static final String KEY_PROFILE_LASTNAME = "last";
    protected static final String KEY_PROFILE_BIRTHDATE = "birthdate";
    protected static final String KEY_PROFILE_HEIGHT = "height";
    protected static final String KEY_PROFILE_WEIGHT = "weight";
    protected static final String KEY_PROFILE_NOTES = "notes";
    protected static final String KEY_PROFILE_INSURANCE_COMPANY = "insurance_company";
    protected static final String KEY_PROFILE_INSURANCE_POLICY_NUMBER = "policy_number";
    protected static final String KEY_PROFILE_INSURANCE_PHONE = "insurance_phone";
    protected static final String KEY_PROFILE_EVENTS = "events";
    protected static final String KEY_PROFILE_MEDS = "meds";
    protected static final String KEY_PROFILE_VACCINES = "vaccines";
    protected static final String KEY_PROFILE_DOCS = "docs";

    // -------------------- VACCINES TABLE --------------------
    protected static final String TABLE_VACCINES = "vaccines";
    protected static final String KEY_VACCINE_ID = "_id";
    protected static final String KEY_VACCINE_DATE = "date";
    protected static final String KEY_VACCINE_EXPIRES = "expires";
    protected static final String KEY_VACCINE_DOSAGE = "dosage";
    protected static final String KEY_VACCINE_NOTES = "notes";

    // -------------------- MEDS TABLE --------------------
    protected static final String TABLE_MEDS = "medications";
    protected static final String KEY_MED_ID = "_id";
    protected static final String KEY_MED_TITLE = "title";
    protected static final String KEY_MED_DOSAGE = "dosage";
    protected static final String KEY_MED_REFILLS_LEFT = "refills_left";
    protected static final String KEY_MED_REFILL_NEXT = "next_refill";
    protected static final String KEY_MED_REFILL_NOTIF = "notification_refill";
    protected static final String KEY_MED_PRESCRIBER = "prescriber";
    protected static final String KEY_MED_DESCRIPTION = "description";
    protected static final String KEY_MED_NOTES = "notes";
    protected static final String KEY_MED_TIME_TAKEN = "time_taken";
    protected static final String KEY_MED_TIME_NOTIF = "notification_time";

    // -------------------- DOCS TABLE --------------------
    protected static final String TABLE_DOCS = "doctors";
    protected static final String KEY_DOC_ID = "_id";
    protected static final String KEY_DOC_FIRSTNAME = "first_name";
    protected static final String KEY_DOC_LASTNAME = "last_name";
    protected static final String KEY_DOC_SPECIALTY = "specialty";
    protected static final String KEY_DOC_PHONE1 = "primary_phone";
    protected static final String KEY_DOC_PHONE2 = "secondary_phone";
    protected static final String KEY_DOC_EMAIL = "email";
    protected static final String KEY_DOC_FAX = "fax";
    protected static final String KEY_DOC_HOSPITAL = "hospital";
    protected static final String KEY_DOC_ADDRESS = "address";
    protected static final String KEY_DOC_NOTES = "notes";

    // --------------- SQL statement to create EVENTS table ---------------
    protected static final String SQL_CREATE_TABLE_EVENTS =
            "CREATE TABLE " + TABLE_EVENTS + "("
                    + KEY_EVENT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + KEY_EVENT_TITLE + " TEXT NOT NULL, "
                    + KEY_EVENT_COLOR + " TEXT NOT NULL, "
                    + KEY_EVENT_APT + " INTEGER, "
                    + KEY_EVENT_DOC_ID + " INTEGER, "
                    + KEY_EVENT_DATE + " TEXT NOT NULL, "
                    + KEY_EVENT_TIME_START + " TEXT NOT NULL, "
                    + KEY_EVENT_TIME_END + " TEXT NOT NULL, "
                    //+ KEY_EVENT_PROFILE_ID + " INTEGER NOT NULL, "
                    + KEY_EVENT_LOCATION + " TEXT, "
                    + KEY_EVENT_NOTES + " TEXT "
                    +");";

    // --------------- SQL statement to create PROFILE table ---------------
    protected static final String SQL_CREATE_TABLE_PROFILES =
            "CREATE TABLE " + TABLE_PROFILES + "("
                    + KEY_PROFILE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + KEY_PROFILE_FIRSTNAME + " TEXT NOT NULL, "
                    + KEY_PROFILE_LASTNAME + " TEXT, "
                    + KEY_PROFILE_BIRTHDATE + "TEXT, "
                    + KEY_PROFILE_HEIGHT + " TEXT, "
                    + KEY_PROFILE_WEIGHT + " TEXT, "
                    + KEY_PROFILE_NOTES + " TEXT, "
                    + KEY_PROFILE_INSURANCE_COMPANY + " TEXT, "
                    + KEY_PROFILE_INSURANCE_POLICY_NUMBER + " TEXT, "
                    + KEY_PROFILE_INSURANCE_PHONE + " TEXT, "
                    + KEY_PROFILE_EVENTS + " TEXT, "
                    + KEY_PROFILE_MEDS + " TEXT, "
                    + KEY_PROFILE_VACCINES + " TEXT, "
                    + KEY_PROFILE_DOCS + " TEXT "
                    +");";

    // --------------- SQL statement to create VACCINES table ---------------
    protected static final String SQL_CREATE_TABLE_VACCINES =
            "CREATE TABLE " + TABLE_VACCINES + "("
                    + KEY_EVENT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + KEY_EVENT_TITLE + "TEXT NOT NULL, "
                    + KEY_EVENT_COLOR + "TEXT NOT NULL, "
                    + KEY_EVENT_APT + "INTEGER, "
                    + KEY_EVENT_DOC_ID + "INTEGER, "
                    + KEY_EVENT_DATE + "TEXT NOT NULL, "
                    + KEY_EVENT_TIME_START + "TEXT NOT NULL, "
                    + KEY_EVENT_TIME_END + " TEXT NOT NULL, "
                    //+ KEY_EVENT_PROFILE_ID + "INTEGER NOT NULL, "
                    + KEY_EVENT_LOCATION + "TEXT, "
                    + KEY_EVENT_NOTES + "TEXT "
                    +");";

    // --------------- SQL statement to create MEDS table ---------------
    protected static final String SQL_CREATE_TABLE_MEDS =
            "CREATE TABLE " + TABLE_MEDS + "("
                    + KEY_EVENT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + KEY_EVENT_TITLE + "TEXT NOT NULL, "
                    + KEY_EVENT_COLOR + "TEXT NOT NULL, "
                    + KEY_EVENT_APT + "INTEGER, "
                    + KEY_EVENT_DOC_ID + "INTEGER, "
                    + KEY_EVENT_DATE + "TEXT NOT NULL, "
                    + KEY_EVENT_TIME_START + "TEXT NOT NULL, "
                    + KEY_EVENT_TIME_END + " TEXT NOT NULL, "
                    //+ KEY_EVENT_PROFILE_ID + "INTEGER NOT NULL, "
                    + KEY_EVENT_LOCATION + "TEXT, "
                    + KEY_EVENT_NOTES + "TEXT "
                    +");";

    // --------------- SQL statement to create DOCTORS table ---------------
    protected static final String SQL_CREATE_TABLE_DOCTORS =
            "CREATE TABLE " + TABLE_DOCS + "("
                    + KEY_EVENT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + KEY_EVENT_TITLE + "TEXT NOT NULL, "
                    + KEY_EVENT_COLOR + "TEXT NOT NULL, "
                    + KEY_EVENT_APT + "INTEGER, "
                    + KEY_EVENT_DOC_ID + "INTEGER, "
                    + KEY_EVENT_DATE + "TEXT NOT NULL, "
                    + KEY_EVENT_TIME_START + "TEXT NOT NULL, "
                    + KEY_EVENT_TIME_END + " TEXT NOT NULL, "
                    //+ KEY_EVENT_PROFILE_ID + "INTEGER NOT NULL, "
                    + KEY_EVENT_LOCATION + "TEXT, "
                    + KEY_EVENT_NOTES + "TEXT "
                    +");";

    public DBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        // TODO: Add all necessary tables below
        db.execSQL(SQL_CREATE_TABLE_EVENTS);
        db.execSQL(SQL_CREATE_TABLE_PROFILES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        Log.w(TAG, "Upgrading the database from " + oldVersion + " to " + newVersion);

        // TODO: move data from oldVersion to newVersion

        //clear all data
        // TODO: add in other tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROFILES);

        //recreate tables
        onCreate(db);
    }

    public DBHelper(Context context, String name, CursorFactory factory, int version){
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }


*/
}
