package bklewis.caduceus;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by bryce_000 on 4/2/2015.
 */
public class DBmedAdapter extends DBAdapter {

    protected static final String TAG = "DBmedAdapter";

    public DBmedAdapter(Context context){
        super(context);
    }

    // Create med
    public long createMed(ItemMedicine med){
        ContentValues args = new ContentValues();
        args.put(KEY_MED_TITLE, med.title);
        args.put(KEY_MED_DOSAGE, med.dosage);
        args.put(KEY_MED_TIME_TAKEN_HOUR, med.timeTakenHour);
        args.put(KEY_MED_TIME_TAKEN_MINUTE, med.timeTakenMinute);
        args.put(KEY_MED_TIME_NOTIF, med.notificationTime);
        args.put(KEY_MED_REFILLS_LEFT, med.refillsLeft);
        //args.put(KEY_MED_REFILL_NEXT_DAY, med.);
        //args.put(KEY_MED_REFILL_NEXT_MONTH, med.eHour);
        //args.put(KEY_MED_REFILL_NEXT_YEAR, med.eMinute);
        //args.put(KEY_MED_REFILL_NOTIF, med.location);
        args.put(KEY_MED_PRESCRIBER, med.prescriber);
        args.put(KEY_MED_DESCRIPTION, med.description);
        args.put(KEY_MED_NOTES, med.notes);
        args.put(KEY_MED_PROFILE_ID, med.profileID);

        med.id = db.insert(TABLE_MEDS, null, args);
        // "New medicine /"" + med.title + "/" created"
        //Toast.makeText(context, "Medicine " + med.title + " created", Toast.LENGTH_LONG).show();
        return med.id;
    }

    // Read med
    public ItemMedicine readMed(long rowID){
        ItemMedicine med = null;
        try {
            Cursor c = db.query(TABLE_MEDS, KEYS_MED, "_id = " + rowID, null, null, null, null, null);
            if (c == null) {
                //Toast.makeText(context, "readMed Null Cursor Error: The medicine you tried to access could not be located in the database.", Toast.LENGTH_LONG).show();
                Log.i(TAG, "readMed Null Cursor Error: The medicine you tried to access could not be located in the database.");
            } else {
                c.moveToFirst();
                String title = c.getString(c.getColumnIndex(KEY_MED_TITLE));
                String dosage = c.getString(c.getColumnIndex(KEY_MED_DOSAGE));
                int timeTakenHour = c.getInt(c.getColumnIndex(KEY_MED_TIME_TAKEN_HOUR));
                int timeTakenMinute = c.getInt(c.getColumnIndex(KEY_MED_TIME_TAKEN_MINUTE));
                int timeTakenNotif = c.getInt(c.getColumnIndex(KEY_MED_TIME_NOTIF));
                String refillsLeft = c.getString(c.getColumnIndex(KEY_MED_REFILLS_LEFT));
                String prescriber = c.getString(c.getColumnIndex(KEY_MED_PRESCRIBER));
                String description = c.getString(c.getColumnIndex(KEY_MED_DESCRIPTION));
                String notes = c.getString(c.getColumnIndex(KEY_MED_NOTES));
                long profileID = c.getLong(c.getColumnIndex(KEY_MED_PROFILE_ID));

                med = new ItemMedicine(rowID, title, dosage, refillsLeft, timeTakenHour, timeTakenMinute, timeTakenNotif, prescriber, description, notes, profileID);
                //Toast.makeText(context, "Successfully accessed in readMed medicine=" + med.title + " with id=" + med.id, Toast.LENGTH_LONG).show();
                Log.i(TAG, "Successfully accessed in readMed medicine=" + med.title + " with id=" + med.id);
                c.close();
            }
        } catch(Exception e){
            //Toast.makeText(context, "Failed Cursor Retrieval in readMed: The medicine you tried to access could not be located in the database.", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Failed Cursor Retrieval in readMed: The medicine you tried to access could not be located in the database.");
        }
        return med;
    }

    // Read med
    public ItemMedicine readMedFromCursor(Cursor c){
        ItemMedicine med = null;
        if (c == null){
            //Toast.makeText(context, "readMedFromCursor Null Cursor Error: The medicine you tried to access could not be located in the database.", Toast.LENGTH_LONG).show();
            Log.i(TAG, "readMed Null Cursor Error: The medicine you tried to access could not be located in the database.");
        }
        else {
            //c.moveToFirst();
            long id = c.getLong(c.getColumnIndex(KEY_ID));
            String title = c.getString(c.getColumnIndex(KEY_MED_TITLE));
            String dosage = c.getString(c.getColumnIndex(KEY_MED_DOSAGE));
            int timeTakenHour = c.getInt(c.getColumnIndex(KEY_MED_TIME_TAKEN_HOUR));
            int timeTakenMinute = c.getInt(c.getColumnIndex(KEY_MED_TIME_TAKEN_MINUTE));
            int timeTakenNotif = c.getInt(c.getColumnIndex(KEY_MED_TIME_NOTIF));
            String refillsLeft = c.getString(c.getColumnIndex(KEY_MED_REFILLS_LEFT));
            String prescriber = c.getString(c.getColumnIndex(KEY_MED_PRESCRIBER));
            String description = c.getString(c.getColumnIndex(KEY_MED_DESCRIPTION));
            String notes = c.getString(c.getColumnIndex(KEY_MED_NOTES));
            long profileID = c.getLong(c.getColumnIndex(KEY_MED_PROFILE_ID));

            med = new ItemMedicine(id, title, dosage, refillsLeft, timeTakenHour, timeTakenMinute, timeTakenNotif, prescriber, description, notes, profileID);
            //Toast.makeText(context, "readMedFromCursor: Successfully accessed medicine " + med.title + " with id=" + med.id + "!", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Successfully accessed in readMedFromCursor medicine=" + med.title + " with id=" + med.id);
        }
        return med;
    }

    // Update med
    public boolean updateMed(ItemMedicine med) {
        long rowId = med.id;
        boolean updated = false;
        if (rowId == -1) {
            // create a new med from that med
            createMed(med);
            //Toast.makeText(context, "You just created a new medicine " + med.title + " with id " + med.id + " rather than updating a current medicine!", Toast.LENGTH_LONG).show();
        } else {
            ContentValues args = new ContentValues();
            args.put(KEY_MED_TITLE, med.title);
            args.put(KEY_MED_DOSAGE, med.dosage);
            args.put(KEY_MED_TIME_TAKEN_HOUR, med.timeTakenHour);
            args.put(KEY_MED_TIME_TAKEN_MINUTE, med.timeTakenMinute);
            args.put(KEY_MED_TIME_NOTIF, med.notificationTime);
            args.put(KEY_MED_REFILLS_LEFT, med.refillsLeft);
            //args.put(KEY_MED_REFILL_NEXT_DAY, med.);
            //args.put(KEY_MED_REFILL_NEXT_MONTH, med.eHour);
            //args.put(KEY_MED_REFILL_NEXT_YEAR, med.eMinute);
            //args.put(KEY_MED_REFILL_NOTIF, med.location);
            args.put(KEY_MED_PRESCRIBER, med.prescriber);
            args.put(KEY_MED_DESCRIPTION, med.description);
            args.put(KEY_MED_NOTES, med.notes);
            args.put(KEY_MED_PROFILE_ID, med.profileID);

            updated = db.update(TABLE_MEDS, args, KEY_ID + "=" + rowId, null) != 0;
        }
        if (updated){
            //Toast.makeText(context, "Medicine "+med.title+" updated successfully", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Medicine "+med.title+" updated successfully");
            return true;
        } else {
            //Toast.makeText(context, "Medicine "+med.title+" update unsuccessful.", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Medicine "+med.title+" update unsuccessful.");
            return false;
        }
    }

    // Delete med
    public boolean deleteMed(ItemMedicine med){
        //return db.delete(TABLE_MEDS, KEY_ID + "=" + med.id, null) > 0;
        boolean b = db.delete(TABLE_MEDS, KEY_ID + "=" + med.id, null) > 0;
        if (b){
            //Toast.makeText(context, "Medicine " + med.title + " deleted successfully", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Medicine " + med.title + " with id=" + med.id + " deleted successfully");
        } else {
            //Toast.makeText(context, "Medicine delete unsuccessful.  We apologize for the issue", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Medicine " + med.title + " with id=" + med.id + " delete unsuccessful");

        }
        return b;
    }

    public boolean deleteMed(long rowID){
        //return db.delete(TABLE_MEDS, KEY_ID + "=" + med.id, null) > 0;
        ItemMedicine med = readMed(rowID);
        boolean b = db.delete(TABLE_MEDS, KEY_ID + "=" + rowID, null) > 0;
        if (b){
            //Toast.makeText(context, "Medicine " + med.title + " deleted successfully", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Medicine " + med.title + " with id=" + med.id + " deleted successfully");
        } else {
            //Toast.makeText(context, "Medicine delete unsuccessful.  We apologize for the issue.", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Medicine " + med.title + " with id=" + med.id + " delete unsuccessful");
        }
        return b;
    }

    public ArrayList<ItemMedicine> getAllMeds() {
        ArrayList<ItemMedicine> list = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_MEDS;

        //SQLiteDatabase db = this.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);
            try {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        ItemMedicine m = readMedFromCursor(cursor);
                        list.add(m);
                    } while (cursor.moveToNext());
                }
            } finally {
                try { cursor.close(); } catch (Exception ignore) {}
            }

        } catch(Exception e){
            Toast.makeText(context, "There are currently no medicines to be loaded from the database.", Toast.LENGTH_SHORT).show();
        }
        //finally {    try { db.close(); } catch (Exception ignore) {}}
        return list;
    }

    public ArrayList<ItemMedicine> getMedsByProfile(long profileID) {
        ArrayList<ItemMedicine> list = new ArrayList<>();

        // Select All Query
        //String selectQuery = "SELECT  * FROM " + TABLE_MEDS + "WHERE profile = " + profileID;
        String[] args = new String[] {Long.toString(profileID)};

        try {
            //Cursor cursor = db.rawQuery(selectQuery, null);
            Cursor cursor = db.query(TABLE_MEDS, KEYS_MED, "profile = ?", args, null, null, null);
            try {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        ItemMedicine m = readMedFromCursor(cursor);
                        list.add(m);
                    } while (cursor.moveToNext());
                }
            } finally {
                try { cursor.close(); } catch (Exception ignore) {}
            }

        } catch(Exception e){
           // Toast.makeText(context, "There are no events to be loaded from the database currently.", Toast.LENGTH_SHORT).show();
        }
        //finally {    try { db.close(); } catch (Exception ignore) {}}
        return list;
    }
}
