package bklewis.caduceus;

import android.util.Log;

import java.text.DateFormatSymbols;
import java.util.Comparator;
import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by bryce_000 on 3/28/2015.
 */
public class ItemEvent implements Comparable<ItemEvent>{

    protected long id = -1L;
    protected String title;
    protected String color;
    //protected int apt;
    //protected int doc;
    protected int month = -1;
    protected int day = -1;
    protected int year = -1;
    protected int sMinute = -1;
    protected int sHour = -1;
    protected int eMinute = -1;
    protected int eHour = -1;
    protected String location = null;
    protected String notes = null;
    protected long profileID = -1L;
    protected Calendar calendar;
    protected String weekDay = null;

    protected static String[] dayz = {"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"};
    protected static String[] days = {"TUE", "WED", "THU", "FRI", "SAT", "SUN", "MON"};


    public ItemEvent(long id, String title, String color, int year, int month, int day, int sHour, int sMinute, int eHour, int eMinute, String location, String notes, long profileID){
        this.id = id;
        this.title = title;
        this.color = color;
        //this.apt = apt;
        //this.doc = doc;
        this.month = month;
        this.day = day;
        this.year = year;
        this.sHour = sHour;
        this.eHour = eHour;
        this.eMinute = eMinute;
        this.sMinute = sMinute;
        this.location = location;
        this.notes = notes;
        this.profileID = profileID;

        calendar = new GregorianCalendar();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, sHour);
        calendar.set(Calendar.MINUTE, sMinute);

        //int wDay = calendar.get(Calendar.DAY_OF_WEEK);
        //this.weekDay = days[wDay];

        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        this.weekDay = new DateFormatSymbols().getShortWeekdays()[dayOfWeek];

        //Log.i(Integer.toString(this.day), this.weekDay);

    }

    @Override
    public int compareTo(ItemEvent event) {
        return calendar.compareTo(event.calendar);
    }

    /*Comparator for sorting the list by Student Name
    public static Comparator<ItemEvent> StuNameComparator = new Comparator<ItemEvent>() {

        public int compare(Student s1, Student s2) {
            String StudentName1 = s1.getStudentname().toUpperCase();
            String StudentName2 = s2.getStudentname().toUpperCase();

            //ascending order
            return StudentName1.compareTo(StudentName2);

            //descending order
            //return StudentName2.compareTo(StudentName1);
        }};

    //Comparator for sorting the list by roll no
    public static Comparator<Student> StuRollno = new Comparator<Student>() {

        public int compare(Student s1, Student s2) {

            int rollno1 = s1.getRollno();
            int rollno2 = s2.getRollno();

	   //For ascending order
            return rollno1-rollno2;

	   //For descending order
            //rollno2-rollno1;
        }};

    @Override
    public String toString() {
        return "[ rollno=" + rollno + ", name=" + studentname + ", age=" + studentage + "]";
    }*/

}
