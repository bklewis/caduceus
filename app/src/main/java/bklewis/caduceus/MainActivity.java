package bklewis.caduceus;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.RectF;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;

import java.util.ArrayList;
import java.util.HashMap;


//import android.widget.SearchView;

public class MainActivity extends ActionBarActivity implements OnItemClickListener, /*WeekView.MonthChangeListener,
        WeekView.EventClickListener, */WeekView.EventLongPressListener/*, ActionBar.TabListener*/ {

    /**
     * The serialization (saved instance state)
     * Bundle key representing the
     * current tab position.
     */
    private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";

    final ActionBar actionBar = getSupportActionBar();

    // Initializing tabs
    /*ActionBar.Tab tab1, tab2, tab3;
    Fragment fragmentTab1 = new FragmentTab1();
    Fragment fragmentTab2 = new FragmentTab2();
    Fragment fragmentTab3 = new FragmentTab3();*/

    //variables for calendar
    private static final int TYPE_DAY_VIEW = 1;
    private static final int TYPE_THREE_DAY_VIEW = 2;
    private static final int TYPE_WEEK_VIEW = 3;
    private int mWeekViewType = TYPE_THREE_DAY_VIEW;
    private WeekView mWeekView;
    private long selectorId = -1l;

    //variables for navigation draw
    private DrawerLayout drawerLayout=null;
    private ActionBarDrawerToggle toggle=null;

    //hashmap for storing the profile ids with their associated first names
    protected static HashMap<Long,String> profiles = new HashMap<>();
    //array of profile ids (keys of hashmap)
    protected static ArrayList<Long> profileIDs = new ArrayList<>();

    //hashmap for storing events associated with ids
    protected static HashMap<WeekViewEvent, Long> weekViews = new HashMap<>();

    Context context;

    // Database
    //DBAdapter is superclass of all adapters
    static DBAdapter db;
    // Specifc adapters for each table in the database
    static DBeventAdapter dbe;
    static DBprofileAdapter dbp;
    static DBdocAdapter dbd;
    static DBmedAdapter dbm;
    static DBvaccineAdapter dbv;

    //notifications
    NotificationManager noteMan;

    //Tabs
    ViewPager pager;
    ViewPagerAdapter adapter;
    SlidingTabLayout tabs;
    CharSequence Titles[]={"Day","Three Day","List"};
    int Numboftabs = 3;
    private static final int PEEK_DRAWER_TIME_SECONDS = 2;
    private long downTime;
    private long eventTime;
    private float x = 0.0f;
    private float y = 100.0f;
    private int metaState = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setIcon(R.drawable.ic_launcher);


        // TODO: TAB STUFF

        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
         // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        /*tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabsScrollColor);
            }
        });*/

        // Setting the ViewPager For the SlidingTabsLayout
        //tabs.setViewPager(pager);

        /*
        // Get the ViewPager and set it's PagerAdapter so that it can display items
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new SampleFragmentPagerAdapter(getSupportFragmentManager(),
                MainActivity.this));

        // Give the SlidingTabLayout the ViewPager
        SlidingTabLayout slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        // Center the tabs in the layout
        slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setViewPager(viewPager);
        */
        adapter =  new ViewPagerAdapter(getSupportFragmentManager(),Titles,Numboftabs);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.viewpager);
        pager.setAdapter(adapter);



        Context context = getApplicationContext();

        //database
        db = new DBAdapter(context);
        dbe = new DBeventAdapter(context);
        dbp = new DBprofileAdapter(context);
        dbd = new DBdocAdapter(context);
        dbm = new DBmedAdapter(context);
        dbv = new DBvaccineAdapter(context);

        // Get a reference for the week view in the layout.
        mWeekView = (WeekView) findViewById(R.id.weekView);


        ListView drawer=(ListView)findViewById(R.id.drawer);
        //set up draws for navigation draw
        drawer.setAdapter(new ArrayAdapter<String>(
                this,
                R.layout.drawer_row,
                getResources().getStringArray(R.array.drawer_rows)));
        drawer.setOnItemClickListener(this);
        //toggle opening and closing the draws
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle=
                new ActionBarDrawerToggle(this, drawerLayout,
                        R.drawable.ic_drawer,
                        R.string.drawer_open,
                        R.string.drawer_close);
        drawerLayout.setDrawerListener(toggle);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        int tabInt = prefs.getInt("tabs", 0);
        String install = prefs.getString("installed", "no");
        if(install != null) {
            if (!(install.equals("no"))) {
                pager.setCurrentItem(tabInt);
            } else {
                drawerLayout.openDrawer(Gravity.LEFT);
                AlertDialog.Builder builder = new AlertDialog.Builder(this)
                        .setTitle("Welcome to Caduceus!")
                        .setMessage("Feel free to get started by making a profile, creating a new event or just exploring the app. For more information about the app, head over to the about page.")
                        .setPositiveButton(android.R.string.ok, new Dialog.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                builder.create().show();
            }
        }

        tabs = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        tabs.setDistributeEvenly(true);
        tabs.setViewPager(pager);
        new SimpleEula(this).show();
    }

    public void onStart() {
        super.onStart();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        //set up profile "spinner" for dropdown menu
        ActionBar actionBar = getSupportActionBar();
        DBprofileAdapter dbp = MainActivity.dbp;
        final ArrayList<ItemProfile> profiles = dbp.getAllProfiles();
        String[] names  = new String[profiles.size()+1];
        int i = 1;
        names[0] = "ALL";
        for(ItemProfile profile : profiles){
            names[i] = profile.firstName;
            i++;
        }
        long newId = prefs.getLong("selectorId", -1l);
        selectorId = newId;

        ArrayAdapter<String> spinnerArrayAdapter =new ArrayAdapter<String>(getBaseContext(), R.layout.custom_list_item, names);
        actionBar.setNavigationMode(android.app.ActionBar.NAVIGATION_MODE_LIST);
        actionBar.setListNavigationCallbacks(spinnerArrayAdapter, mOnNavigationListener);
        int location = prefs.getInt("location", 0);
        actionBar.setSelectedNavigationItem(location);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("installed", "yes");
        editor.apply();

    }

    //function that listens to navigation listener
    android.support.v7.app.ActionBar.OnNavigationListener mOnNavigationListener = new android.support.v7.app.ActionBar.OnNavigationListener() {
        // Get the same strings provided for the drop-down's ArrayAdapter
        @Override
        public boolean onNavigationItemSelected(int position, long itemId) {
            DBprofileAdapter dbp = MainActivity.dbp;
            final ArrayList<ItemProfile> profiles = dbp.getAllProfiles();
            Log.i("position is", "" + position);
            int location;
            if(position == 0) {
                selectorId = -1l;
                location = 0;
            } else {
                selectorId = profiles.get(position-1).id;
                location = position;
            }
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
            SharedPreferences.Editor editor = prefs.edit();
            long oldSelector = prefs.getLong("selectorId", -1l);
            if(oldSelector != selectorId) {
                recreate();
                overridePendingTransition(0, 0);
            }
            editor.putLong("selectorId", selectorId);
            editor.putInt("location", location);
            editor.apply();

            return true;
        }
    };

    public long getSelectorId(){
        return selectorId;
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.options, menu);
        MenuItem item = menu.findItem(R.id.export);
        item.setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        //int id = item.getItemId();

        if (toggle.onOptionsItemSelected(item)) {
            return(true);
        }
        switch (item.getItemId()) {
            //start new activity depending on which button is clicked
            case android.R.id.home:
                return(true);
            case R.id.add:
                Bundle b = new Bundle();
                b.putInt("type", 0);
                Intent iNew = new Intent(this, NewEvent.class);
                iNew.putExtras(b);
                startActivity(iNew);
                this.overridePendingTransition(0, 0);
                finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }


    @Override
    public void onItemClick(AdapterView<?> listView, View row,
                            int position, long id) {
        if (position == 1) {
            Intent iNew = new Intent(this, Doctors.class);
            iNew.setFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT|Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
        } else if (position == 2) {
            Intent iNew = new Intent(this, Profiles.class );

            startActivity(iNew);
            this.overridePendingTransition(0, 0);
        } else if (position == 3) {
            Intent iNew = new Intent(this, Medicines.class);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
        } else if (position == 4) {
            Intent iNew = new Intent(this, Vaccines.class);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
        } else if (position == 5) {
            Intent iNew = new Intent(this, About.class);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
        }
        drawerLayout.closeDrawers();
    }


    @Override
    public void onEventLongPress(WeekViewEvent event, RectF eventRect) {
        long id = weekViews.get(event);
        ItemEvent events = dbe.readEvent(id);
        Toast.makeText(this, "Clicked " + events.title, Toast.LENGTH_SHORT).show();
    }

    public void onStop() {
        super.onStop();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("tabs", pager.getCurrentItem());
        editor.apply();
    }

}