package bklewis.caduceus;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class Event extends ActionBarActivity implements View.OnClickListener {
    private Button ok;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        getSupportActionBar().setTitle("Event");
        //get medicine info from bundle passed in
        Bundle b = getIntent().getExtras();
        String theName = b.getString("key");
        TextView name = (TextView) findViewById(R.id.name);
        //set text view
        name.setText("Name: " + theName);
        //enable button clicking
        this.ok = (Button) findViewById(R.id.ok_button);
        this.ok.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_information, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void onClick(View v) {
        switch (v.getId()) {
            //what occurs when ok button clicked
            case R.id.ok_button:
                Intent iNew = new Intent(this, Medicines.class);
                startActivity(iNew);
                this.overridePendingTransition(0, 0);
                finish();
                break;

        }
    }

}
