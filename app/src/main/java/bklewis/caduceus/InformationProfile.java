package bklewis.caduceus;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class InformationProfile extends ActionBarActivity implements View.OnClickListener {
    private Button ok;
    private Button delete;
    private Button edit;
    private String firstName = null;
    private String lastName = null;
    private int birthMonth = -1;
    private int birthDay = -1;
    private int birthYear = -1;
    private String height = null;
    private String weight = null;
    private String allergies = null;
    private String insuranceCompany = null;
    private String insurancePhone = null;
    private String insuranceNumber = null;
    private String notes = null;
    Bundle b;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_profile);
        getSupportActionBar().setTitle("Profile Information");
        b = getIntent().getExtras();
        firstName = b.getString("firstName");
        lastName = b.getString("lastName");
        birthMonth = b.getInt("birthMonth");
        birthDay = b.getInt("birthDay");
        birthYear = b.getInt("birthYear");
        height = b.getString("height");
        weight = b.getString("weight");
        allergies = b.getString("allergies");
        notes = b.getString("notes");
        insuranceCompany = b.getString("insuranceCompany");
        insurancePhone = b.getString("insurancePhone");
        insuranceNumber = b.getString("insuranceNumber");
        TextView nameText = (TextView) findViewById(R.id.name);
        if (lastName != null) {
            nameText.setText(firstName + " " + lastName);
        } else {
            nameText.setText(firstName);
        }
        TextView one = (TextView)findViewById(R.id.text1);
        TextView two = (TextView)findViewById(R.id.text2);
        TextView three = (TextView)findViewById(R.id.text3);
        TextView four = (TextView)findViewById(R.id.text4);
        TextView five = (TextView)findViewById(R.id.text5);
        TextView six = (TextView)findViewById(R.id.text6);
        TextView seven = (TextView)findViewById(R.id.text7);
        TextView eight = (TextView)findViewById(R.id.text8);
        TextView ids[] = {one, two, three, four, five, six, seven, eight};
        if(birthMonth != 0 && birthDay != 0 && birthYear != 0) {
            String theBirthday = birthMonth + "/" + birthDay + "/" + birthYear;
            one.setText("Date of Birth: " + theBirthday);
            one.setVisibility(View.VISIBLE);
        } else {
            one.setVisibility(View.INVISIBLE);
        }
        this.ok = (Button) findViewById(R.id.ok_button);
        this.ok.setOnClickListener(this);
        this.edit = (Button) findViewById(R.id.edit_button);
        this.edit.setOnClickListener(this);
        this.delete = (Button) findViewById(R.id.delete_button);
        this.delete.setOnClickListener(this);
        String[] optionals = {height, weight, allergies, insuranceCompany, insuranceNumber, insurancePhone, notes};
        setOptionalEntries(ids, optionals);

    }

    public void onClick(View v) {
        switch (v.getId()) {
            //what occurs when ok button clicked
            case R.id.ok_button:
                //Intent iNew = new Intent(this, Profiles.class);
                //startActivity(iNew);
                finish();
                break;
            case R.id.edit_button:
                Intent iNew3 = new Intent(this, New_Profile.class);
                b.putInt("type", 1);
                if(b != null) {
                    iNew3.putExtras(b);
                }
                startActivity(iNew3);
                finish();
                break;
            case R.id.delete_button:
                AlertDialog.Builder builder3 = new AlertDialog.Builder(InformationProfile.this)
                        .setTitle("Are you sure you want to delete this profile?")
                        .setMessage("This action is permanent.")

                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                DBprofileAdapter dbp = MainActivity.dbp;
                                long id = b.getLong("id");
                                dbp.deleteProfile(id);
                                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(InformationProfile.this);
                                SharedPreferences.Editor editor = prefs.edit();
                                editor.putLong("selectorId", -1);
                                editor.putInt("location", 0);
                                editor.apply();
                                Intent iNew2 = new Intent(InformationProfile.this, Profiles.class);
                                iNew2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(iNew2);
                                finish();

                            }
                        })
                        .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                builder3.show();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_information_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void setOptionalEntries(TextView[] ids, String[] optionals) {
        int j = 0;
        for(int i = 0; i < optionals.length; i++) {
            if(optionals[i] != null && !(optionals[i].equals(""))) {
                TextView set = ids[j];
                set.setVisibility(View.VISIBLE);
                if(optionals[i].equals(weight)) {
                    set.setText("Weight: " + optionals[i]);
                } else if (optionals[i].equals(height)) {
                    set.setText("Height: " + optionals[i]);
                } else if(optionals[i].equals(allergies)) {
                    set.setText("Allergies: "+ optionals[i]);
                } else if(optionals[i].equals(insuranceCompany)) {
                    set.setText("Insurance Company: "+ optionals[i]);
                } else if(optionals[i].equals(insuranceNumber)) {
                    set.setText("Insurance Policy Number: " + optionals[i]);
                } else if(optionals[i].equals(insurancePhone)) {
                    set.setText("Insurance Phone Number: " + optionals[i]);
                    set.setTextColor(Color.parseColor("#318CE7"));
                    set.setPaintFlags(set.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                } else if(optionals[i].equals(notes)) {
                    set.setText("Notes: " + notes);
                } else {
                    set.setText(optionals[i]);
                }
                if(optionals[i].equals(insurancePhone)) {
                    set.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            String uri = "tel:" + insurancePhone.trim() ;
                            Intent intent = new Intent(Intent.ACTION_CALL);
                            intent.setData(Uri.parse(uri));
                            startActivity(intent);
                            // TODO Auto-generated method stub

                        }
                    });
                }
                j++;
            }
        }
        for(int c = j; c < ids.length; c++) {
            TextView set = ids[j];
            set.setVisibility(View.INVISIBLE);

        }
    }
}
