package bklewis.caduceus;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;


public class NewMedicine extends ActionBarActivity implements View.OnClickListener{
    //buttons in activity
    private Button ok;
    private Button cancel;
    private Button btnChangeTime;
    private Button notificationPicker;
    private Button profilePicker;
    //variables to store time
    private int mSelectedHour;
    private int mSelectedMinutes;
    String timeString;
    int notificationOptions = 0;
    private String medicineTitle;
    private String notes;
    private String description;
    private String refillsLeft;
    private String prescriber;
    private String dosage;
    private int h;
    private int m;
    private Long profileID = -1L;
    Bundle b;

    //store if time has been set
    boolean timeSet = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_medicine);
        getSupportActionBar().setTitle("New Medicine");
        b = getIntent().getExtras();
        //find buttons and set listener
        this.btnChangeTime = (Button) findViewById(R.id.medicineTime);
        this.ok = (Button) findViewById(R.id.ok_button);
        this.cancel = (Button) findViewById(R.id.cancel_button);
        this.ok.setOnClickListener(this);
        this.cancel.setOnClickListener(this);
        this.btnChangeTime.setOnClickListener(this);
        this.notificationPicker = (Button) findViewById(R.id.notificationPicker);
        this.notificationPicker.setOnClickListener(this);
        this.profilePicker = (Button) findViewById(R.id.profilePicker);
        this.profilePicker.setOnClickListener(this);
        //set current date
        Calendar calendar = Calendar.getInstance();
        this.mSelectedHour = calendar.get(Calendar.HOUR_OF_DAY);
        this.mSelectedMinutes = calendar.get(Calendar.MINUTE);
        //retrieve date/time if already set
        if(savedInstanceState != null) {
            String time1 = savedInstanceState.getString("startTime");
            if(savedInstanceState.getString("startTime") != null) {
                timeString = time1;
                btnChangeTime.setText("Taken at: " + time1);
            }

        }
        //check to see if profile view is set and set new profile to that
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Long newId = prefs.getLong("selectorId", -1l);
        if(newId != null) {
            if(newId != -1l) {
                DBprofileAdapter dbp = MainActivity.dbp;
                final ArrayList<ItemProfile> profiles = dbp.getAllProfiles();
                for (ItemProfile profile : profiles) {
                    if (profile.id == newId) {
                        profileID = profile.id;
                        profilePicker.setText(profile.firstName);
                    }
                }
            } else {
                profilePicker.setText("ALL");
                profileID = -1l;
            }



        }
        if(b.getInt("type") == 1) {
            getEdits();
        } else {
            if(newId != null) {
                if(newId != -1l) {
                    DBprofileAdapter dbp = MainActivity.dbp;
                    final ArrayList<ItemProfile> profiles = dbp.getAllProfiles();
                    for (ItemProfile profile : profiles) {
                        if (profile.id == newId) {
                            profileID = profile.id;
                            profilePicker.setText(profile.firstName);
                        }
                    }
                } else {
                    profilePicker.setText("ALL");
                    profileID = -1l;
                }



            }
        }

    }
    //function that is called when profile dropdown item is clicked
    android.support.v7.app.ActionBar.OnNavigationListener mOnNavigationListener = new android.support.v7.app.ActionBar.OnNavigationListener() {
        // Get the same strings provided for the drop-down's ArrayAdapter
        @Override
        public boolean onNavigationItemSelected(int position, long itemId) {
            return true;
        }
    };
    private TimePickerDialog.OnTimeSetListener mOnTimeSetListener = new TimePickerDialog.OnTimeSetListener() {

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // update the current variables (hour and minutes)
            mSelectedHour = hourOfDay;
            mSelectedMinutes = minute;
            timeSet = true;
            // update txtTime with the selected time
            updateTimeUI(0);
        }
    };
    private void updateTimeUI(int choose) {
        String amPm;
        String hour;
        h = mSelectedHour;
        m = mSelectedMinutes;
        //choose =  0 if start time is changing, 1 if end time is changing
        int time = choose;
        //change time from military to 12 hour
        if (mSelectedHour > 12) {
            int tempHour = mSelectedHour - 12;
            hour = "" + tempHour;
            amPm = "PM";
        } else if (mSelectedHour == 12) {
            hour = "" + mSelectedHour;
            amPm = "PM";
        } else if (mSelectedHour > 0) {
            hour = ""+mSelectedHour;
            amPm = "AM";
        } else {
            hour = "12";
            amPm = "AM";
        }
        String minutes = (mSelectedMinutes > 9) ?""+mSelectedMinutes : "0"+mSelectedMinutes;
        timeString = hour + ":" + minutes + " " + amPm;
        btnChangeTime.setText("Taken at: " + timeString);

    }
    // initialize the TimePickerDialog
    private TimePickerDialog showTimePickerDialog(int initialHour, int initialMinutes, boolean is24Hour, TimePickerDialog.OnTimeSetListener listener) {
        TimePickerDialog dialog = new TimePickerDialog(this, listener, initialHour, initialMinutes, is24Hour);
        dialog.show();
        return dialog;
    }

    @Override
    public void onClick(View v) {
        //set up cases for different buttons that are clicked
        switch (v.getId()) {
            case R.id.ok_button:

                PendingIntent pIntent;
                Calendar calendar = Calendar.getInstance();

                getStrings();
                if(medicineTitle != null && ((notificationOptions == 0) || (timeSet))) {
                    if (b.getInt("type") != 1) {

                        //creating event
                        ItemMedicine med = new ItemMedicine(-1, medicineTitle, dosage, refillsLeft, h, m, notificationOptions, prescriber, description, notes, profileID);
                        DBmedAdapter dbm = MainActivity.dbm;
                        int id = (int)dbm.createMed(med);



                        if(notificationOptions != 0){
                            Log.i("NOTE ON", Integer.toString(notificationOptions));
                            calendar.set(Calendar.HOUR_OF_DAY, h);
                            calendar.set(Calendar.MINUTE, m);
                            calendar.set(Calendar.SECOND, 0);

                            Intent myIntent = new Intent(getApplicationContext(), Receiver.class);
                            pIntent = PendingIntent.getBroadcast(getApplicationContext(), id, myIntent,0);

                            AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
                            //alarmManager.set(AlarmManager.RTC, calendar.getTimeInMillis(), pIntent);
                            alarmManager.setRepeating(AlarmManager.RTC, calendar.getTimeInMillis(), 24 * 60 * 60 * 1000 , pIntent);
                        }

                        //notifications
                    } else {
                        //updating event
                        DBmedAdapter dbm = MainActivity.dbm;

                        Long id = b.getLong("id");
                        ItemMedicine oldMed = dbm.readMed(id);

                        if(oldMed.notificationTime != 0){
                            Intent intent = new Intent(getApplicationContext(), Receiver.class);
                            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), (int)oldMed.id, intent, 0);
                            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                            alarmManager.cancel(pendingIntent);
                        }

                        ItemMedicine med = new ItemMedicine(id, medicineTitle, dosage, refillsLeft, h, m, notificationOptions, prescriber, description, notes, profileID);
                        dbm.updateMed(med);

                        if(notificationOptions != 0){
                            Log.i("NOTE ON", Integer.toString(notificationOptions));
                            Calendar newCal = Calendar.getInstance();

                            newCal.set(Calendar.HOUR_OF_DAY, h);
                            newCal.set(Calendar.MINUTE, m);
                            newCal.set(Calendar.SECOND, 0);

                            Intent myIntent = new Intent(getApplicationContext(), Receiver.class);
                            pIntent = PendingIntent.getBroadcast(getApplicationContext(), (int)med.id, myIntent,0);

                            AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
                            //alarmManager.set(AlarmManager.RTC, newCal.getTimeInMillis(), pIntent);
                            alarmManager.setRepeating(AlarmManager.RTC, newCal.getTimeInMillis(), 24 * 60 * 60 * 1000 , pIntent);
                        }


                    }
                    Intent iNew = new Intent(this, Medicines.class);
                    iNew.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(iNew);
                    this.overridePendingTransition(0, 0);
                    finish();
                } else {
                    if(medicineTitle == null) {
                        AlertDialog.Builder builder3 = new AlertDialog.Builder(NewMedicine.this)
                                .setTitle("Required Information Missing")
                                .setMessage("Please enter a medicine name for your Medicine to be saved.")

                                .setPositiveButton("ok", new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {


                                    }
                                });
                        builder3.show();
                        break;
                    } else {
                        AlertDialog.Builder builder3 = new AlertDialog.Builder(NewMedicine.this)
                                .setTitle("Required Information Missing")
                                .setMessage("In order to receive notifications, you must select a time.")

                                .setPositiveButton("ok", new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {


                                    }
                                });
                        builder3.show();
                        break;
                    }


                }
                break;
            case R.id.cancel_button:
                AlertDialog.Builder builder3 = new AlertDialog.Builder(NewMedicine.this)
                        .setTitle("Are you sure you want to leave this page?")
                        .setMessage("If you leave this page, your information will not be saved.")

                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                Intent iNew2 = new Intent(NewMedicine.this, Medicines.class);
                                startActivity(iNew2);
                                overridePendingTransition(0, 0);
                                finish();

                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                builder3.show();
                break;
            case R.id.medicineTime:
                showTimePickerDialog(mSelectedHour, mSelectedMinutes, false, mOnTimeSetListener);
                break;
            case R.id.profilePicker:
                DBprofileAdapter dbp = MainActivity.dbp;
                final ArrayList<ItemProfile> profiles = dbp.getAllProfiles();
                final String[] names  = new String[profiles.size()+1];
                int i = 1;
                names[0] = "ALL";
                for(ItemProfile profile : profiles){
                    names[i] = profile.firstName;
                    i++;
                }
                AlertDialog.Builder builder2 = new AlertDialog.Builder(NewMedicine.this)
                        .setTitle("Choose a Profile")
                        .setItems(names, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                profilePicker.setText(names[which]);
                                if(which > 0) {
                                    profileID = profiles.get(which - 1).id;
                                } else {
                                    profileID = -1l;
                                }
                            }
                        });
                builder2.show();
                break;
            case R.id.notificationPicker:
                AlertDialog.Builder builder4 = new AlertDialog.Builder(NewMedicine.this)
                        .setTitle("Notification")
                        .setItems(R.array.notificationOptions, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if(which == 0) {
                                    notificationOptions = 0;
                                    notificationPicker.setText("No notification");
                                } else {
                                    notificationOptions = 1;
                                    notificationPicker.setText("Notification at Med Time");
                                }
                            }
                        });
                builder4.show();
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_medicine, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //put year and times into outState in case of rotation
        outState.putString("startTime", timeString);
    }
    public void getStrings() {
        EditText medicineName = (EditText) findViewById(R.id.medicineName);
        medicineTitle = medicineName.getText().toString();
        EditText dosageName = (EditText) findViewById(R.id.dosage);
        dosage = dosageName.getText().toString();
        EditText refills = (EditText) findViewById(R.id.refillsLeft);
        refillsLeft = refills.getText().toString();
        EditText prescriberName = (EditText) findViewById(R.id.prescriber);
        prescriber = prescriberName.getText().toString();
        EditText descrip = (EditText) findViewById(R.id.description);
        description = descrip.getText().toString();
        EditText note  = (EditText) findViewById(R.id.medicineNotes);
        notes = note.getText().toString();
    }
    public void getEdits() {
        notificationOptions = b.getInt("notificationOptions");
        if(notificationOptions == 1) {
            notificationPicker.setText("Notification at Med Time");
        } else {
            notificationPicker.setText("No notification");
        }
        medicineTitle = b.getString("medicineName");
        EditText medicineName = (EditText) findViewById(R.id.medicineName);
        medicineName.setText(medicineTitle);
        dosage = b.getString("dosage");
        if(dosage != null) {
            EditText dosageName = (EditText) findViewById(R.id.dosage);
            dosageName.setText(dosage);
        }
        refillsLeft = b.getString("refillsLeft");
        if(refillsLeft != null) {
            EditText refills = (EditText) findViewById(R.id.refillsLeft);
            refills.setText(refillsLeft);
        }
        prescriber = b.getString("prescriber");
        if(prescriber != null) {
            EditText prescriberName = (EditText) findViewById(R.id.prescriber);
            prescriberName.setText(prescriber);
        }
        description = b.getString("description");
        if(description != null) {
            EditText descrip = (EditText) findViewById(R.id.description);
            descrip.setText(description);
        }
        notes = b.getString("notes");
        if(notes != null) {
            EditText note = (EditText) findViewById(R.id.medicineNotes);
            note.setText(notes);
        }
        int hour = b.getInt("hour");
        int minute = b.getInt("minute");
        if( hour != 0 || minute != 0) {
            mSelectedHour = hour;
            mSelectedMinutes = minute;
            timeSet = true;
            updateTimeUI(0);
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Long newId = prefs.getLong("selectorId", -1l);
        if(newId != null) {
            if(newId != -1l) {
                DBprofileAdapter dbp = MainActivity.dbp;
                final ArrayList<ItemProfile> profiles = dbp.getAllProfiles();
                for (ItemProfile profile : profiles) {
                    if (profile.id == newId) {
                        profileID = profile.id;
                        profilePicker.setText(profile.firstName);
                    }
                }
            } else {
                profilePicker.setText("ALL");
                profileID = -1l;
            }



        }
    }
}
