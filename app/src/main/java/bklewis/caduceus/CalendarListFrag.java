package bklewis.caduceus;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by hp1 on 21-01-2015.
 */
public class CalendarListFrag extends Fragment {

    HashMap<WeekViewEvent, Long> weekViews = MainActivity.weekViews;
    DBeventAdapter dbe;
    MainActivity main;
    Context context;
    View view;
    private WeekView mWeekView;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.calendar_list_frag,container,false);
        main = (MainActivity) getActivity();
        context = main.getApplicationContext();
        dbe = new DBeventAdapter(context);

        //List of Events
        ArrayList<ItemEvent> eventList; // = dbe.getAllEvents();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(main);
        long selectorId = prefs.getLong("selectorId", -1l);
        ListView myListview = (ListView) view.findViewById(R.id.eventListView);
        TextView tv = (TextView) view.findViewById(R.id.texts);
        if (selectorId == -1L) {
            eventList = dbe.getAllEvents();
        } else {
            eventList = dbe.getEventsByProfile(selectorId);
        }

        if (eventList.size() == 0) {
            tv.setText(R.string.events_empty);
        } else {

            ListEventAdapter myAdapter = new ListEventAdapter(eventList, context);;
            myListview.setAdapter(myAdapter);
            myListview.setOnItemClickListener(
                    new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {

                            Intent i = new Intent(context, Information.class);
                            Bundle b = new Bundle();

                            ItemEvent event = dbe.readEvent(id);
                            Toast.makeText(context, "Clicked " + event.title, Toast.LENGTH_SHORT).show();
                            b.putString("name", event.title);
                            b.putString("location", event.location);
                            b.putString("notes", event.notes);
                            b.putInt("month", event.month);
                            b.putInt("year", event.year);
                            b.putInt("day", event.day);
                            b.putInt("sHour", event.sHour);
                            b.putInt("eHour", event.eHour);
                            b.putInt("sMinute", event.sMinute);
                            b.putInt("eMinute", event.eMinute);
                            b.putString("color", event.color);
                            b.putLong("id", id);
                            b.putLong("profileID", event.profileID);
                            if (b != null) {
                                i.putExtras(b);
                            }
                            startActivity(i);
                        }
                    }
            );
        }
        return view;
    }

}