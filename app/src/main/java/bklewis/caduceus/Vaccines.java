package bklewis.caduceus;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Vaccines extends ActionBarActivity implements AdapterView.OnItemClickListener {
    private DrawerLayout drawerLayout=null;
    private ActionBarDrawerToggle toggle=null;
    private Long selectorId = -1l;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vaccines1);
        getSupportActionBar().setTitle("Vaccines");
        ListView drawer = (ListView) findViewById(R.id.drawer);

        drawer.setAdapter(new ArrayAdapter<String>(
                this,
                R.layout.drawer_row,
                getResources().getStringArray(R.array.drawer_rows)));
        drawer.setOnItemClickListener(this);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle =
                new ActionBarDrawerToggle(this, drawerLayout,
                        R.drawable.ic_drawer,
                        R.string.drawer_open,
                        R.string.drawer_close);
        drawerLayout.setDrawerListener(toggle);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Long newId = prefs.getLong("selectorId", -1l);
        if (newId != null) {
            selectorId = newId;
        }
        DBprofileAdapter dbp = MainActivity.dbp;
        final ArrayList<ItemProfile> profile1 = dbp.getAllProfiles();
        String[] names  = new String[profile1.size()+1];
        int j = 1;
        names[0] = "ALL";
        for(ItemProfile profile : profile1){
            names[j] = profile.firstName;
            j++;
        }
        //set up profile "spinner" for dropdown menu
        ArrayAdapter<String> spinnerArrayAdapter =new ArrayAdapter<String>(getBaseContext(), R.layout.custom_list_item, names);
        //spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        actionBar.setNavigationMode(android.app.ActionBar.NAVIGATION_MODE_LIST);
        actionBar.setListNavigationCallbacks(spinnerArrayAdapter, mOnNavigationListener);
        int location = prefs.getInt("location", 0);
        actionBar.setSelectedNavigationItem(location);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        final ListView listview = (ListView) findViewById(R.id.listview);
        DBvaccineAdapter dbv = MainActivity.dbv;
        final ArrayList<ItemVaccine> profiles;
        if (selectorId == -1l) {
            profiles = dbv.getAllVaccines();
        } else {
            profiles = dbv.getVaccineByProfile(selectorId);
        }
        //sample profiles
        if(profiles.size() == 0) {
            empty();
        } else {
            final ArrayList<String> list = new ArrayList<String>();
            for (int i = 0; i < profiles.size(); ++i) {
                list.add(profiles.get(i).title);
            }
            final StableArrayAdapter adapter = new StableArrayAdapter(this,
                    R.layout.mylist, list);
            listview.setAdapter(adapter);

            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, final View view,
                                        int position, long id) {
                    ItemVaccine newProfile = profiles.get(position);
                    //create new bundle to pass to information class
                    Intent i = new Intent(Vaccines.this, InformationVaccine.class);
                    Bundle b = new Bundle();
                    b.putString("title", newProfile.title);
                    b.putInt("expMonth", newProfile.expMonth);
                    b.putInt("expDay", newProfile.expDay);
                    b.putInt("expYear", newProfile.expYear);
                    b.putInt("dateDay", newProfile.dateDay);
                    b.putInt("dateMonth", newProfile.dateMonth);
                    b.putInt("dateYear", newProfile.dateYear);
                    b.putString("notes", newProfile.notes);
                    b.putString("dosage", newProfile.dosage);
                    b.putLong("profileId", newProfile.profileID);
                    b.putLong("id", newProfile.id);
                    if (b != null) {
                        i.putExtras(b);
                    }
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    //finish();

                }

            });
        }
    }
    //function that is called when profile dropdown item is clicked
    android.support.v7.app.ActionBar.OnNavigationListener mOnNavigationListener = new android.support.v7.app.ActionBar.OnNavigationListener() {
        // Get the same strings provided for the drop-down's ArrayAdapter
        @Override
        public boolean onNavigationItemSelected(int position, long itemId) {
            DBprofileAdapter dbp = MainActivity.dbp;
            final ArrayList<ItemProfile> profiles = dbp.getAllProfiles();
            Log.i("position is", "" + position);
            int location;
            if(position == 0) {
                selectorId = -1l;
                location = 0;
            } else {
                selectorId = profiles.get(position-1).id;
                location = position;
            }

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Vaccines.this);
            SharedPreferences.Editor editor = prefs.edit();
            long oldSelector = prefs.getLong("selectorId", -1l);
            if(oldSelector != selectorId) {
                recreate();
                overridePendingTransition(0, 0);
            }
            editor.putLong("selectorId", selectorId);
            editor.putInt("location", location);
            editor.apply();
            return true;
        }
    };


    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if (toggle.onOptionsItemSelected(item)) {
            return(true);
        }
        switch (item.getItemId()) {
            //menu options
            case android.R.id.home:
                return(true);
            case R.id.add:
                Bundle bundle = new Bundle();
                bundle.putInt("type", 0);
                Intent iNew = new Intent(this, NewVaccine.class);
                iNew.putExtras(bundle);
                startActivity(iNew);
                this.overridePendingTransition(0, 0);
                finish();
                return true;
            case R.id.export:
                DBvaccineAdapter dbv = MainActivity.dbv;
                final ArrayList<ItemVaccine> profiles;
                if (selectorId == -1l) {
                    profiles = dbv.getAllVaccines();
                } else {
                    profiles = dbv.getVaccineByProfile(selectorId);
                }
                if (profiles.size() == 0) {
                    AlertDialog.Builder builder3 = new AlertDialog.Builder(Vaccines.this)
                            .setTitle("Error: No vaccines to export.")
                            .setMessage("You cannot export to Google Drive since you currently have no vaccines associated with this profile. ")

                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                    builder3.show();
                } else {
                    AlertDialog.Builder builder3 = new AlertDialog.Builder(Vaccines.this)
                            .setTitle("Are you sure you want to export this data?")
                            .setMessage("Exporting this data will send it to your google drive.")

                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    Intent i = new Intent(Vaccines.this, FileWriter2.class);
                                    startActivityForResult(i, 1);
                                }
                            })
                            .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                    builder3.show();
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }


    @Override
    public void onItemClick(AdapterView<?> listView, View row,
                            int position, long id) {
        if(position == 0) {
            Intent iNew = new Intent(this, MainActivity.class);
            iNew.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        } else if (position == 1) {
            Intent iNew = new Intent(this, Doctors.class);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        } else if (position == 2) {
            Intent iNew = new Intent(this, Profiles.class);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        } else if (position ==3) {
            Intent iNew = new Intent(this, Medicines.class);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        } else if (position == 5) {
            Intent iNew = new Intent(this, About.class);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        }
        drawerLayout.closeDrawers();
    }
    public void empty() {
        TextView tv = (TextView)findViewById(R.id.eMessage);
        tv.setText(R.string.vacs_empty);

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == RESULT_OK){
                Toast.makeText(this, "Your information was uploaded in a spreadsheet to your Google Drive account!", Toast.LENGTH_LONG).show();
            }
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Unfortunately, your information could not be uploaded to Google Drive.", Toast.LENGTH_LONG).show();
            }
        }
    }
}
