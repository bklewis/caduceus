package bklewis.caduceus;

//imports for the newEvent
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;



public class NewEvent extends ActionBarActivity implements View.OnClickListener {
    public static final String TAG = "New Event";

    // views
    private Button btnChangeDate;
    private Button btnChangeTime;
    private Button btnChangeEndTime;
    private Button ok;
    private Button cancel;
    private Button colorPicker;
    private Button profilePicker;

    // variables to store the selected date and time
    private int mSelectedYear;
    private int mSelectedMonth;
    private int mSelectedDay;
    private int mSelectedHour;
    private int mSelectedMinutes;
    String dateString;
    String startString;
    String endString;
    String location;
    String eventNotes;
    String eventTitle;

    //store if time has been set
    boolean startTimeSet = false;
    boolean endTimeSet = false;
    boolean dateSet = false;

    //variables for creating new events
    int sHour;
    int sMinute;
    int eHour;
    int eMinute;
    Long profileID = -1L;
    Bundle bundle;

    //color string
    String color;



    //dialog variable
    //AlertDialog alert;

    // CallBacks for date and time pickers
    private DatePickerDialog.OnDateSetListener mOnDateSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // update the current variables ( year, month and day)
            mSelectedDay = dayOfMonth;
            mSelectedMonth = monthOfYear;
            mSelectedYear = year;
            dateSet = true;
            // update txtDate with the selected date
            updateDateUI();
        }
    };

    private TimePickerDialog.OnTimeSetListener mOnTimeSetListener = new TimePickerDialog.OnTimeSetListener() {

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // update the current variables (hour and minutes)
            mSelectedHour = hourOfDay;
            mSelectedMinutes = minute;
            startTimeSet = true;
            sHour = hourOfDay;
            sMinute = minute;
            // update txtTime with the selected time
            updateTimeUI(0);
        }
    };

    private TimePickerDialog.OnTimeSetListener mOnTimeSetListener2 = new TimePickerDialog.OnTimeSetListener() {

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // update the current variables (hour and minutes)
            mSelectedHour = hourOfDay;
            mSelectedMinutes = minute;
            endTimeSet = true;
            eHour = hourOfDay;
            eMinute = minute;
            // update txtTime with the selected time
            updateTimeUI(1);
        }
    };

    //database
    //DBeventAdapter db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_event);
        getSupportActionBar().setTitle("New Event");

        //database create
        //db = new DBeventAdapter(this);

        //set alert dialog
        //alert.onCreate(savedInstanceState);

        // retrieve views and set buttons
        this.btnChangeDate = (Button) findViewById(R.id.btn_show_date_picker);
        this.btnChangeTime = (Button) findViewById(R.id.btn_show_time_picker);
        this.btnChangeEndTime = (Button) findViewById(R.id.btn_show_endTime_picker);
        this.ok = (Button) findViewById(R.id.ok_button);
        this.cancel = (Button) findViewById(R.id.cancel_button);
        this.btnChangeDate.setOnClickListener(this);
        this.btnChangeTime.setOnClickListener(this);
        this.btnChangeEndTime.setOnClickListener(this);
        this.ok.setOnClickListener(this);
        this.cancel.setOnClickListener(this);
        this.colorPicker = (Button) findViewById(R.id.colorPicker);
        this.colorPicker.setOnClickListener(this);
        this.profilePicker = (Button) findViewById(R.id.profilePicker);
        this.profilePicker.setOnClickListener(this);
        // initialize the current date
        Calendar calendar = Calendar.getInstance();
        this.mSelectedYear = calendar.get(Calendar.YEAR);
        this.mSelectedMonth = calendar.get(Calendar.MONTH);
        this.mSelectedDay = calendar.get(Calendar.DAY_OF_MONTH);
        this.mSelectedHour = calendar.get(Calendar.HOUR_OF_DAY);
        this.mSelectedMinutes = calendar.get(Calendar.MINUTE);
        //retrieve date/time if already set
        //Log.i("is there a saved state?", savedInstanceState.getString("year"));
        if(savedInstanceState == null) {
            Log.i("saved instance state, ", "none");
        }
        if(savedInstanceState != null) {
            String date = savedInstanceState.getString("year");
            String time1 = savedInstanceState.getString("startTime");
            String time2 = savedInstanceState.getString("endTime");
            Log.i("saved instance state ", "exists");
            //Log.i("in saved state and year is: ", date);
            if(savedInstanceState.getString("year") != null) {
                dateString = date;
                btnChangeDate.setText(date);
            }
            if(savedInstanceState.getString("startTime") != null) {
                startString = time1;
                btnChangeTime.setText(time1);
            }
            if(savedInstanceState.getString("endTime") != null) {
                endString = time2;
                btnChangeEndTime.setText(time2);
            }

        }
        //check to see if profile view is set and set new profile to that
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Long newId = prefs.getLong("selectorId", -1l);
        if(newId != null) {
            if(newId != -1l) {
                DBprofileAdapter dbp = MainActivity.dbp;
                final ArrayList<ItemProfile> profiles = dbp.getAllProfiles();
                for (ItemProfile profile : profiles) {
                    if (profile.id == newId) {
                        profileID = profile.id;
                        profilePicker.setText(profile.firstName);
                    }
                }
            } else {
                profilePicker.setText("ALL");
                profileID = -1l;
            }



        }
        //get bundle, check to see if it's an event to be edited
        bundle = getIntent().getExtras();
        if(bundle.getInt("type") == 1) {
            getEdits();
        }


    }
    //function that is called when profile dropdown item is clicked
    android.support.v7.app.ActionBar.OnNavigationListener mOnNavigationListener = new android.support.v7.app.ActionBar.OnNavigationListener() {
        // Get the same strings provided for the drop-down's ArrayAdapter
        @Override
        public boolean onNavigationItemSelected(int position, long itemId) {
            return true;
        }
    };

    private void updateDateUI() {
        //update the UI for the date
        String month = ((mSelectedMonth+1) > 9) ? ""+(mSelectedMonth+1): "0"+(mSelectedMonth+1) ;
        String day = ((mSelectedDay) < 10) ? "0"+mSelectedDay: ""+mSelectedDay ;
        dateString = month+"/"+day+"/"+mSelectedYear;
        btnChangeDate.setText(dateString);
    }

    private void updateTimeUI(int choose) {
        String amPm;
        String hour;
        //choose =  0 if start time is changing, 1 if end time is changing
        int time = choose;
        //change time from military to 12 hour
        if (mSelectedHour > 12) {
            int tempHour = mSelectedHour - 12;
            hour = "" + tempHour;
            amPm = "PM";
        } else if (mSelectedHour == 12) {
            amPm = "PM";
            hour = "" + mSelectedHour;
        } else if (mSelectedHour > 0) {
            hour = ""+mSelectedHour;
            amPm = "AM";
        } else {
            hour = "12";
            amPm = "AM";
        }
        String minutes = (mSelectedMinutes > 9) ?""+mSelectedMinutes : "0"+mSelectedMinutes;
        if (time == 0) {
            startString = hour + ":" + minutes + " " + amPm;
            btnChangeTime.setText(startString);
        } else {
            endString = hour+":"+minutes+ " " + amPm;
            btnChangeEndTime.setText(endString);
        }
    }


    // initialize the DatePickerDialog
    private DatePickerDialog showDatePickerDialog(int initialYear, int initialMonth, int initialDay, DatePickerDialog.OnDateSetListener listener) {
        DatePickerDialog dialog = new DatePickerDialog(this, listener, initialYear, initialMonth, initialDay);
        dialog.show();
        return dialog;
    }

    // initialize the TimePickerDialog
    private TimePickerDialog showTimePickerDialog(int initialHour, int initialMinutes, boolean is24Hour, TimePickerDialog.OnTimeSetListener listener) {
        TimePickerDialog dialog = new TimePickerDialog(this, listener, initialHour, initialMinutes, is24Hour);
        dialog.show();
        return dialog;
    }


    @Override
    public void onClick(View v) {
        //set up cases for different buttons that are clicked
        switch (v.getId()) {
                case R.id.btn_show_date_picker:
                    showDatePickerDialog(mSelectedYear, mSelectedMonth, mSelectedDay, mOnDateSetListener);
                    break;
                case R.id.btn_show_time_picker:
                    showTimePickerDialog(mSelectedHour, mSelectedMinutes, false, mOnTimeSetListener);
                    break;
                case R.id.btn_show_endTime_picker:
                    showTimePickerDialog(mSelectedHour, mSelectedMinutes, false, mOnTimeSetListener2);
                    break;
                case R.id.colorPicker:
                    AlertDialog.Builder builder = new AlertDialog.Builder(NewEvent.this)
                            .setTitle("Choose a color")
                            .setItems(R.array.eventColors, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // The 'which' argument contains the index position
                                    // of the selected item
                                    if(which == 0) {
                                        color = "pink";
                                        colorPicker.setText("Pink");
                                    } else if (which ==1) {
                                        color = "blue";
                                        colorPicker.setText("Blue");
                                    } else if (which ==2) {
                                        color = "green";
                                        colorPicker.setText("Green");
                                    } else {
                                        color = "orange";
                                        colorPicker.setText("Orange");
                                    }
                                }
                            });
                    builder.show();
                    break;
                case R.id.profilePicker:
                    DBprofileAdapter dbp = MainActivity.dbp;
                    final ArrayList<ItemProfile> profiles = dbp.getAllProfiles();
                    final String[] names  = new String[profiles.size()+1];
                    int i = 1;
                    names[0] = "ALL";
                    for(ItemProfile profile : profiles){
                        names[i] = profile.firstName;
                        i++;
                    }
                    AlertDialog.Builder builder2 = new AlertDialog.Builder(NewEvent.this)
                            .setTitle("Choose a Profile")
                            .setItems(names, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    profilePicker.setText(names[which]);
                                    if(which > 0) {
                                        profileID = profiles.get(which - 1).id;
                                    } else {
                                        profileID = -1l;
                                    }
                                }
                            });
                    builder2.show();
                    break;

                case R.id.ok_button:
                    setStrings();
                    if (eventTitle != null && dateString != null && startString != null & endString != null && color != null && profileID != null) {
                        if ((sHour < eHour) || (sHour == eHour && sMinute < eMinute)) {

                            if (bundle.getInt("type") == 1) {
                                Long id = bundle.getLong("id");
                                ItemEvent event = new ItemEvent(id, eventTitle, color, mSelectedYear, mSelectedMonth, mSelectedDay, sHour, sMinute, eHour, eMinute, location, eventNotes, profileID);
                                Log.i("profile id: ", "" + profileID);
                                DBeventAdapter dbe = MainActivity.dbe;
                                dbe.updateEvent(event);
                            } else {
                                ItemEvent event = new ItemEvent(-1, eventTitle, color, mSelectedYear, mSelectedMonth, mSelectedDay, sHour, sMinute, eHour, eMinute, location, eventNotes, profileID);
                                DBeventAdapter dbe = MainActivity.dbe;//new DBeventAdapter(this);
                                Log.i("profile id: ", "" + profileID);
                                dbe.createEvent(event);
                            }

                            //Toast toast = Toast.makeText(getApplicationContext(), endString, Toast.LENGTH_LONG);
                            //toast.show();
                            //Cursor cursor = dbe.getAllRows(dbe.TABLE_EVENTS,dbe.EVENT_KEYS);
                            //Log.i("TAG", "");

                            //ItemEvent e = dbe.getEvent(cursor);
                            //cursor.close();

                            Intent iNew = new Intent(this, MainActivity.class);
                            iNew.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(iNew);
                            this.overridePendingTransition(0, 0);
                            finish();
                        } else {
                            AlertDialog.Builder builder3 = new AlertDialog.Builder(NewEvent.this)
                                    .setTitle("Error with Times Selected")
                                    .setMessage("The end time of the event should be after the start time.")

                                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {

                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });
                            builder3.show();

                        }
                    } else {
                        AlertDialog.Builder builder3 = new AlertDialog.Builder(NewEvent.this)
                                .setTitle("Required Information Missing")
                                .setMessage("For your event to be saved, you need to enter a title, date, start time, end time and color.")

                                .setPositiveButton("ok", new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                        builder3.show();


                    }
                    //else, create popup saying they need to enter all required fields
                    break;
                case R.id.cancel_button:
                    AlertDialog.Builder builder3 = new AlertDialog.Builder(NewEvent.this)
                            .setTitle("Are you sure you want to leave this page?")
                            .setMessage("If you leave this page, your information will not be saved.")

                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    Intent iNew2 = new Intent(NewEvent.this, MainActivity.class);
                                    startActivity(iNew2);
                                    overridePendingTransition(0, 0);
                                    finish();

                                }
                            })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                    builder3.show();
                    break;
        }
    }

    public void getEdits() {
        profileID = bundle.getLong("profileID");
        eventTitle = bundle.getString("name");
        EditText title = (EditText) findViewById(R.id.eventName);
        title.setText(eventTitle);
        mSelectedDay = bundle.getInt("year");
        mSelectedMonth = bundle.getInt("month") - 1;
        mSelectedDay = bundle.getInt("day");
        updateDateUI();
        sHour = bundle.getInt("sHour");
        sMinute = bundle.getInt("sMinute");
        mSelectedHour = sHour;
        mSelectedMinutes = sMinute;
        updateTimeUI(0);
        eHour = bundle.getInt("eHour");
        eMinute = bundle.getInt("eMinute");
        mSelectedHour = eHour;
        mSelectedMinutes = eMinute;
        color = bundle.getString("color");
        colorPicker.setText(color);
        updateTimeUI(1);
        //optional variables to be displayed
        location = bundle.getString("location");
        eventNotes = bundle.getString("notes");
        if (location != null) {
            EditText locations = (EditText) findViewById(R.id.location);
            locations.setText(location);
        }
        if( eventNotes != null) {
            EditText note = (EditText) findViewById(R.id.notes);
            note.setText(eventNotes);
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Long newId = prefs.getLong("profileID", -1l);
        if(newId != null) {
            if(newId != -1l) {
                DBprofileAdapter dbp = MainActivity.dbp;
                final ArrayList<ItemProfile> profiles = dbp.getAllProfiles();
                for (ItemProfile profile : profiles) {
                    if (profile.id == newId) {
                        profileID = profile.id;
                        profilePicker.setText(profile.firstName);
                    }
                }
            } else {
                profilePicker.setText("ALL");
                profileID = -1l;
            }



        }
    }
    public void setStrings() {
        EditText title = (EditText) findViewById(R.id.eventName);
        if(!(isEmpty(title))) {
            eventTitle = title.getText().toString();
        }
        EditText locations = (EditText) findViewById(R.id.location);
        if(!(isEmpty(locations))) {
            location = locations.getText().toString();
        }
        EditText note2 = (EditText) findViewById(R.id.notes);
        if(!(isEmpty(note2))) {
            eventNotes = note2.getText().toString();
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //put year and times into outState
        outState.putString("year", dateString);
        outState.putString("startTime", startString);
        outState.putString("endTime", endString);
    }
    private boolean isEmpty(EditText etText) {
        return etText.getText().toString().trim().length() == 0;
    }

}
