package bklewis.caduceus;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bryce_000 on 3/27/2015.
 * DB Adapter.  To be extended by all DB*Adapter classes.
 */
public class DBAdapter {

    // Tag for logging
    protected static final String TAG = "DBAdapter";

    // Database information
    protected static final String DATABASE_NAME = "CaduceusDB";
    protected static final int DATABASE_VERSION = 1;

    // Database Components
    protected static SQLiteDatabase db = null;
    protected static DBHelper dbHelper;
    protected final Context context;

    // ID column name of all db tables
    protected static String KEY_ID = "_id";
    protected static final String KEY_ID_PROFILE = "profile";


    // TODO: TABLE STUFF

    // -------------------- EVENTS TABLE --------------------
    protected static final String TABLE_EVENTS = "events";
    protected static final String KEY_EVENT_ID = "_id";
    protected static final String KEY_EVENT_TITLE = "title";
    protected static final String KEY_EVENT_COLOR = "color";
    //protected static final String KEY_EVENT_APT = "appointment";
    //protected static final String KEY_EVENT_DOC_ID = "doctor_id";
    protected static final String KEY_EVENT_START_HOUR = "start_hour";
    protected static final String KEY_EVENT_START_MINUTE = "start_minute";
    protected static final String KEY_EVENT_END_HOUR = "end_hour";
    protected static final String KEY_EVENT_END_MINUTE = "end_minute";
    protected static final String KEY_EVENT_DAY = "day";
    protected static final String KEY_EVENT_YEAR = "year";
    protected static final String KEY_EVENT_MONTH = "month";
    protected static final String KEY_EVENT_LOCATION = "location";
    protected static final String KEY_EVENT_NOTES = "notes";
    protected static final String KEY_EVENT_PROFILE_ID = "profile";

    // -------------------- PROFILE TABLE --------------------
    protected static final String TABLE_PROFILES = "profiles";
    protected static final String KEY_PROFILE_ID = "_id";
    protected static final String KEY_PROFILE_FIRSTNAME = "first";
    protected static final String KEY_PROFILE_LASTNAME = "last";
    protected static final String KEY_PROFILE_BIRTH_DAY = "birthDay";
    protected static final String KEY_PROFILE_BIRTH_MONTH = "birthMonth";
    protected static final String KEY_PROFILE_BIRTH_YEAR = "birthYear";
    protected static final String KEY_PROFILE_HEIGHT = "height";
    protected static final String KEY_PROFILE_WEIGHT = "weight";
    protected static final String KEY_PROFILE_ALLERGIES = "allergies";
    protected static final String KEY_PROFILE_NOTES = "notes";
    protected static final String KEY_PROFILE_INSURANCE_COMPANY = "insurance_company";
    protected static final String KEY_PROFILE_INSURANCE_POLICY_NUMBER = "policy_number";
    protected static final String KEY_PROFILE_INSURANCE_PHONE = "insurance_phone";

    // -------------------- VACCINES TABLE --------------------
    protected static final String TABLE_VACCINES = "vaccines";
    protected static final String KEY_VACCINE_ID = "_id";
    protected static final String KEY_VACCINE_TITLE = "title";
    protected static final String KEY_VACCINE_DATE_DAY = "date_received_day";
    protected static final String KEY_VACCINE_DATE_MONTH = "date_received_month";
    protected static final String KEY_VACCINE_DATE_YEAR = "date_received_year";
    protected static final String KEY_VACCINE_EXP_DAY = "expiration_day";
    protected static final String KEY_VACCINE_EXP_MONTH = "expiration_month";
    protected static final String KEY_VACCINE_EXP_YEAR = "expiration_year";
    protected static final String KEY_VACCINE_DOSAGE = "dosage";
    protected static final String KEY_VACCINE_NOTES = "notes";
    protected static final String KEY_VACCINE_PROFILE_ID = "profile";

    // -------------------- MEDS TABLE --------------------
    protected static final String TABLE_MEDS = "medications";
    protected static final String KEY_MED_ID = "_id";
    protected static final String KEY_MED_TITLE = "title";
    protected static final String KEY_MED_DOSAGE = "dosage";
    protected static final String KEY_MED_REFILLS_LEFT = "refills_left";
    //protected static final String KEY_MED_REFILL_NEXT_MONTH = "next_refill_monthy";
    //protected static final String KEY_MED_REFILL_NEXT_DAY = "next_refill_day";
    //protected static final String KEY_MED_REFILL_NEXT_YEAR = "next_refill_year";
    //protected static final String KEY_MED_REFILL_NOTIF = "notification_refill";
    protected static final String KEY_MED_PRESCRIBER = "prescriber";
    protected static final String KEY_MED_DESCRIPTION = "description";
    protected static final String KEY_MED_NOTES = "notes";
    protected static final String KEY_MED_TIME_TAKEN_HOUR = "time_taken_hour";
    protected static final String KEY_MED_TIME_TAKEN_MINUTE = "time_taken_minute";
    protected static final String KEY_MED_TIME_NOTIF = "notification_time";
    protected static final String KEY_MED_PROFILE_ID = "profile";

    // -------------------- DOCS TABLE --------------------
    protected static final String TABLE_DOCS = "doctors";
    protected static final String KEY_DOC_ID = "_id";
    protected static final String KEY_DOC_FIRSTNAME = "first_name";
    protected static final String KEY_DOC_LASTNAME = "last_name";
    protected static final String KEY_DOC_SPECIALTY = "specialty";
    protected static final String KEY_DOC_PHONE1 = "primary_phone";
    protected static final String KEY_DOC_PHONE2 = "secondary_phone";
    protected static final String KEY_DOC_EMAIL = "email";
    protected static final String KEY_DOC_FAX = "fax";
    protected static final String KEY_DOC_HOSPITAL = "hospital";
    protected static final String KEY_DOC_ADDRESS = "address";
    protected static final String KEY_DOC_NOTES = "notes";

    // --------------- SQL statement to create EVENTS table ---------------
    protected static final String SQL_CREATE_TABLE_EVENTS =
            "CREATE TABLE IF NOT EXISTS " + TABLE_EVENTS + "("
                    + KEY_EVENT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + KEY_EVENT_TITLE + " TEXT NOT NULL, "
                    + KEY_EVENT_COLOR + " TEXT NOT NULL, "
                    //+ KEY_EVENT_APT + " INTEGER, "
                    //+ KEY_EVENT_DOC_ID + " INTEGER, "
                    + KEY_EVENT_DAY + " INTEGER NOT NULL, "
                    + KEY_EVENT_MONTH + " INTEGER NOT NULL, "
                    + KEY_EVENT_YEAR + " INTEGER NOT NULL, "
                    + KEY_EVENT_START_HOUR + " INTEGER NOT NULL, "
                    + KEY_EVENT_START_MINUTE + " INTEGER NOT NULL, "
                    + KEY_EVENT_END_HOUR + " INTEGER NOT NULL, "
                    + KEY_EVENT_END_MINUTE + " INTEGER NOT NULL, "
                    + KEY_EVENT_LOCATION + " TEXT, "
                    + KEY_EVENT_NOTES + " TEXT, "
                    + KEY_EVENT_PROFILE_ID + " INTEGER NOT NULL "
                    +");";

    // --------------- SQL statement to create PROFILE table ---------------
    protected static final String SQL_CREATE_TABLE_PROFILES =
            "CREATE TABLE IF NOT EXISTS " + TABLE_PROFILES + "("
                    + KEY_PROFILE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + KEY_PROFILE_FIRSTNAME + " TEXT NOT NULL, "
                    + KEY_PROFILE_LASTNAME + " TEXT, "
                    + KEY_PROFILE_BIRTH_DAY + " INTEGER, "
                    + KEY_PROFILE_BIRTH_MONTH + " INTEGER, "
                    + KEY_PROFILE_BIRTH_YEAR + " INTEGER, "
                    + KEY_PROFILE_HEIGHT + " TEXT, "
                    + KEY_PROFILE_WEIGHT + " TEXT, "
                    + KEY_PROFILE_ALLERGIES + " TEXT, "
                    + KEY_PROFILE_NOTES + " TEXT, "
                    + KEY_PROFILE_INSURANCE_COMPANY + " TEXT, "
                    + KEY_PROFILE_INSURANCE_POLICY_NUMBER + " TEXT, "
                    + KEY_PROFILE_INSURANCE_PHONE + " TEXT "
                    +");";

    // --------------- SQL statement to create VACCINES table ---------------
    protected static final String SQL_CREATE_TABLE_VACCINES =
            "CREATE TABLE IF NOT EXISTS " + TABLE_VACCINES + "("
                    + KEY_VACCINE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + KEY_VACCINE_TITLE + " TEXT NOT NULL, "
                    + KEY_VACCINE_DATE_DAY + " TEXT, "
                    + KEY_VACCINE_DATE_MONTH + " TEXT, "
                    + KEY_VACCINE_DATE_YEAR + " TEXT, "
                    + KEY_VACCINE_EXP_DAY + " TEXT, "
                    + KEY_VACCINE_EXP_MONTH + " TEXT, "
                    + KEY_VACCINE_EXP_YEAR + " TEXT, "
                    + KEY_VACCINE_DOSAGE + " TEXT, "
                    + KEY_VACCINE_NOTES + " TEXT, "
                    + KEY_VACCINE_PROFILE_ID + " INTEGER NOT NULL "
                    +");";

    // --------------- SQL statement to create MEDS table ---------------
    protected static final String SQL_CREATE_TABLE_MEDS =
            "CREATE TABLE IF NOT EXISTS " + TABLE_MEDS + "("
                    + KEY_MED_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + KEY_MED_TITLE + " TEXT NOT NULL, "
                    + KEY_MED_DOSAGE + " TEXT, "
                    + KEY_MED_TIME_TAKEN_HOUR + " INTEGER, "
                    + KEY_MED_TIME_TAKEN_MINUTE + " INTEGER, "
                    + KEY_MED_TIME_NOTIF + " INTEGER NOT NULL, "
                    + KEY_MED_REFILLS_LEFT + " TEXT, "
                    //+ KEY_MED_REFILL_NEXT_MONTH + " INTEGER, "
                    //+ KEY_MED_REFILL_NEXT_DAY + " INTEGER, "
                    //+ KEY_MED_REFILL_NEXT_YEAR + " INTEGER, "
                    //+ KEY_MED_REFILL_NOTIF + " INTEGER NOT NULL, "
                    + KEY_MED_PRESCRIBER + " TEXT, "
                    + KEY_MED_DESCRIPTION + " TEXT, "
                    + KEY_MED_NOTES + " TEXT, "
                    + KEY_MED_PROFILE_ID + " INTEGER NOT NULL "
                    +");";

    // --------------- SQL statement to create DOCTORS table ---------------
    protected static final String SQL_CREATE_TABLE_DOCTORS =
            "CREATE TABLE IF NOT EXISTS " + TABLE_DOCS + "("
                    + KEY_DOC_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + KEY_DOC_FIRSTNAME + " TEXT, "
                    + KEY_DOC_LASTNAME + " TEXT NOT NULL, "
                    + KEY_DOC_SPECIALTY + " TEXT, "
                    + KEY_DOC_PHONE1 + " TEXT, "
                    + KEY_DOC_PHONE2 + " TEXT, "
                    + KEY_DOC_EMAIL + " TEXT, "
                    + KEY_DOC_FAX + " TEXT, "
                    + KEY_DOC_HOSPITAL + " TEXT, "
                    + KEY_DOC_ADDRESS + " TEXT, "
                    + KEY_DOC_NOTES + " TEXT "
                    +");";


    // TODO: END OF TABLE STUFF

    // All keys for tables
    public static final String[] KEYS_EVENT = {KEY_ID,
            KEY_EVENT_TITLE, KEY_EVENT_COLOR, KEY_EVENT_DAY,
            KEY_EVENT_MONTH, KEY_EVENT_YEAR, KEY_EVENT_START_HOUR,
            KEY_EVENT_START_MINUTE, KEY_EVENT_END_HOUR, KEY_EVENT_END_MINUTE,
            KEY_EVENT_LOCATION, KEY_EVENT_NOTES, KEY_ID_PROFILE};
    public static final String[] KEYS_PROFILE = {KEY_ID,
            KEY_PROFILE_FIRSTNAME, KEY_PROFILE_LASTNAME,
            KEY_PROFILE_BIRTH_DAY, KEY_PROFILE_BIRTH_MONTH, KEY_PROFILE_BIRTH_YEAR,
            KEY_PROFILE_HEIGHT, KEY_PROFILE_WEIGHT, KEY_PROFILE_ALLERGIES,
            KEY_PROFILE_NOTES, KEY_PROFILE_INSURANCE_COMPANY,
            KEY_PROFILE_INSURANCE_POLICY_NUMBER, KEY_PROFILE_INSURANCE_PHONE};
    public static final String[] KEYS_VACCINE = {KEY_ID,
            KEY_VACCINE_TITLE, KEY_VACCINE_DATE_DAY, KEY_VACCINE_DATE_MONTH, KEY_VACCINE_DATE_YEAR,
            KEY_VACCINE_EXP_DAY, KEY_VACCINE_EXP_MONTH, KEY_VACCINE_EXP_YEAR,
            KEY_VACCINE_DOSAGE, KEY_VACCINE_NOTES, KEY_VACCINE_PROFILE_ID};
    public static final String[] KEYS_MED = {KEY_ID,
            KEY_MED_TITLE, KEY_MED_DOSAGE, KEY_MED_TIME_TAKEN_HOUR,
            KEY_MED_TIME_TAKEN_MINUTE, KEY_MED_TIME_NOTIF, KEY_MED_REFILLS_LEFT,
            //KEY_MED_REFILL_NEXT_MONTH, KEY_MED_REFILL_NEXT_DAY, KEY_MED_REFILL_NEXT_YEAR,
            //KEY_MED_REFILL_NOTIF,
            KEY_MED_PRESCRIBER, KEY_MED_DESCRIPTION,
            KEY_MED_NOTES, KEY_MED_PROFILE_ID};
    public static final String[] KEYS_DOC = {KEY_ID,
            KEY_DOC_FIRSTNAME, KEY_DOC_LASTNAME, KEY_DOC_SPECIALTY,
            KEY_DOC_PHONE1, KEY_DOC_PHONE2, KEY_DOC_EMAIL,
            KEY_DOC_FAX, KEY_DOC_HOSPITAL, KEY_DOC_ADDRESS, KEY_DOC_NOTES};


    // Constructor (takes in context)
    public DBAdapter(Context context){

        this.context = context;
        dbHelper = dbHelper.getInstance(context);
        //dbHelper = new DBHelper(context);

        // open database
        try{
            open();
        } catch (SQLiteException e){
            Log.e(TAG, "SQL Exception on opening database: " + e.getMessage());
            e.printStackTrace();
        }
    }

    // Open Database
    public void open() throws SQLiteException{
        db = dbHelper.getWritableDatabase();
    }

    // Close database
    public void close(){
        dbHelper.close();
    }

    // Delete a row in a table
    // Return true if deleted, false otherwise
    public boolean deleteRow(String dbTable, long rowId){
        return db.delete(dbTable, KEY_ID + "=" + rowId, null) > 0;
    }

    // Deletes all data in a table
    // Given a table name and list of the table's keys, completely clears table
    public void clearTable(String dbTable, String[] allKeys){
        Cursor cursor = getAllRows(dbTable, allKeys);
        long rowId = cursor.getColumnIndexOrThrow(KEY_ID);
        if(cursor.moveToFirst()){
            do{
                deleteRow(dbTable, cursor.getLong((int) rowId));
            } while (cursor.moveToNext());
        }
        cursor.close();
    }

    // Gets all rows from a table
    public Cursor getAllRows(String dbTable, String[] allKeys){
        return db.rawQuery("SELECT * FROM " + dbTable, null);
        //return db.query(true, dbTable, allKeys, null, null, null, null, null, null);
    }

    public Cursor getAllRows(String dbTable){
        return db.rawQuery("SELECT * FROM " + dbTable, null);
    }

    //public abstract long insertRow(Object object);

    //public abstract boolean updateRow();



    // get all rows from the database
    /*public Object getAllCompanies(){
        List<String> listRows = new ArrayList<>();
        Cursor cursor = db.query(DBHelper.TABLE_EVENTS, allKeys,
                null, null, null, null, null);

        if (cursor != null){
            cursor.moveToFirst();
            while (!cursor.isAfterLast()){
                <Row> row = cursorToRow(cursor);
                listRows.add(row);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return listRows;
    }*/

    /*protected Object getRowById(long id){
        Cursor cursor = db.query(DBHelper.<Table>, allKeys,
                DBHelper.<key_id> + )" = ?",
                new String[] {String.valueOf(id) }, null, null, null});
        if (cursor != null)
            cursor.moveToFirst();
    }*/



    // --------------- Database Helper Class ---------------
    protected static class DBHelper extends SQLiteOpenHelper{


        //private static DBHelper mInstance = null;

        //private static final String DATABASE_NAME = "database_name";
        //private static final String DATABASE_TABLE = "table_name";
        //private static final int DATABASE_VERSION = 1;

        public static DBHelper getInstance(Context ctx) {

            // Use the application context, which will ensure that you
            // don't accidentally leak an Activity's context.
            // See this article for more information: http://bit.ly/6LRzfx
            if (dbHelper == null) {
                dbHelper = new DBHelper(ctx.getApplicationContext());
            }
            return dbHelper;
        }

        public DBHelper(Context context){
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db){
            db.execSQL(SQL_CREATE_TABLE_EVENTS);
            db.execSQL(SQL_CREATE_TABLE_PROFILES);
            db.execSQL(SQL_CREATE_TABLE_MEDS);
            db.execSQL(SQL_CREATE_TABLE_DOCTORS);
            db.execSQL(SQL_CREATE_TABLE_VACCINES);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
            Log.w(TAG, "Upgrading the database from " + oldVersion + " to " + newVersion);

            // TODO: move data from oldVersion to newVersion
            //clear all data
            // TODO: add in other tables
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENTS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROFILES);

            //recreate tables
            onCreate(db);
        }

        public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
            super(context, DATABASE_NAME, factory, DATABASE_VERSION);
        }

    }

}
