package bklewis.caduceus;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class InformationMedicine extends ActionBarActivity implements View.OnClickListener{
    private Button ok;
    private Button delete;
    private Button edit;
    private String medicineName = null;
    private long id = -1L;
    private String dosage = null;
    private String prescriber = null;
    private String description = null;
    private String refillsLeft = null;
    private int minute = 0;
    private int hour = 0;
    private int notificationOptions = 0;
    private String notes = null;
    private long profileId = -1l;
    private String time = null;
    String notify;
    Bundle b;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_medicine);
        getSupportActionBar().setTitle("Medicine Information");
        b = getIntent().getExtras();
        ok = (Button) findViewById(R.id.ok_button);
        ok.setOnClickListener(this);
        edit = (Button) findViewById(R.id.edit_button);
        edit.setOnClickListener(this);
        delete = (Button) findViewById(R.id.delete_button);
        delete.setOnClickListener(this);
        TextView title = (TextView)findViewById(R.id.name);
        medicineName = b.getString("medicineName");
        title.setText(medicineName);

        id = b.getLong("id");
        profileId = b.getLong("profileId");
        dosage = b.getString("dosage");
        refillsLeft = b.getString("refillsLeft");
        notes = b.getString("notes");
        description = b.getString("description");
        prescriber = b.getString("prescriber");
        minute = b.getInt("minute");
        hour = b.getInt("hour");
        notificationOptions = b.getInt("notificationOptions");
        TextView one = (TextView)findViewById(R.id.text1);
        TextView two = (TextView)findViewById(R.id.text2);
        TextView three = (TextView)findViewById(R.id.text3);
        TextView four = (TextView)findViewById(R.id.text4);
        TextView five = (TextView)findViewById(R.id.text5);
        TextView six = (TextView)findViewById(R.id.text6);
        TextView seven = (TextView)findViewById(R.id.text7);
        TextView[] ids = {one, two, three, four, five, six, seven};
        if(notificationOptions == 0) {
            notify = "Notifications Off";
        } else {
            notify = "Notifications On";
        }
        if(hour != 0 && minute != 0) {
            time = timeCreator(hour, minute);
        }
        String[] optionals = {dosage, description, prescriber, refillsLeft, time, notify, notes};
        setOptionalEntries(ids, optionals);
    }

    public String timeCreator(int mSelectedHour,int mSelectedMinute) {
        String amPm;
        String hours;
        if (mSelectedHour > 12) {
            int tempHour = mSelectedHour - 12;
            hours = "" + tempHour;
            amPm = "PM";
        } else if (mSelectedHour == 12) {
            hours = "" + mSelectedHour;
            amPm = "PM";
        } else if (mSelectedHour > 0) {
            hours = ""+mSelectedHour;
            amPm = "AM";
        } else {
            hours = "12";
            amPm = "AM";
        }
        String minutes = (mSelectedMinute > 9) ?""+mSelectedMinute : "0"+mSelectedMinute;
        String timeString = "" + hours + ":" + minutes + " " + amPm;
        return timeString;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_information_medicine, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void onClick(View v) {
        switch (v.getId()) {
            //what occurs when ok button clicked
            case R.id.ok_button:
                //Intent iNew = new Intent(this, Medicines.class);
                //startActivity(iNew);
                finish();
                break;
            case R.id.edit_button:
                Intent iNew3 = new Intent(this, NewMedicine.class);
                b.putInt("type", 1);
                if(b != null) {
                    iNew3.putExtras(b);
                }
                startActivity(iNew3);
                finish();
                break;
            case R.id.delete_button:
                AlertDialog.Builder builder3 = new AlertDialog.Builder(InformationMedicine.this)
                        .setTitle("Are you sure you want to delete this doctor profile?")
                        .setMessage("This action is permanent.")

                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                DBmedAdapter dbm = MainActivity.dbm;
                                long id = b.getLong("id");
                                dbm.deleteMed(id);
                                Intent iNew2 = new Intent(InformationMedicine.this, Medicines.class);
                                iNew2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(iNew2);
                                finish();

                            }
                        })
                        .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                builder3.show();
                break;

        }
    }
    public void setOptionalEntries(TextView[] ids, String[] optionals) {
        int j = 0;
        for(int i = 0; i < optionals.length; i++) {
            if(optionals[i] != null && !(optionals[i].equals(""))) {
                TextView set = ids[j];
                set.setVisibility(View.VISIBLE);
                if(optionals[i].equals(dosage)) {
                    set.setText("Dosage: " + optionals[i]);
                } else if(optionals[i].equals(description)) {
                    set.setText("Description: " + optionals[i]);
                } else if(optionals[i].equals(prescriber)) {
                    set.setText("Prescriber: " + optionals[i]);
                } else if(optionals[i].equals(refillsLeft)) {
                    set.setText("Refills Left: "+ optionals[i]);
                } else if(optionals[i].equals(time)) {
                    set.setText("Time Taken: " + optionals[i]);
                } else if(optionals[i].equals(notes)) {
                    set.setText("Notes: " + optionals[i]);
                } else {
                    set.setText(optionals[i]);
                }
                j++;
            }
        }
        for(int c = j; c < ids.length; c++) {
            TextView set = ids[j];
            set.setVisibility(View.INVISIBLE);

        }
    }
}
