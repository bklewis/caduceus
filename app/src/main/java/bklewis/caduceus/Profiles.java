package bklewis.caduceus;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Profiles extends ActionBarActivity implements AdapterView.OnItemClickListener {
    //variables for navigation draw
    private DrawerLayout drawerLayout=null;
    private ActionBarDrawerToggle toggle=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profiles1);
        getSupportActionBar().setTitle("Profiles");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ListView drawer = (ListView) findViewById(R.id.drawer);

        drawer.setAdapter(new ArrayAdapter<String>(
                this,
                R.layout.drawer_row,
                getResources().getStringArray(R.array.drawer_rows)));
        drawer.setOnItemClickListener(this);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle =
                new ActionBarDrawerToggle(this, drawerLayout,
                        R.drawable.ic_drawer,
                        R.string.drawer_open,
                        R.string.drawer_close);
        drawerLayout.setDrawerListener(toggle);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);

        //get all profiles from database
        DBprofileAdapter dbp = MainActivity.dbp;
        final ArrayList<ItemProfile> profiles = dbp.getAllProfiles();

        if(profiles.size() == 0) {
            emptyProfiles();
        } else {
            /*
            for(ItemProfile profile : profiles) {
                if(newMonth-1 == event.month && newYear == event.year  ) {
                    WeekViewEvent wvEvent = generateEvent(event);
                    weekViews.put(wvEvent, event.id);
                    events.add(wvEvent);
                }
            }*/


            final ListView listview = (ListView) findViewById(R.id.listview);


            //sample profiles
            final ArrayList<String> list = new ArrayList<String>();
            for (int i = 0; i < profiles.size(); ++i) {
                list.add(profiles.get(i).firstName);
            }
            final StableArrayAdapter adapter = new StableArrayAdapter(this,
                    android.R.layout.simple_list_item_1, list);
            listview.setAdapter(adapter);


            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, final View view,
                                        int position, long id) {
                    ItemProfile newProfile = profiles.get(position);
                    //create new bundle to pass to information class
                    Intent i = new Intent(Profiles.this, InformationProfile.class);
                    Bundle b = new Bundle();
                    b.putString("firstName", newProfile.firstName);
                    b.putString("lastName", newProfile.lastName);
                    b.putString("insuranceCompany", newProfile.insuranceCompany);
                    b.putString("insuranceNumber", newProfile.insurancePolicyNumber);
                    b.putString("insurancePhone", newProfile.insurancePhoneNumber);
                    String a = newProfile.allergies;
                    b.putString("allergies", newProfile.allergies);
                    b.putString("notes", newProfile.notes);
                    b.putString("height", newProfile.height);
                    b.putString("weight", newProfile.weight);
                    b.putInt("birthDay", newProfile.birthDay);
                    b.putInt("birthMonth", newProfile.birthMonth);
                    b.putInt("birthYear", newProfile.birthYear);
                    b.putLong("id", newProfile.id);
                    if (b != null) {
                        i.putExtras(b);
                    }
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    //finish();

                }

            });
        }
    }

    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options, menu);
        MenuItem item = menu.findItem(R.id.export);
        item.setVisible(false);
        return(super.onCreateOptionsMenu(menu));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item)) {
            return(true);
        }
        switch (item.getItemId()) {
            //menu options
            case android.R.id.home:
                return(true);
            case R.id.add:
                Bundle b = new Bundle();
                b.putInt("type", 0);
                Intent iNew = new Intent(this, New_Profile.class);
                iNew.putExtras(b);
                startActivity(iNew);
                this.overridePendingTransition(0, 0);
                finish();
                return true;
            case R.id.search:
                Intent iNew5 = new Intent(this, SearchResults.class);
                startActivity(iNew5);
                this.overridePendingTransition(0, 0);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }
    @Override
    public void onItemClick(AdapterView<?> listView, View row,
                            int position, long id) {
        if(position == 0) {
            Intent iNew = new Intent(this, MainActivity.class);
            iNew.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        } else if (position == 1) {
            Intent iNew = new Intent(this, Doctors.class);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        } else if (position ==3 ) {
            Intent iNew = new Intent(this, Medicines.class);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        } else if (position == 4) {
            Intent iNew = new Intent(this, Vaccines.class);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        } else if (position ==5 ){
            Intent iNew = new Intent(this, About.class);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        }
        drawerLayout.closeDrawers();
    }

    public void emptyProfiles() {
        TextView tv = (TextView)findViewById(R.id.eMessage);
        tv.setText(R.string.prof_empty);
    }

}
