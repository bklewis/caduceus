package bklewis.caduceus;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by bryce_000 on 3/31/2015.
 */
public class DBprofileAdapter extends DBAdapter{

    protected static final String TAG = "DBprofileAdapter";

    public DBprofileAdapter(Context context){
        super(context);
    }

    // Create profile
    public long createProfile(ItemProfile profile){
        ContentValues args = new ContentValues();

        args.put(KEY_PROFILE_FIRSTNAME, profile.firstName);
        args.put(KEY_PROFILE_LASTNAME, profile.lastName);
        args.put(KEY_PROFILE_BIRTH_DAY, profile.birthDay);
        args.put(KEY_PROFILE_BIRTH_MONTH, profile.birthMonth);
        args.put(KEY_PROFILE_BIRTH_YEAR, profile.birthYear);
        args.put(KEY_PROFILE_HEIGHT, profile.height);
        args.put(KEY_PROFILE_ALLERGIES, profile.allergies);
        args.put(KEY_PROFILE_WEIGHT, profile.weight);
        args.put(KEY_PROFILE_NOTES, profile.notes);
        args.put(KEY_PROFILE_INSURANCE_COMPANY, profile.insuranceCompany);
        args.put(KEY_PROFILE_INSURANCE_POLICY_NUMBER, profile.insurancePolicyNumber);
        args.put(KEY_PROFILE_INSURANCE_PHONE, profile.insurancePhoneNumber);

        profile.id = 1;
        profile.id = db.insert(TABLE_PROFILES, null, args);
        //Toast.makeText(context, "Profile " + profile.firstName + " created!", Toast.LENGTH_LONG).show();
        return profile.id;
    }

    // Read profile
    public ItemProfile readProfile(long rowID){
        ItemProfile profile = null;
        try {
            Cursor c = db.query(TABLE_PROFILES, KEYS_PROFILE, "_id = " + rowID, null, null, null, null, null);
            if (c == null) {
                //Toast.makeText(context, "readProfile Null Cursor Error: The profile you tried to access could not be located in the database.", Toast.LENGTH_LONG).show();
            } else {
                c.moveToFirst();
                String firstName = c.getString(c.getColumnIndex(KEY_PROFILE_FIRSTNAME));
                String lastName = c.getString(c.getColumnIndex(KEY_PROFILE_LASTNAME));
                int birthDay = c.getInt(c.getColumnIndex(KEY_PROFILE_BIRTH_DAY));
                int birthMonth = c.getInt(c.getColumnIndex(KEY_PROFILE_BIRTH_MONTH));
                int birthYear = c.getInt(c.getColumnIndex(KEY_PROFILE_BIRTH_YEAR));
                String height = c.getString(c.getColumnIndex(KEY_PROFILE_HEIGHT));
                String weight = c.getString(c.getColumnIndex(KEY_PROFILE_WEIGHT));
                String allergies = c.getString(c.getColumnIndex(KEY_PROFILE_ALLERGIES));
                String notes = c.getString(c.getColumnIndex(KEY_PROFILE_NOTES));
                String iCompany = c.getString(c.getColumnIndex(KEY_PROFILE_INSURANCE_COMPANY));
                String iPolicy = c.getString(c.getColumnIndex(KEY_PROFILE_INSURANCE_POLICY_NUMBER));
                String iPhone = c.getString(c.getColumnIndex(KEY_PROFILE_INSURANCE_PHONE));

                profile = new ItemProfile(rowID, firstName, lastName, birthDay, birthMonth, birthYear, height, weight, allergies, notes, iCompany, iPolicy, iPhone);
                //Toast.makeText(context, "Successfully accessed in readProfile profile=" + profile.firstName + " with id=" + profile.id, Toast.LENGTH_LONG).show();
                c.close();
            }
        } catch(Exception e){
            //Toast.makeText(context, "Failed Cursor Retrieval in readProfile: The profile you tried to access could not be located in the database.", Toast.LENGTH_LONG).show();
        }
        return profile;
    }

    // Read profile
    public ItemProfile readProfileFromCursor(Cursor c){
        ItemProfile profile = null;
        if (c == null){
            //Toast.makeText(context, "readProfileFromCursor Null Cursor Error: The profile you tried to access could not be located in the database.", Toast.LENGTH_LONG).show();
        }
        else {
            //c.moveToFirst();
            long id = c.getInt(c.getColumnIndex(KEY_ID));
            String firstName = c.getString(c.getColumnIndex(KEY_PROFILE_FIRSTNAME));
            String lastName = c.getString(c.getColumnIndex(KEY_PROFILE_LASTNAME));
            int birthDay = c.getInt(c.getColumnIndex(KEY_PROFILE_BIRTH_DAY));
            int birthMonth = c.getInt(c.getColumnIndex(KEY_PROFILE_BIRTH_MONTH));
            int birthYear = c.getInt(c.getColumnIndex(KEY_PROFILE_BIRTH_YEAR));
            String height = c.getString(c.getColumnIndex(KEY_PROFILE_HEIGHT));
            String weight = c.getString(c.getColumnIndex(KEY_PROFILE_WEIGHT));
            String allergies = c.getString(c.getColumnIndex(KEY_PROFILE_ALLERGIES));
            String notes = c.getString(c.getColumnIndex(KEY_PROFILE_NOTES));
            String iCompany = c.getString(c.getColumnIndex(KEY_PROFILE_INSURANCE_COMPANY));
            String iPolicy = c.getString(c.getColumnIndex(KEY_PROFILE_INSURANCE_POLICY_NUMBER));
            String iPhone = c.getString(c.getColumnIndex(KEY_PROFILE_INSURANCE_PHONE));

            profile = new ItemProfile(id, firstName, lastName, birthDay, birthMonth, birthYear, height, weight, allergies, notes, iCompany, iPolicy, iPhone);
            //Toast.makeText(context, "readProfileFromCursor: Successfully accessed profile " + profile.firstName + " with id=" + profile.id + "!", Toast.LENGTH_LONG).show();
        }

        return profile;
    }

    // Update profile
    public boolean updateProfile(ItemProfile profile) {
        long rowId = profile.id;
        boolean updated = false;
        if (rowId == -1) {
            // create a new profile from that profile
            createProfile(profile);
            //Toast.makeText(context, "You just created a new profile " + profile.firstName + " with id " + profile.id + " rather than updating a current profile!", Toast.LENGTH_LONG).show();
        } else {
            ContentValues args = new ContentValues();
            args.put(KEY_PROFILE_FIRSTNAME, profile.firstName);
            args.put(KEY_PROFILE_LASTNAME, profile.lastName);
            args.put(KEY_PROFILE_BIRTH_DAY, profile.birthDay);
            args.put(KEY_PROFILE_BIRTH_MONTH, profile.birthMonth);
            args.put(KEY_PROFILE_BIRTH_YEAR, profile.birthYear);
            args.put(KEY_PROFILE_HEIGHT, profile.height);
            args.put(KEY_PROFILE_WEIGHT, profile.weight);
            args.put(KEY_PROFILE_ALLERGIES, profile.allergies);
            args.put(KEY_PROFILE_NOTES, profile.notes);
            args.put(KEY_PROFILE_INSURANCE_COMPANY, profile.insuranceCompany);
            args.put(KEY_PROFILE_INSURANCE_POLICY_NUMBER, profile.insurancePolicyNumber);
            args.put(KEY_PROFILE_INSURANCE_PHONE, profile.insurancePhoneNumber);

            updated = db.update(TABLE_PROFILES, args, KEY_ID + "=" + rowId, null) != 0;
        }
        if (updated){
            //Toast.makeText(context, "Profile update successful!", Toast.LENGTH_LONG).show();
            return true;
        } else {
            //Toast.makeText(context, "Profile update unsuccessful.", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    // Delete profile
    public boolean deleteProfile(ItemProfile profile){
        //return db.delete(TABLE_PROFILES, KEY_ID + "=" + profile.id, null) > 0;
        boolean b = db.delete(TABLE_PROFILES, KEY_ID + "=" + profile.id, null) > 0;
        if (b){
            //Toast.makeText(context, "Profile " + profile.firstName + " deleted successfully.", Toast.LENGTH_LONG).show();
        } else {
            //Toast.makeText(context, "Delete unsuccessful.", Toast.LENGTH_LONG).show();
        }
        return b;
    }

    public boolean deleteProfile(long rowID){
        //return db.delete(TABLE_PROFILES, KEY_ID + "=" + profile.id, null) > 0;
        ItemProfile profile = readProfile(rowID);
        boolean b = db.delete(TABLE_PROFILES, KEY_ID + "=" + rowID, null) > 0;
        if (b){
            Toast.makeText(context, "Profile " + profile.firstName + " deleted successfully.", Toast.LENGTH_LONG).show();
        } else {
            //Toast.makeText(context, "Delete unsuccessful.  We apologize for the issue.", Toast.LENGTH_LONG).show();
        }
        return b;
    }

    public ArrayList<ItemProfile> getAllProfiles() {
        ArrayList<ItemProfile> list = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_PROFILES;
        //SQLiteDatabase db = this.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);
            try {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        ItemProfile p = readProfileFromCursor(cursor);
                        list.add(p);
                    } while (cursor.moveToNext());
                }
            } finally {
                try { cursor.close(); } catch (Exception ignore) {}
            }

        } catch(Exception e){
            //Toast.makeText(context, "There are no profiles to be loaded from the database currently.", Toast.LENGTH_SHORT).show();
        }
        //finally {    try { db.close(); } catch (Exception ignore) {}}
        return list;
    }
}
