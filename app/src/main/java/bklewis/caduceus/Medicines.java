package bklewis.caduceus;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Medicines extends ActionBarActivity implements AdapterView.OnItemClickListener {
    //variables for navigation draw
    private DrawerLayout drawerLayout = null;
    private ActionBarDrawerToggle toggle = null;
    private long selectorId = -1l;
    ProgressDialog progressDialog;
    int finished = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicines1);
        Log.i("CAD", "MAKES IT HERE");
        getSupportActionBar().setTitle("Medicines");
        int position = 0;
        if (savedInstanceState != null) {
            selectorId = savedInstanceState.getLong("selectorId");
            position = savedInstanceState.getInt("position");

        }
        ListView drawer = (ListView) findViewById(R.id.drawer);

        drawer.setAdapter(new ArrayAdapter<String>(
                this,
                R.layout.drawer_row,
                getResources().getStringArray(R.array.drawer_rows)));
        drawer.setOnItemClickListener(this);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle =
                new ActionBarDrawerToggle(this, drawerLayout,
                        R.drawable.ic_drawer,
                        R.string.drawer_open,
                        R.string.drawer_close);
        drawerLayout.setDrawerListener(toggle);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        DBprofileAdapter dbp = MainActivity.dbp;
        final ArrayList<ItemProfile> profiles = dbp.getAllProfiles();
        String[] names = new String[profiles.size() + 1];
        int j = 1;
        names[0] = "ALL";
        //select names to be displayed in selector drop down
        for (ItemProfile profile : profiles) {
            names[j] = profile.firstName;
            j++;
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Long newId = prefs.getLong("selectorId", -1l);
        if (newId != null) {
            selectorId = newId;
        }

        //code for profile selector drop down in action bar
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getBaseContext(), R.layout.custom_list_item, names);
        actionBar.setNavigationMode(android.app.ActionBar.NAVIGATION_MODE_LIST);
        actionBar.setListNavigationCallbacks(spinnerArrayAdapter, mOnNavigationListener);
        int location = prefs.getInt("location", 0);
        actionBar.setSelectedNavigationItem(location);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        //get medicines out of database
        displayMed();

    }

    //function that is called when profile dropdown item is clicked
    android.support.v7.app.ActionBar.OnNavigationListener mOnNavigationListener = new android.support.v7.app.ActionBar.OnNavigationListener() {
        // Get the same strings provided for the drop-down's ArrayAdapter
        @Override
        public boolean onNavigationItemSelected(int position, long itemId) {
            DBprofileAdapter dbp = MainActivity.dbp;
            final ArrayList<ItemProfile> profiles = dbp.getAllProfiles();
            Log.i("position is", "" + position);
            int location;
            if (position == 0) {
                selectorId = -1l;
                location = 0;
            } else {
                selectorId = profiles.get(position - 1).id;
                location = position;
            }
            //if profile filter is changed, update the information displayed
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Medicines.this);
            SharedPreferences.Editor editor = prefs.edit();
            long oldSelector = prefs.getLong("selectorId", -1l);
            if (oldSelector != selectorId) {
                recreate();
                overridePendingTransition(0, 0);
            }
            editor.putLong("selectorId", selectorId);
            editor.putInt("location", location);
            editor.apply();
            return true;
        }
    };


    public void displayMed() {
        final ListView listview = (ListView) findViewById(R.id.listview);
        DBmedAdapter dbm = MainActivity.dbm;
        final ArrayList<ItemMedicine> medicines;
        if (selectorId == -1l) {
            medicines = dbm.getAllMeds();
        } else {
            medicines = dbm.getMedsByProfile(selectorId);
        }
        if (dbm.getAllMeds().size() == 0) {
            empty();
        } else {
            final ArrayList<String> list = new ArrayList<String>();
            for (int i = 0; i < medicines.size(); ++i) {
                list.add(medicines.get(i).title);
            }
            final StableArrayAdapter adapter = new StableArrayAdapter(this,
                    android.R.layout.simple_list_item_1, list);
            listview.setAdapter(adapter);

            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, final View view,
                                        int position, long id) {
                    //when medicine clicked, start new activity to display info about medicine
                    ItemMedicine newMed = medicines.get(position);
                    Intent i = new Intent(Medicines.this, InformationMedicine.class);
                    //create bundle to be passed to Information class
                    Bundle b = new Bundle();
                    //add information to bundle
                    b.putString("medicineName", newMed.title);
                    b.putString("prescriber", newMed.prescriber);
                    b.putString("dosage", newMed.dosage);
                    b.putString("description", newMed.description);
                    b.putString("refillsLeft", newMed.refillsLeft);
                    b.putString("notes", newMed.notes);
                    b.putInt("hour", newMed.timeTakenHour);
                    b.putInt("minute", newMed.timeTakenMinute);
                    b.putInt("notificationOptions", newMed.notificationTime);
                    b.putLong("id", newMed.id);
                    b.putLong("profileId", newMed.profileID);
                    if (b != null) {
                        i.putExtras(b);
                    }
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    //finish();

                }

            });
        }
    }


    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options, menu);
        return (super.onCreateOptionsMenu(menu));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //menu options
        if (toggle.onOptionsItemSelected(item)) {
            return (true);
        }
        switch (item.getItemId()) {
            case android.R.id.home:
                return (true);
            case R.id.add:
                Bundle b = new Bundle();
                b.putInt("type", 0);
                Intent iNew = new Intent(this, NewMedicine.class);
                iNew.putExtras(b);
                startActivity(iNew);
                this.overridePendingTransition(0, 0);
                finish();
                return true;
            case R.id.export:
                DBmedAdapter dbm = MainActivity.dbm;
                final ArrayList<ItemMedicine> medicines;
                if (selectorId == -1l) {
                    medicines = dbm.getAllMeds();
                } else {
                    medicines = dbm.getMedsByProfile(selectorId);
                }
                if (medicines.size() == 0) {
                    AlertDialog.Builder builder3 = new AlertDialog.Builder(Medicines.this)
                            .setTitle("Error: No medicines to export.")
                            .setMessage("You cannot export to Google Drive since you currently have no medicines associated with this profile. ")

                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });

                    builder3.show();
                } else {
                    //allows the user to call filewriter activity which starts process of uploading to google drive
                    AlertDialog.Builder builder3 = new AlertDialog.Builder(Medicines.this)
                            .setTitle("Are you sure you want to export this data?")
                            .setMessage("Exporting this data will send it to your google drive.")

                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    Intent iNew = new Intent(Medicines.this, FileWriter.class);
                                    startActivityForResult(iNew, 1);
                                }
                            })
                            .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                    builder3.show();
                }
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }


    @Override
    public void onItemClick(AdapterView<?> listView, View row,
                            int position, long id) {
        if (position == 0) {
            Intent iNew = new Intent(this, MainActivity.class);
            iNew.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        } else if (position == 1) {
            Intent iNew = new Intent(this, Doctors.class);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        } else if (position == 2) {
            Intent iNew = new Intent(this, Profiles.class);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        } else if (position == 4) {
            Intent iNew = new Intent(this, Vaccines.class);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        } else if (position == 5) {
            Intent iNew = new Intent(this, About.class);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        }
        drawerLayout.closeDrawers();
    }

    public void empty() {
        TextView tv = (TextView) findViewById(R.id.eMessage);
        tv.setText(R.string.meds_empty);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //create bundle to be saved in case of rotation
        super.onSaveInstanceState(outState);
        outState.putLong("selectorId", selectorId);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == RESULT_OK){
                Toast.makeText(this, "Your information was uploaded in a spreadsheet to your Google Drive account!", Toast.LENGTH_LONG).show();
            }
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Unfortunately, your information could not be uploaded to Google Drive.", Toast.LENGTH_LONG).show();
            }
        }
    }

}
