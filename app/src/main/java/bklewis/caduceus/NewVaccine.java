package bklewis.caduceus;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Calendar;


public class NewVaccine extends ActionBarActivity implements View.OnClickListener {
    //views in new vaccine
    private Button ok;
    private Button cancel;
    private Button profilePicker;
    private Button btnChangeDate;
    private Button btnChangeDate2;
    private String name = null;
    private String dosage = null;
    private String notes = null;
    private int expDay = 0;
    private int expMonth = 0;
    private int expYear = 0;
    private int dateDay = 0;
    private int dateYear= 0;
    private int dateMonth = 0;
    // variables to store the selected date
    private int mSelectedYear;
    private int mSelectedMonth;
    private int mSelectedDay;
    String dateString1;
    String dateString2;
    boolean dateSet = false;
    long profileId = -1;
    long id = -1l;
    Bundle b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_vaccine);
        getSupportActionBar().setTitle("New Vaccine");
        b = getIntent().getExtras();
        // retrieve views
        this.btnChangeDate = (Button) findViewById(R.id.vaccineDate);
        this.btnChangeDate2 = (Button) findViewById(R.id.vaccineDate2);
        this.ok = (Button) findViewById(R.id.ok_button);
        this.cancel = (Button) findViewById(R.id.cancel_button);
        this.btnChangeDate.setOnClickListener(this);
        this.btnChangeDate2.setOnClickListener(this);
        this.ok.setOnClickListener(this);
        this.cancel.setOnClickListener(this);
        // initialize the current date
        Calendar calendar = Calendar.getInstance();
        this.mSelectedYear = calendar.get(Calendar.YEAR);
        this.mSelectedMonth = calendar.get(Calendar.MONTH);
        this.mSelectedDay = calendar.get(Calendar.DAY_OF_MONTH);
        this.profilePicker = (Button) findViewById(R.id.profilePicker);
        this.profilePicker.setOnClickListener(this);
        //retrieve date if already set
        if(savedInstanceState != null) {
            String date1 = savedInstanceState.getString("date1");
            String date2 = savedInstanceState.getString("date2");
            if (savedInstanceState.getString("date1") != null) {
                dateString1 = date1;
                btnChangeDate.setText(date1);
            }
            if (savedInstanceState.getString("date2") != null) {
                dateString2 = date2;
                btnChangeDate2.setText(date2);
            }
        }
        if(b.getInt("type") == 1) {
            getEdits();
        }
        //check to see if profile view is set and set new profile to that
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Long newId = prefs.getLong("selectorId", -1l);
        if(newId != null) {
            if(newId != -1l) {
                DBprofileAdapter dbp = MainActivity.dbp;
                final ArrayList<ItemProfile> profiles = dbp.getAllProfiles();
                for (ItemProfile profile : profiles) {
                    if (profile.id == newId) {
                        profileId = profile.id;
                        profilePicker.setText(profile.firstName);
                    }
                }
            } else {
                profilePicker.setText("ALL");
                profileId = -1l;
            }



        }

    }

    // CallBacks for date and time pickers
    private DatePickerDialog.OnDateSetListener mOnDateSetListener1 = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // update the current variables ( year, month and day)
            mSelectedDay = dayOfMonth;
            mSelectedMonth = monthOfYear;
            mSelectedYear = year;
            dateDay = dayOfMonth;
            dateMonth = monthOfYear;
            dateYear = year;
            dateSet = true;
            // update txtDate with the selected date
            updateDateUI(0);
        }
    };
    // CallBacks for date and time pickers
    private DatePickerDialog.OnDateSetListener mOnDateSetListener2 = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // update the current variables ( year, month and day)
            mSelectedDay = dayOfMonth;
            mSelectedMonth = monthOfYear;
            mSelectedYear = year;
            expDay = dayOfMonth;
            expMonth = monthOfYear;
            expYear = year;
            dateSet = true;
            // update txtDate with the selected date
            updateDateUI(1);
        }
    };
    private void updateDateUI(int choose) {
        //update the UI for the date
        String month = ((mSelectedMonth+1) > 9) ? ""+(mSelectedMonth+1): "0"+(mSelectedMonth+1) ;
        String day = ((mSelectedDay) < 10) ? "0"+mSelectedDay: ""+mSelectedDay ;
        if (choose == 0) {
            dateString1 = month+"/"+day+"/"+mSelectedYear;
            btnChangeDate.setText(dateString1);
        } else {
            dateString2 = month+"/"+day+"/"+mSelectedYear;
            btnChangeDate2.setText(dateString2);
        }
    }
    // initialize the DatePickerDialog
    private DatePickerDialog showDatePickerDialog(int initialYear, int initialMonth, int initialDay, DatePickerDialog.OnDateSetListener listener) {
        DatePickerDialog dialog = new DatePickerDialog(this, listener, initialYear, initialMonth, initialDay);
        dialog.show();
        return dialog;
    }
    @Override
    public void onClick(View v) {
        //set up cases for different buttons that are clicked
        switch (v.getId()) {
            case R.id.vaccineDate:
                showDatePickerDialog(mSelectedYear, mSelectedMonth, mSelectedDay, mOnDateSetListener1);
                break;
            case R.id.vaccineDate2:
                showDatePickerDialog(mSelectedYear, mSelectedMonth, mSelectedDay, mOnDateSetListener2);
                break;
            case R.id.ok_button:
                setStrings();
                if(name != null) {
                    if (b.getInt("type") != 1) {
                        //creating event
                        ItemVaccine vaccine = new ItemVaccine(-1, name, dosage, dateDay, dateMonth, dateYear, expDay, expMonth, expYear, notes, profileId);
                        DBvaccineAdapter dbv = MainActivity.dbv;
                        dbv.createVaccine(vaccine);
                    } else {
                        //updating event
                        Long id = b.getLong("id");
                        ItemVaccine vaccine = new ItemVaccine(id, name, dosage, dateDay, dateMonth, dateYear, expDay, expMonth, expYear, notes, profileId);
                        DBvaccineAdapter dbv = MainActivity.dbv;
                        dbv.updateVaccine(vaccine);
                    }
                    Intent iNew = new Intent(this, Vaccines.class);
                    startActivity(iNew);
                    iNew.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    this.overridePendingTransition(0, 0);
                    finish();
                    break;
                } else {
                    AlertDialog.Builder builder3 = new AlertDialog.Builder(NewVaccine.this)
                            .setTitle("Required Information Missing")
                            .setMessage("Please enter a vaccine name for your Vaccine to be saved.")

                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {


                                }
                            });

                    builder3.show();
                    break;
                }
            case R.id.profilePicker:
                DBprofileAdapter dbp = MainActivity.dbp;
                final ArrayList<ItemProfile> profiles = dbp.getAllProfiles();
                final String[] names  = new String[profiles.size()+1];
                int i = 1;
                names[0] = "ALL";
                for(ItemProfile profile : profiles){
                    names[i] = profile.firstName;
                    i++;
                }
                AlertDialog.Builder builder2 = new AlertDialog.Builder(NewVaccine.this)
                        .setTitle("Choose a Profile")
                        .setItems(names, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                profilePicker.setText(names[which]);
                                if(which > 0) {
                                    profileId = profiles.get(which - 1).id;
                                } else {
                                    profileId = -1l;
                                }
                            }
                        });
                builder2.show();
                break;
            case R.id.cancel_button:
                AlertDialog.Builder builder3 = new AlertDialog.Builder(NewVaccine.this)
                        .setTitle("Are you sure you want to leave this page?")
                        .setMessage("If you leave this page, your information will not be saved.")

                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                Intent iNew2 = new Intent(NewVaccine.this, Vaccines.class);
                                startActivity(iNew2);
                                overridePendingTransition(0, 0);
                                finish();

                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                builder3.show();
                break;
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //create bundle to be saved in case of rotation
        super.onSaveInstanceState(outState);
        outState.putString("date1", dateString1);
        outState.putString("date2", dateString2);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_vaccine, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setStrings() {
       EditText title = (EditText)findViewById(R.id.vaccineName);
       name = title.getText().toString();
        EditText dose = (EditText)findViewById(R.id.vaccineDose);
        dosage = dose.getText().toString();
        EditText note = (EditText)findViewById(R.id.vaccineNotes);
        notes = note.getText().toString();
    }

    public void getEdits() {
        id = b.getLong("id");
        name = b.getString("title");
        EditText title = (EditText)findViewById(R.id.vaccineName);
        title.setText(name);
        dosage = b.getString("dosage");
        Log.i("dosage", dosage);
        if(dosage != null) {
            EditText dose = (EditText)findViewById(R.id.vaccineDose);
            dose.setText(dosage);
        }
        notes = b.getString("notes");
        if(notes != null) {
            EditText note = (EditText)findViewById(R.id.vaccineNotes);
            note.setText(notes);
        }
        if(b.getInt("expDay") != 0 && b.getInt("expMonth") != 0 && b.getInt("expYear") != 0) {
            mSelectedDay = b.getInt("expDay");
            mSelectedMonth = b.getInt("expMonth");
            mSelectedYear = b.getInt("expYear");
            expDay = b.getInt("expDay");
            expMonth = b.getInt("expMonth");
            expYear = b.getInt("expYear");
            dateSet = true;
            // update txtDate with the selected date
            updateDateUI(1);
        }
        if(b.getInt("dateDay") != 0 && b.getInt("dateMonth") != 0 && b.getInt("dateYear") != 0) {
            mSelectedDay = b.getInt("dateDay");
            mSelectedMonth = b.getInt("dateMonth");
            mSelectedYear = b.getInt("dateYear");
            dateDay = b.getInt("dateDay");
            dateMonth = b.getInt("dateMonth");
            dateYear = b.getInt("dateYear");
            dateSet = true;
            // update txtDate with the selected date
            updateDateUI(0);
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Long newId = prefs.getLong("selectorId", -1l);
        if(newId != null) {
            if(newId != -1l) {
                DBprofileAdapter dbp = MainActivity.dbp;
                final ArrayList<ItemProfile> profiles = dbp.getAllProfiles();
                for (ItemProfile profile : profiles) {
                    if (profile.id == newId) {
                        profileId = profile.id;
                        profilePicker.setText(profile.firstName);
                    }
                }
            } else {
                profilePicker.setText("ALL");
                profileId = -1l;
            }



        }

    }
}
