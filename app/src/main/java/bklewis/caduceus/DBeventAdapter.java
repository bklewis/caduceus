package bklewis.caduceus;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by bryce_000 on 3/28/2015.
 */
public class DBeventAdapter extends DBAdapter{

    protected static final String TAG = "DBeventAdapter";

    public DBeventAdapter(Context context){
        super(context);
    }

    // Create event
    public long createEvent(ItemEvent event){
        ContentValues args = new ContentValues();
        args.put(KEY_EVENT_TITLE, event.title);
        args.put(KEY_EVENT_COLOR, event.color);
        args.put(KEY_EVENT_DAY, event.day);
        args.put(KEY_EVENT_MONTH, event.month);
        args.put(KEY_EVENT_YEAR, event.year);
        args.put(KEY_EVENT_START_HOUR, event.sHour);
        args.put(KEY_EVENT_START_MINUTE, event.sMinute);
        args.put(KEY_EVENT_END_HOUR, event.eHour);
        args.put(KEY_EVENT_END_MINUTE, event.eMinute);
        args.put(KEY_EVENT_LOCATION, event.location);
        args.put(KEY_EVENT_NOTES, event.notes);
        args.put(KEY_EVENT_PROFILE_ID, event.profileID);

        event.id = db.insert(TABLE_EVENTS, null, args);
        //Toast.makeText(context, "Event " + event.title + " created!" + event.id, Toast.LENGTH_LONG).show();
        return event.id;
    }

    // Read event
    public ItemEvent readEvent(long rowID){
        ItemEvent event = null;
        try {
            Cursor c = db.query(TABLE_EVENTS, KEYS_EVENT, "_id = " + rowID, null, null, null, null, null);
            if (c == null) {
                //Toast.makeText(context, "readEvent Null Cursor Error: The event you tried to access could not be located in the database.", Toast.LENGTH_LONG).show();
            } else {
                c.moveToFirst();
                String title = c.getString(c.getColumnIndex(KEY_EVENT_TITLE));
                String color = c.getString(c.getColumnIndex(KEY_EVENT_COLOR));
                int year = c.getInt(c.getColumnIndex(KEY_EVENT_YEAR));
                int month = c.getInt(c.getColumnIndex(KEY_EVENT_MONTH));
                int day = c.getInt(c.getColumnIndex(KEY_EVENT_DAY));
                int sHour = c.getInt(c.getColumnIndex(KEY_EVENT_START_HOUR));
                int sMinute = c.getInt(c.getColumnIndex(KEY_EVENT_START_MINUTE));
                int eHour = c.getInt(c.getColumnIndex(KEY_EVENT_END_HOUR));
                int eMinute = c.getInt(c.getColumnIndex(KEY_EVENT_END_MINUTE));
                long profileID = c.getLong(c.getColumnIndex(KEY_EVENT_PROFILE_ID));
                String location = c.getString(c.getColumnIndex(KEY_EVENT_LOCATION));
                String notes = c.getString(c.getColumnIndex(KEY_EVENT_NOTES));

                event = new ItemEvent(rowID, title, color, year, month, day, sHour, sMinute, eHour, eMinute, location, notes, profileID);
                //Toast.makeText(context, "Successfully accessed in readEvent event=" + event.title + " with id=" + event.id + ", col=" + event.notes, Toast.LENGTH_LONG).show();
                c.close();
            }
        } catch(Exception e){
            //Toast.makeText(context, "Failed Cursor Retrieval in readEvent: The event you tried to access could not be located in the database.", Toast.LENGTH_LONG).show();
        }
        return event;
    }

    // Read event
    public ItemEvent readEventFromCursor(Cursor c){
        ItemEvent event = null;
        if (c == null){
            //Toast.makeText(context, "readEventFromCursor Null Cursor Error: The event you tried to access could not be located in the database.", Toast.LENGTH_LONG).show();
        }
        else {
            //c.moveToFirst();
            long id = c.getLong(c.getColumnIndex(KEY_ID));
            String title = c.getString(c.getColumnIndex(KEY_EVENT_TITLE));
            String color = c.getString(c.getColumnIndex(KEY_EVENT_COLOR));
            int year = c.getInt(c.getColumnIndex(KEY_EVENT_YEAR));
            int month = c.getInt(c.getColumnIndex(KEY_EVENT_MONTH));
            int day = c.getInt(c.getColumnIndex(KEY_EVENT_DAY));
            int sHour = c.getInt(c.getColumnIndex(KEY_EVENT_START_HOUR));
            int sMinute = c.getInt(c.getColumnIndex(KEY_EVENT_START_MINUTE));
            int eHour = c.getInt(c.getColumnIndex(KEY_EVENT_END_HOUR));
            int eMinute = c.getInt(c.getColumnIndex(KEY_EVENT_END_MINUTE));
            long profileID = c.getLong(c.getColumnIndex(KEY_EVENT_PROFILE_ID));
            String location = c.getString(c.getColumnIndex(KEY_EVENT_LOCATION));
            String notes = c.getString(c.getColumnIndex(KEY_EVENT_NOTES));

            event = new ItemEvent(id, title, color, year, month, day, sHour, sMinute, eHour, eMinute, location, notes, profileID);
            //Toast.makeText(context, "readEventFromCursor: Successfully accessed event " + event.title + " with id=" + event.id + "!", Toast.LENGTH_LONG).show();
        }
        return event;
    }

    // Update event
    public boolean updateEvent(ItemEvent event) {
        long rowId = event.id;
        boolean updated = false;
        if (rowId == -1) {
            // create a new event from that event
            createEvent(event);
            Toast.makeText(context, "You just created a new event: " + event.title + "!", Toast.LENGTH_LONG).show();
        } else {
            ContentValues args = new ContentValues();
            args.put(KEY_EVENT_TITLE, event.title);
            args.put(KEY_EVENT_COLOR, event.color);
            args.put(KEY_EVENT_DAY, event.day);
            args.put(KEY_EVENT_MONTH, event.month);
            args.put(KEY_EVENT_YEAR, event.year);
            args.put(KEY_EVENT_START_HOUR, event.sHour);
            args.put(KEY_EVENT_START_MINUTE, event.sMinute);
            args.put(KEY_EVENT_END_HOUR, event.eHour);
            args.put(KEY_EVENT_END_MINUTE, event.eMinute);
            args.put(KEY_EVENT_LOCATION, event.location);
            args.put(KEY_EVENT_NOTES, event.notes);
            args.put(KEY_ID_PROFILE, event.profileID);
            updated = db.update(TABLE_EVENTS, args, KEY_ID + "=" + rowId, null) != 0;
        }
        if (updated){
            //Toast.makeText(context, "Update successful!", Toast.LENGTH_LONG).show();
            return true;
        } else {
            //Toast.makeText(context, "Update unsuccessful.  We apologize for the issue.", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    // Delete event
    public boolean deleteEvent(ItemEvent event){
        //return db.delete(TABLE_EVENTS, KEY_ID + "=" + event.id, null) > 0;
        boolean b = db.delete(TABLE_EVENTS, KEY_ID + "=" + event.id, null) > 0;
        if (b){
            Toast.makeText(context, "Event deleted successfully.", Toast.LENGTH_LONG).show();
        } else {
            //Toast.makeText(context, "Delete unsuccessful.", Toast.LENGTH_LONG).show();
        }
        return b;
    }

    public boolean deleteEvent(long rowID){
        //return db.delete(TABLE_EVENTS, KEY_ID + "=" + event.id, null) > 0;
        ItemEvent event = readEvent(rowID);
        boolean b = db.delete(TABLE_EVENTS, KEY_ID + "=" + rowID, null) > 0;
        if (b){
            Toast.makeText(context, "Event " + event.title + " deleted successfully.", Toast.LENGTH_LONG).show();
        } else {
           // Toast.makeText(context, "Delete unsuccessful.", Toast.LENGTH_LONG).show();
        }
        return b;
    }

    public ArrayList<ItemEvent> getAllEvents() {
        ArrayList<ItemEvent> list = new ArrayList<ItemEvent>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EVENTS;

        //SQLiteDatabase db = this.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);
            try {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        ItemEvent e = readEventFromCursor(cursor);
                        list.add(e);
                    } while (cursor.moveToNext());
                }
            } finally {
                try { cursor.close(); } catch (Exception ignore) {}
            }

        } catch(Exception e){
           // Toast.makeText(context, "There are no events to be loaded from the database currently.", Toast.LENGTH_SHORT).show();
        }
        //finally {    try { db.close(); } catch (Exception ignore) {}}
        return list;
    }

    public ArrayList<ItemEvent> getEventsByProfile(long profileID) {
        ArrayList<ItemEvent> list = new ArrayList<ItemEvent>();

        // Select All Query
        //String selectQuery = "SELECT * FROM " + TABLE_EVENTS + "WHERE profile = " + profileID;

        String[] args = new String[] {Long.toString(profileID)};
        //SQLiteDatabase db = this.getReadableDatabase();
        try {
            //Cursor cursor = db.rawQuery(selectQuery, null);
            Cursor cursor = db.query(TABLE_EVENTS, KEYS_EVENT, "profile = ?", args, null, null, null);
            try {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        ItemEvent e = readEventFromCursor(cursor);
                        list.add(e);
                    } while (cursor.moveToNext());
                }
            } finally {
                try { cursor.close(); } catch (Exception ignore) {}
            }

        } catch(Exception e){
           // Toast.makeText(context, "There are no events to be loaded from the database currently.", Toast.LENGTH_SHORT).show();
        }
        //finally {    try { db.close(); } catch (Exception ignore) {}}
        return list;
    }
}