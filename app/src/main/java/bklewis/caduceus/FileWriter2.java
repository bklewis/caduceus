package bklewis.caduceus;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.MetadataChangeSet;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

/**
 * An activity to illustrate how to create a file.
 */
public class FileWriter2 extends BaseGoogleActivity {

    private static final String TAG = "CreateFileActivity";

    @Override
    public void onConnected(Bundle connectionHint) {
        super.onConnected(connectionHint);
        // create new contents resource
        Drive.DriveApi.newDriveContents(getGoogleApiClient())
                .setResultCallback(driveContentsCallback);
    }

    final private ResultCallback<DriveApi.DriveContentsResult> driveContentsCallback = new
            ResultCallback<DriveApi.DriveContentsResult>() {
                @Override
                public void onResult(DriveApi.DriveContentsResult result) {
                    if (!result.getStatus().isSuccess()) {
                        showMessage("Error while trying to create new file contents");
                        return;
                    }
                    final DriveContents driveContents = result.getDriveContents();

                    // Perform I/O off the UI thread.
                    new Thread() {
                        @Override
                        public void run() {
                            // write content to DriveContents
                            OutputStream outputStream = driveContents.getOutputStream();
                            Writer writer = new OutputStreamWriter(outputStream);
                            try {
                                String header = "Name of Vaccine, Profile,Dosage, Date of Shot, Expiration Date, Notes\n";
                                writer.write(header);
                                // Write the content
                                DBvaccineAdapter dbv = MainActivity.dbv;
                                final ArrayList<ItemVaccine> vaccines;
                                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(FileWriter2.this);
                                String profileName = "ALL";
                                long oldSelector = prefs.getLong("selectorId", -1l);
                                if (oldSelector == -1l) {
                                    vaccines = dbv.getAllVaccines();
                                } else {
                                    vaccines = dbv.getVaccineByProfile(oldSelector);
                                }
                                DBprofileAdapter dbp = MainActivity.dbp;
                                final ArrayList<ItemProfile> profiles = dbp.getAllProfiles();
                                String date;
                                String expir;
                                String line;
                                for (int i = 0; i < vaccines.size(); ++i) {
                                    ItemVaccine vac = vaccines.get(i);
                                    if (vac.profileID != -1l) {
                                        for (ItemProfile profile : profiles) {
                                            if (vac.profileID == profile.id) {
                                                profileName = profile.firstName;
                                            }
                                        }
                                    } else {
                                        profileName = "ALL";
                                    }
                                    if(vac.dateMonth != 0 || vac.dateDay != 0 || vac.dateYear != 0) {
                                        date = vac.dateMonth + "/" + vac.dateDay + "/" + vac.dateYear;
                                    } else {
                                        date = "";
                                    }
                                    if (vac.expMonth != 0 || vac.expDay != 0 || vac.expYear != 0) {
                                        expir = vac.expMonth + "/" + vac.expDay + "/" + vac.expYear;
                                    } else {
                                        expir = "";
                                    }
                                    line = new String(vac.title + "," + profileName + "," + vac.dosage + "," + date + "," + expir + "," + vac.notes +"\n");
                                    writer.write(line);
                                }
                                writer.close();
                            } catch (IOException e) {
                                Log.e(TAG, e.getMessage());
                            }

                            Long tsLong = System.currentTimeMillis() / 1000;
                            String ts = tsLong.toString();
                            MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
                                    .setTitle("CaduceusVaccines" + ts)
                                    .setMimeType("text/csv")
                                    .setStarred(true).build();

                            // create a file on root folder
                            Drive.DriveApi.getRootFolder(getGoogleApiClient())
                                    .createFile(getGoogleApiClient(), changeSet, driveContents)
                                    .setResultCallback(fileCallback);
                        }
                    }.start();
                }
            };

    final private ResultCallback<DriveFolder.DriveFileResult> fileCallback = new
            ResultCallback<DriveFolder.DriveFileResult>() {
                @Override
                public void onResult(DriveFolder.DriveFileResult result) {
                    if (!result.getStatus().isSuccess()) {
                        Intent returnIntent = new Intent();
                        setResult(RESULT_CANCELED, returnIntent);
                        finish();
                    }
                    Intent returnIntent = new Intent();
                    setResult(RESULT_OK,returnIntent);
                    finish();
                }
            };
}


