package bklewis.caduceus;

/**
 * Created by bryce_000 on 3/31/2015.
 */

public class ItemProfile {

    protected long id = -1L;
    protected String firstName;
    protected String lastName;
    protected int birthDay;
    protected int birthMonth;
    protected int birthYear;
    protected String height;
    protected String weight;
    protected String allergies;
    protected String notes;
    protected String insuranceCompany;
    protected String insurancePolicyNumber;
    protected String insurancePhoneNumber;

    public ItemProfile(long id, String firstName, String lastName, int birthDay, int birthMonth, int birthYear, String height, String weight, String allergies, String notes, String insuranceCompany, String insurancePolicyNumber, String insurancePhoneNumber) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDay = birthDay;
        this.birthMonth = birthMonth;
        this.birthYear = birthYear;
        this.height = height;
        this.weight = weight;
        this.allergies = allergies;
        this.notes = notes;
        this.insuranceCompany = insuranceCompany;
        this.insurancePolicyNumber = insurancePolicyNumber;
        this.insurancePhoneNumber = insurancePhoneNumber;
    }
}
