package bklewis.caduceus;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Doctors extends ActionBarActivity implements AdapterView.OnItemClickListener {
    private DrawerLayout drawerLayout=null;
    private ActionBarDrawerToggle toggle=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctors1);
        getSupportActionBar().setTitle("Doctors");
        ListView drawer=(ListView)findViewById(R.id.drawer);

        drawer.setAdapter(new ArrayAdapter<String>(
                this,
                R.layout.drawer_row,
                getResources().getStringArray(R.array.drawer_rows)));
        drawer.setOnItemClickListener(this);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle=
                new ActionBarDrawerToggle(this, drawerLayout,
                        R.drawable.ic_drawer,
                        R.string.drawer_open,
                        R.string.drawer_close);
        drawerLayout.setDrawerListener(toggle);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        //get all doctors from database
        DBdocAdapter dbd = MainActivity.dbd;
        final ArrayList<ItemDoctor> doctors = dbd.getAllDocs();

        if(doctors.size() == 0) {
            emptyDoctors();
        } else {

            final ListView listview = (ListView) findViewById(R.id.listview);

            //sample profiles
            final ArrayList<String> list = new ArrayList<>();
            for (int i = 0; i < doctors.size(); ++i) {
                list.add("Dr. " + doctors.get(i).lastName);
            }
            final StableArrayAdapter adapter = new StableArrayAdapter(this,
                    android.R.layout.simple_list_item_1, list);
            listview.setAdapter(adapter);


            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, final View view,
                                        int position, long id) {
                    ItemDoctor newProfile = doctors.get(position);
                    //create new bundle to pass to information class
                    Intent i = new Intent(Doctors.this, InformationDoctor.class);
                    Bundle b = new Bundle();
                    b.putString("firstName", newProfile.firstName);
                    b.putString("lastName", newProfile.lastName);
                    b.putString("specialty", newProfile.specialty);
                    b.putString("hospital", newProfile.hospital);
                    Log.i("hospital", newProfile.hospital);
                    b.putString("workPhone", newProfile.phone1);
                    b.putString("personalPhone", newProfile.phone2);
                    b.putString("notes", newProfile.notes);
                    b.putString("fax", newProfile.fax);
                    b.putString("email", newProfile.email);
                    b.putString("address", newProfile.address);
                    b.putLong("id", newProfile.id);
                    if (b != null) {
                        i.putExtras(b);
                    }
                    startActivity(i);
                    //finish();
                }

            });
        }
    }


    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.options, menu);
        MenuItem item = menu.findItem(R.id.export);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (toggle.onOptionsItemSelected(item)) {
            return(true);
        }

        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {
            //start new activity depending on which button is clicked
            case android.R.id.home:
                return true;
            case R.id.add:
                Bundle b = new Bundle();
                b.putInt("type", 0);
                Intent iNew = new Intent(this, NewDoctor.class);
                if(b != null) {
                    iNew.putExtras(b);
                }
                startActivity(iNew);
                this.overridePendingTransition(0, 0);
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }


    @Override
    public void onItemClick(AdapterView<?> listView, View row,
                            int position, long id) {
        if(position == 0) {
            Intent iNew = new Intent(this, MainActivity.class);
            iNew.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        } else if(position == 2) {
            Intent iNew = new Intent(this, Profiles.class);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        } else if (position == 3) {
            Intent iNew = new Intent(this, Medicines.class);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        } else if (position == 4) {
            Intent iNew = new Intent(this, Vaccines.class);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        } else if (position == 5) {
            Intent iNew = new Intent(this, About.class);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        }
        drawerLayout.closeDrawers();
    }
    public void emptyDoctors() {
        TextView tv = (TextView)findViewById(R.id.eMessage);
        tv.setText(R.string.docs_empty);
    }
}
