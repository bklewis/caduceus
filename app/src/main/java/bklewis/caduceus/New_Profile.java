package bklewis.caduceus;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;

public class New_Profile extends ActionBarActivity implements View.OnClickListener {

    //views
    private Button ok;
    private Button cancel;
    private Button btnChangeDate;

    // variables to store the selected date
    private int mSelectedYear;
    private int mSelectedMonth;
    private int mSelectedDay;
    String dateString;
    boolean dateSet = false;
    String firstName = null;
    String lastName = null;
    String weight = null;
    String height = null;
    String allergies = null;
    String notes = null;
    String insurancePhone = null;
    String insuranceCompany = null;
    String insuranceNumber = null;
    Bundle b;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new__profile);
        getSupportActionBar().setTitle("New Profile");
        b = getIntent().getExtras();
        // retrieve views
        this.btnChangeDate = (Button) findViewById(R.id.btn_show_date_picker);
        this.ok = (Button) findViewById(R.id.ok_button);
        this.cancel = (Button) findViewById(R.id.cancel_button);
        this.btnChangeDate.setOnClickListener(this);
        this.ok.setOnClickListener(this);
        this.cancel.setOnClickListener(this);

        // initialize the current date
        Calendar calendar = Calendar.getInstance();
        this.mSelectedYear = calendar.get(Calendar.YEAR);
        this.mSelectedMonth = calendar.get(Calendar.MONTH);
        this.mSelectedDay = calendar.get(Calendar.DAY_OF_MONTH);
        firstName = b.getString("firstName");
        lastName = b.getString("lastName");
        allergies = b.getString("allergies");
        insuranceCompany = b.getString("insuranceCompany");
        insuranceNumber = b.getString("insuranceNumber");
        insurancePhone = b.getString("insurancePhone");
        weight = b.getString("weight");
        height = b.getString("height");
        notes= b.getString("notes");
        //retrieve date if already set
        if(savedInstanceState != null) {
            String date = savedInstanceState.getString("year");
            if (savedInstanceState.getString("year") != null) {
                dateString = date;
                btnChangeDate.setText(date);
            }
        }
        if(b.getInt("type") == 1) {
            getEdits();
        }
    }

    // CallBacks for date and time pickers
    private DatePickerDialog.OnDateSetListener mOnDateSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // update the current variables ( year, month and day)
            mSelectedDay = dayOfMonth;
            mSelectedMonth = monthOfYear;
            mSelectedYear = year;
            dateSet = true;
            // update txtDate with the selected date
            updateDateUI();
        }
    };

    private void updateDateUI() {
        //update the UI for the date
        String month = ((mSelectedMonth + 1) > 9) ? "" + (mSelectedMonth+1) : "0" + (mSelectedMonth+1);
        String day = ((mSelectedDay) < 10) ? "0" + mSelectedDay : "" + mSelectedDay;
        dateString = month + "/" + day + "/" + mSelectedYear;
        btnChangeDate.setText(dateString);
    }

    // initialize the DatePickerDialog
    private DatePickerDialog showDatePickerDialog(int initialYear, int initialMonth, int initialDay, DatePickerDialog.OnDateSetListener listener) {
        DatePickerDialog dialog = new DatePickerDialog(this, listener, initialYear, initialMonth, initialDay);
        dialog.show();
        return dialog;
    }

    @Override
    public void onClick(View v) {
        //set up cases for different buttons that are clicked
        switch (v.getId()) {
            case R.id.btn_show_date_picker:
                showDatePickerDialog(mSelectedYear, mSelectedMonth, mSelectedDay, mOnDateSetListener);
                break;
            case R.id.ok_button:
                setStrings();

                // Database profile adapter
                DBprofileAdapter dbp = MainActivity.dbp;

                if(firstName != null) {
                    //updating an event
                    if(b.getInt("type") == 1) {
                        Long ids = b.getLong("id");
                        ItemProfile newProfile = new ItemProfile(ids, firstName, lastName, mSelectedDay, mSelectedMonth, mSelectedYear, height, weight, allergies, notes, insuranceCompany, insuranceNumber, insurancePhone);
                        dbp.updateProfile(newProfile);
                    } else {
                        //creating completely new event
                        ItemProfile newProfile = new ItemProfile(-1L, firstName, lastName, mSelectedDay, mSelectedMonth, mSelectedYear, height, weight, allergies, notes, insuranceCompany, insuranceNumber, insurancePhone);
                        dbp.createProfile(newProfile);
                    }
                    Intent iNew = new Intent(this, Profiles.class);
                    iNew.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(iNew);
                    this.overridePendingTransition(0, 0);
                    finish();
                } else {
                    AlertDialog.Builder builder3 = new AlertDialog.Builder(New_Profile.this)
                            .setTitle("Required Information Missing")
                            .setMessage("You need to enter a first name for your profile to be saved.")

                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                    builder3.show();
                }
                break;
            case R.id.cancel_button:
                AlertDialog.Builder builder3 = new AlertDialog.Builder(New_Profile.this)
                        .setTitle("Are you sure you want to leave this page?")
                        .setMessage("If you leave this page, your information will not be saved.")

                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                Intent iNew2 = new Intent(New_Profile.this, Profiles.class);
                                startActivity(iNew2);
                                overridePendingTransition(0, 0);
                                finish();

                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                builder3.show();
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //create bundle to be saved in case of rotation
        super.onSaveInstanceState(outState);
        outState.putString("year", dateString);
    }

    protected void setStrings() {
        EditText firstNameText = (EditText) findViewById(R.id.firstName);
        firstName = firstNameText.getText().toString();
        EditText lastNameText = (EditText) findViewById(R.id.lastName);
        lastName = lastNameText.getText().toString();
        EditText heightText = (EditText) findViewById(R.id.height);
        height = heightText.getText().toString();
        EditText weightText = (EditText) findViewById(R.id.weight);
        weight = weightText.getText().toString();
        EditText allergiesText = (EditText) findViewById(R.id.allergies);
        allergies = allergiesText.getText().toString();
        EditText notesText = (EditText) findViewById(R.id.notes);
        notes = notesText.getText().toString();
        EditText insuranceText1 = (EditText) findViewById(R.id.insuranceCompany);
        insuranceCompany = insuranceText1.getText().toString();
        EditText insuranceText2 = (EditText) findViewById(R.id.insuranceNumber);
        insuranceNumber = insuranceText2.getText().toString();
        EditText insuranceText3 = (EditText) findViewById(R.id.insurancePhone);
        insurancePhone = insuranceText3.getText().toString();

    }
    protected void getEdits() {
        EditText firstNameText = (EditText) findViewById(R.id.firstName);
        firstNameText.setText(firstName);
        if(b.getInt("birthDay") != 0 && b.getInt("birthMonth") !=0 && b.getInt("birthYear")!= 0) {
            mSelectedYear = b.getInt("birthYear");
            mSelectedMonth = b.getInt("birthMonth");
            mSelectedDay = b.getInt("birthDay");
            updateDateUI();

        }
        if(lastName != null) {
            EditText lastNameText = (EditText) findViewById(R.id.lastName);
            lastNameText.setText(lastName);
        } if(height != null) {
            EditText heightText = (EditText) findViewById(R.id.height);
            heightText.setText(height);
        } if (weight != null) {
            EditText weightText = (EditText) findViewById(R.id.weight);
            weightText.setText(weight);
        } if (allergies != null) {
            EditText allergiesText = (EditText) findViewById(R.id.allergies);
            allergiesText.setText(allergies);
        } if(notes != null) {
            EditText notesText = (EditText) findViewById(R.id.notes);
            notesText.setText(notes);
        } if (insuranceCompany != null) {
            EditText insuranceText1 = (EditText) findViewById(R.id.insuranceCompany);
            insuranceText1.setText(insuranceCompany);
        } if(insuranceNumber != null) {
            EditText insuranceText2 = (EditText) findViewById(R.id.insuranceNumber);
            insuranceText2.setText(insuranceNumber);
        } if(insurancePhone != null ) {
            EditText insuranceText3 = (EditText) findViewById(R.id.insurancePhone);
            insuranceText3.setText(insurancePhone);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new__profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
