package bklewis.caduceus;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class InformationVaccine extends ActionBarActivity implements View.OnClickListener {
    private Button ok;
    private Button delete;
    private Button edit;
    protected String title = null;
    protected String dosage = null;
    protected int dateDay = 0;
    protected int dateMonth = 0;
    protected int dateYear = 0;
    protected int expDay = 0;
    protected int expMonth = 0;
    protected int expYear = 0;
    protected String notes = null;
    protected String date = null;
    protected String expiration = null;
    protected long id = -1l;
    protected long profileId = -1l;
    Bundle b;
    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_vaccine);
        getSupportActionBar().setTitle("Vaccine Information");
        b = getIntent().getExtras();
        this.ok = (Button) findViewById(R.id.ok_button);
        this.ok.setOnClickListener(this);
        this.edit = (Button) findViewById(R.id.edit_button);
        this.edit.setOnClickListener(this);
        this.delete = (Button) findViewById(R.id.delete_button);
        this.delete.setOnClickListener(this);
        title = b.getString("title");
        id = b.getLong("id");
        TextView name = (TextView) findViewById(R.id.name);
        name.setText(title);
        TextView one = (TextView)findViewById(R.id.text1);
        TextView two = (TextView)findViewById(R.id.text2);
        TextView three = (TextView)findViewById(R.id.text3);
        TextView four = (TextView)findViewById(R.id.text4);
        TextView five = (TextView)findViewById(R.id.text5);
        TextView[] ids = {one, two, three, four, five};
        dateDay = b.getInt("dateDay");
        dateMonth = b.getInt("dateMonth");
        dateYear = b.getInt("dateYear");
        expMonth = b.getInt("expMonth");
        expYear = b.getInt("expYear");
        expDay = b.getInt("expDay");
        dosage = b.getString("dosage");
        notes = b.getString("notes");
        if(dateDay != 0 && dateMonth != 0 && dateYear != 0) {
            date = dateMonth + "/" + dateDay + "/" + dateYear;
        }
        if(expDay != 0 && expMonth != 0 && expDay != 0) {
            expiration = expMonth + "/" + expDay + "/" + expYear;
        }
        String[] optionals = {dosage, date, expiration, notes};
        setOptionalEntries(ids, optionals);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_information_vaccine, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void onClick(View v) {
        switch (v.getId()) {
            //what occurs when ok button clicked
            case R.id.ok_button:
                //Intent iNew = new Intent(this, Vaccines.class);
                //startActivity(iNew);
                finish();
                break;
            case R.id.edit_button:
                Intent iNew3 = new Intent(this, NewVaccine.class);
                b.putInt("type", 1);
                if(b != null) {
                    iNew3.putExtras(b);
                }
                startActivity(iNew3);
                finish();
                break;
            case R.id.delete_button:
                AlertDialog.Builder builder3 = new AlertDialog.Builder(InformationVaccine.this)
                        .setTitle("Are you sure you want to delete this profile?")
                        .setMessage("This action is permanent.")

                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                DBvaccineAdapter dbv = MainActivity.dbv;;
                                dbv.deleteVaccine(id);
                                Intent iNew2 = new Intent(InformationVaccine.this, Vaccines.class);
                                iNew2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(iNew2);
                                finish();

                            }
                        })
                        .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                builder3.show();
        }
    }
    public void setOptionalEntries(TextView[] ids, String[] optionals) {
        int j = 0;
        int d = 0;
        for(int i = 0; i < optionals.length; i++) {
            if(optionals[i] != null && !(optionals[i].equals(""))) {
                TextView set = ids[j];
                set.setVisibility(View.VISIBLE);
                if(optionals[i].equals(date) && d==0) {
                    set.setText("Date of shot: " + optionals[i]);
                    d=1;
                } else if(optionals[i].equals(expiration)) {
                    set.setText("Expiration Date: " + optionals[i]);
                } else if (optionals[i].equals(notes) && !(notes.equals(""))) {
                    set.setText("Notes: " + optionals[i]);
                } else if (optionals[i].equals(dosage) && !(dosage.equals(""))) {
                    set.setText("Dosage: "+ optionals[i]);
                }
                j++;
            }
        }
        for(int c = j; c < ids.length; c++) {
            TextView set = ids[j];
            set.setVisibility(View.INVISIBLE);

        }
    }
}
