package bklewis.caduceus;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class About extends ActionBarActivity implements AdapterView.OnItemClickListener {
    private DrawerLayout drawerLayout=null;
    private ActionBarDrawerToggle toggle=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about1);
        ListView drawer=(ListView)findViewById(R.id.drawer);

        drawer.setAdapter(new ArrayAdapter<String>(
                this,
                R.layout.drawer_row,
                getResources().getStringArray(R.array.drawer_rows)));
        drawer.setOnItemClickListener(this);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle=
                new ActionBarDrawerToggle(this, drawerLayout,
                        R.drawable.ic_drawer,
                        R.string.drawer_open,
                        R.string.drawer_close);
        drawerLayout.setDrawerListener(toggle);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_about, menu);
        return true;
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
            if (toggle.onOptionsItemSelected(item)) {
                return(true);
            }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onItemClick(AdapterView<?> listView, View row,
                            int position, long id) {
        if(position == 0) {
            Intent iNew = new Intent(this, MainActivity.class);
            iNew.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        } else if (position == 1) {
            Intent iNew = new Intent(this, Doctors.class);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        } else if(position == 2) {
            Intent iNew = new Intent(this, Profiles.class);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        } else if (position == 3) {
            Intent iNew = new Intent(this, Medicines.class);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        } else if (position == 4) {
            Intent iNew = new Intent(this, Vaccines.class);
            startActivity(iNew);
            this.overridePendingTransition(0, 0);
            finish();
        }
        drawerLayout.closeDrawers();
    }
}
