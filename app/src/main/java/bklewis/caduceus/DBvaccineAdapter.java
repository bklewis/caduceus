package bklewis.caduceus;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by bryce_000 on 4/2/2015.
 */
public class DBvaccineAdapter extends DBAdapter{

    protected static final String TAG = "DBvaccineAdapter";

    public DBvaccineAdapter(Context context){
        super(context);
    }

    // Create vaccine
    public long createVaccine(ItemVaccine vac){
        ContentValues args = new ContentValues();
        args.put(KEY_VACCINE_TITLE, vac.title);
        args.put(KEY_VACCINE_DOSAGE, vac.dosage);
        args.put(KEY_VACCINE_DATE_DAY, vac.dateDay);
        args.put(KEY_VACCINE_DATE_MONTH, vac.dateMonth);
        args.put(KEY_VACCINE_DATE_YEAR, vac.dateYear);
        args.put(KEY_VACCINE_EXP_DAY, vac.expDay);
        args.put(KEY_VACCINE_EXP_MONTH, vac.expMonth);
        args.put(KEY_VACCINE_EXP_YEAR, vac.expYear);
        args.put(KEY_VACCINE_NOTES, vac.notes);
        args.put(KEY_VACCINE_PROFILE_ID, vac.profileID);

        vac.id = db.insert(TABLE_VACCINES, null, args);
        // "New vaccine /"" + vac.title + "/" created"
        //Toast.makeText(context, "New vaccine " + vac.title + " created" , Toast.LENGTH_LONG).show();
        return vac.id;
    }

    // Read vaccine
    public ItemVaccine readVaccine(long rowID){
        ItemVaccine vac = null;
        //Log.i(TAG, "readVaccine ID=" + Long.toString(rowID));
        //Toast.makeText(context, "readVaccine ID=" + Long.toString(rowID), Toast.LENGTH_LONG).show();
        try {
            Cursor c = db.query(TABLE_VACCINES, KEYS_VACCINE, "_id = " + rowID, null, null, null, null, null);
            if (c == null) {
                //Toast.makeText(context, "readVaccine Null Cursor Error: The vaccine you tried to access could not be located in the database.", Toast.LENGTH_LONG).show();
                Log.i(TAG, "readVaccine Null Cursor Error: The vaccine you tried to access could not be located in the database.");
            } else {
                Log.i("TAG", "DO I GET HERE?");
                c.moveToFirst();
                Log.i("TAG", "moveToFirst");
                String title = c.getString(c.getColumnIndex(KEY_VACCINE_TITLE));
                Log.i("TAG", "Title");
                String dosage = c.getString(c.getColumnIndex(KEY_VACCINE_DOSAGE));
                Log.i("TAG", "Dosage");
                int dateDay = c.getInt(c.getColumnIndex(KEY_VACCINE_DATE_DAY));
                Log.i("TAG", "DateDay");
                int dateMonth = c.getInt(c.getColumnIndex(KEY_VACCINE_DATE_MONTH));
                Log.i("TAG", "DateMonth");
                int dateYear = c.getInt(c.getColumnIndex(KEY_VACCINE_DATE_YEAR));
                Log.i("TAG", "DateYear");
                int expDay = c.getInt(c.getColumnIndex(KEY_VACCINE_EXP_DAY));
                Log.i("TAG", "ExpDay");
                int expMonth = c.getInt(c.getColumnIndex(KEY_VACCINE_EXP_MONTH));
                Log.i("TAG", "ExpMonth");
                int expYear = c.getInt(c.getColumnIndex(KEY_VACCINE_EXP_YEAR));
                Log.i("TAG", "ExpYear");
                String notes = c.getString(c.getColumnIndex(KEY_VACCINE_NOTES));
                Log.i("TAG", "NOTES");
                long profileID = c.getLong(c.getColumnIndex(KEY_VACCINE_PROFILE_ID));
                Log.i("TAG", "PROFILEID");

                vac = new ItemVaccine(rowID, title, dosage, dateDay, dateMonth, dateYear, expDay, expMonth, expYear, notes, profileID);
                //Toast.makeText(context, "Successfully accessed in readVaccine vaccine=" + vac.title + " with id=" + vac.id, Toast.LENGTH_LONG).show();
                Log.i(TAG, "Successfully accessed in readVaccine vaccine=" + vac.title + " with id=" + vac.id);
                c.close();
            }
        } catch(Exception e){
            Log.i(TAG, e.getMessage());
            //Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
            //Toast.makeText(context, "Failed Cursor Retrieval in readVaccine: The vaccine you tried to access could not be located in the database.", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Failed Cursor Retrieval in readVaccine: The vaccine you tried to access could not be located in the database.");
        }
        return vac;
    }

    // Read vaccine
    public ItemVaccine readVacFromCursor(Cursor c){
        ItemVaccine vac = null;
        if (c == null){
            //Toast.makeText(context, "readVaccineFromCursor Null Cursor Error: The vaccine you tried to access could not be located in the database.", Toast.LENGTH_LONG).show();
            Log.i(TAG, "readVaccine Null Cursor Error: The vaccine you tried to access could not be located in the database.");
        }
        else {
            //c.moveToFirst();
            long id = c.getLong(c.getColumnIndex(KEY_ID));
            String title = c.getString(c.getColumnIndex(KEY_VACCINE_TITLE));
            String dosage = c.getString(c.getColumnIndex(KEY_VACCINE_DOSAGE));
            int dateDay = c.getInt(c.getColumnIndex(KEY_VACCINE_DATE_DAY));
            int dateMonth = c.getInt(c.getColumnIndex(KEY_VACCINE_DATE_MONTH));
            int dateYear = c.getInt(c.getColumnIndex(KEY_VACCINE_DATE_YEAR));
            int expDay = c.getInt(c.getColumnIndex(KEY_VACCINE_EXP_DAY));
            int expMonth = c.getInt(c.getColumnIndex(KEY_VACCINE_EXP_MONTH));
            int expYear = c.getInt(c.getColumnIndex(KEY_VACCINE_EXP_YEAR));
            String notes = c.getString(c.getColumnIndex(KEY_VACCINE_NOTES));
            long profileID = c.getLong(c.getColumnIndex(KEY_VACCINE_PROFILE_ID));

            vac = new ItemVaccine(id, title, dosage, dateDay, dateMonth, dateYear, expDay, expMonth, expYear, notes, profileID);
            //Toast.makeText(context, "Successfully accessed in readVaccineFromCursor vaccine=" + vac.title + " with id=" + vac.id, Toast.LENGTH_LONG).show();
            Log.i(TAG, "Successfully accessed in readVaccineFromCursor vaccine=" + vac.title + " with id=" + vac.id);
        }
        return vac;
    }

    // Update vaccine
    public boolean updateVaccine(ItemVaccine vac) {
        long rowId = vac.id;
        boolean updated = false;
        if (rowId == -1) {
            // create a new vaccine from argument
            createVaccine(vac);
            //Toast.makeText(context, "You just created a new vaccine " + vac.title + " with id = " + vac.id + " rather than updating a current vaccine!", Toast.LENGTH_LONG).show();
        } else {
            ContentValues args = new ContentValues();
            args.put(KEY_VACCINE_TITLE, vac.title);
            args.put(KEY_VACCINE_DOSAGE, vac.dosage);
            args.put(KEY_VACCINE_DATE_DAY, vac.dateDay);
            args.put(KEY_VACCINE_DATE_MONTH, vac.dateMonth);
            args.put(KEY_VACCINE_DATE_YEAR, vac.dateYear);
            args.put(KEY_VACCINE_EXP_DAY, vac.expDay);
            args.put(KEY_VACCINE_EXP_MONTH, vac.expMonth);
            args.put(KEY_VACCINE_EXP_YEAR, vac.expYear);
            args.put(KEY_VACCINE_NOTES, vac.notes);
            args.put(KEY_VACCINE_PROFILE_ID, vac.profileID);

            updated = db.update(TABLE_VACCINES, args, KEY_ID + "=" + rowId, null) != 0;
        }
        if (updated){
            //Toast.makeText(context, "Vaccine "+vac.title+" updated successfully", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Vaccine "+vac.title+" updated successfully");
            return true;
        } else {
            //Toast.makeText(context, "Vaccine "+vac.title+" update unsuccessful", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Vaccine "+vac.title+" update unsuccessful.");
            return false;
        }
    }

    // Delete vaccine
    public boolean deleteVaccine(ItemVaccine vac){
        boolean b = db.delete(TABLE_VACCINES, KEY_ID + "=" + vac.id, null) > 0;
        if (b){
            Toast.makeText(context, "Vaccine " + vac.title + " deleted successfully", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Vaccine " + vac.title + " with id=" + vac.id + " deleted successfully");
        } else {
            //Toast.makeText(context, "Vaccine delete unsuccessful", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Vaccine " + vac.title + " with id=" + vac.id + " delete unsuccessful");

        }
        return b;
    }

    public boolean deleteVaccine(long rowID){
        ItemVaccine vac = readVaccine(rowID);
        boolean b;
        if (vac == null){
           // Toast.makeText(context, "Attempted to delete null vaccine with id " + Long.toString(rowID), Toast.LENGTH_LONG).show();
            Log.i(TAG, "Attempted to delete null vaccine with id " + Long.toString(rowID));
            b = false;
        } else {
            b = db.delete(TABLE_VACCINES, KEY_ID + "=" + rowID, null) > 0;
            if (b) {
               // Toast.makeText(context, "Vaccine " + vac.title + "deleted successfully", Toast.LENGTH_LONG).show();
                Log.i(TAG, "Vaccine " + vac.title + " with id=" + vac.id + " deleted successfully");
            } else {
               // Toast.makeText(context, "Vaccine delete unsuccessful", Toast.LENGTH_LONG).show();
                Log.i(TAG, "Vaccine " + vac.title + " with id=" + vac.id + " delete unsuccessful");
            }
        }
        return b;
    }

    public ArrayList<ItemVaccine> getAllVaccines(){
        ArrayList<ItemVaccine> list = new ArrayList<ItemVaccine>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_VACCINES;

        //SQLiteDatabase db = this.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);
            try {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        ItemVaccine m = readVacFromCursor(cursor);
                        list.add(m);
                    } while (cursor.moveToNext());
                }
            } finally {
                try { cursor.close(); } catch (Exception ignore) {}
            }

        } catch(Exception e){
            //Toast.makeText(context, "There are currently no vaccines to be loaded from the database.", Toast.LENGTH_SHORT).show();
        }
        //finally {    try { db.close(); } catch (Exception ignore) {}}
        return list;
    }

    public ArrayList<ItemVaccine> getVaccineByProfile(long profileID) {
        ArrayList<ItemVaccine> list = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_VACCINES + "WHERE profile = " + profileID;

        String[] args = new String[] {Long.toString(profileID)};
        //SQLiteDatabase db = this.getReadableDatabase();
        try {
            //Cursor cursor = db.rawQuery(selectQuery, null);
            Cursor cursor = db.query(TABLE_VACCINES, KEYS_VACCINE, "profile = ?", args, null, null, null);
            try {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        ItemVaccine v = readVacFromCursor(cursor);
                        list.add(v);
                    } while (cursor.moveToNext());
                }
            } finally {
                try { cursor.close(); } catch (Exception ignore) {}
            }

        } catch(Exception e){
           // Toast.makeText(context, "There are no events to be loaded from the database currently.", Toast.LENGTH_SHORT).show();
        }
        //finally {    try { db.close(); } catch (Exception ignore) {}}
        return list;
    }

}
