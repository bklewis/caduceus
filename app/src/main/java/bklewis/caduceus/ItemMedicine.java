package bklewis.caduceus;

/**
 * Created by axmangel on 3/31/15.
 */

public class ItemMedicine {

    protected long id = -1L;
    protected String title;
    protected String dosage;
    protected String refillsLeft;
    //protected String nextRefill;
    protected int timeTakenHour;
    protected int timeTakenMinute;
    protected int notificationTime;
    //protected int notification_Refill;
    protected String prescriber;
    protected String description;
    protected String notes;
    protected long profileID = -1L;

    public ItemMedicine(long id, String title, String dosage, String refillsLeft, int timeTakenHour, int timeTakenMinute, int notificationTime, String prescriber, String description, String notes, long profileID){
        this.id = id;
        this.title = title;
        this.dosage = dosage;
        this.refillsLeft = refillsLeft;
        this.timeTakenHour = timeTakenHour;
        this.timeTakenMinute = timeTakenMinute;
        this.notificationTime = notificationTime;
        this.prescriber = prescriber;
        this.description = description;
        this.notes = notes;
        this.profileID = profileID;
    }
}
