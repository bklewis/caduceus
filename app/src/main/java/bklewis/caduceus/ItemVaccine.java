package bklewis.caduceus;

/**
 * Created by axmangel on 3/31/15.
 */
public class ItemVaccine {

    protected long id = -1L;
    protected String title;
    protected String dosage;
    protected int dateDay;
    protected int dateMonth;
    protected int dateYear;
    protected int expDay;
    protected int expMonth;
    protected int expYear;
    protected String notes;
    protected long profileID = -1L;

    public ItemVaccine(long id, String name, String dosage, int dateDay, int dateMonth, int dateYear, int expiresDay, int expiresMonth, int expiresYear, String notes, long profileID) {
        this.id = id;
        this.title = name;
        this.dosage = dosage;
        this.dateDay = dateDay;
        this.dateMonth = dateMonth;
        this.dateYear = dateYear;
        this.expDay = expiresDay;
        this.expMonth = expiresMonth;
        this.expYear = expiresYear;
        this.notes = notes;
        this.profileID = profileID;
    }
}
