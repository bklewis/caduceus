package bklewis.caduceus;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class Information extends ActionBarActivity implements View.OnClickListener {
    private Button ok;
    private Button delete;
    private Button edit;
    //required variables to be displayed
    int sHour;
    int sMinute;
    int eHour;
    int eMinute;
    int day;
    int year;
    int month;
    String name;
    String location;
    String notes;
    String color;
    Bundle b;
    Long id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);
        getSupportActionBar().setTitle("Event Information");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //get  info from bundle passed in
        b = getIntent().getExtras();
        name = b.getString("name");
        year = b.getInt("year");
        month = b.getInt("month") + 1;
        day = b.getInt("day");
        sHour = b.getInt("sHour");
        sMinute = b.getInt("sMinute");
        eHour = b.getInt("eHour");
        eMinute = b.getInt("eMinute");
        //optional variables to be displayed
        location = b.getString("location");
        notes = b.getString("notes");
        color = b.getString("color");
        id = b.getLong("id");
        id = b.getLong("profileID");
        String[] optionals = {location, notes};
        //create strings for text
        String startTime = timeCreator(sHour, sMinute);
        String endTime = timeCreator(eHour, eMinute);
        String date = "" + month +"/" + day+ "/" + year;
        //get textviews
        TextView title = (TextView) findViewById(R.id.name);
        TextView date1 = (TextView) findViewById(R.id.date);
        TextView start = (TextView) findViewById(R.id.startTime);
        TextView end = (TextView) findViewById(R.id.endTime);
        //set textviews
        title.setText(name);
        date1.setText("Event Date: " + date);
        start.setText("Start Time: " + startTime);
        end.setText("End Time: " + endTime);
        //enable button clicking
        this.ok = (Button) findViewById(R.id.ok_button);
        this.ok.setOnClickListener(this);
        this.edit = (Button) findViewById(R.id.edit_button);
        this.edit.setOnClickListener(this);
        this.delete = (Button) findViewById(R.id.delete_button);
        this.delete.setOnClickListener(this);
        TextView one = (TextView)findViewById(R.id.text5);
        TextView two = (TextView)findViewById(R.id.text6);
        TextView[] ids = {one, two};
        setOptionalEntries(ids, optionals);
    }

    public String timeCreator(int mSelectedHour,int mSelectedMinute) {
        String amPm;
        String hour;
        if (mSelectedHour > 12) {
            int tempHour = mSelectedHour - 12;
            hour = "" + tempHour;
            amPm = "PM";
        } else if (mSelectedHour == 12) {
            hour = "" + mSelectedHour;
            amPm = "PM";
        } else if (mSelectedHour > 0) {
            hour = ""+mSelectedHour;
            amPm = "AM";
        } else {
            hour = "12";
            amPm = "AM";
        }
        String minutes = (mSelectedMinute > 9) ?""+mSelectedMinute : "0"+mSelectedMinute;
        String timeString = "" + hour + ":" + minutes + " " + amPm;
        return timeString;
    }

    public void setOptionalEntries(TextView[] ids, String[] optionals) {
        int j = 0;
        for(int i = 0; i < optionals.length; i++) {
            if(optionals[i] != null) {
                TextView set = ids[j];
                set.setVisibility(View.VISIBLE);
                if(optionals[i].equals(notes)) {
                    set.setText("Notes: " + optionals[i]);
                } else {
                    set.setTextColor(Color.parseColor("#318CE7"));
                    set.setPaintFlags(set.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                    //set.setText("Email: "+ optionals[i]);
                    set.setText("Location: " + optionals[i]);
                }
                if(optionals[i].equals(location)) {
                    set.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            String loc = location.trim();
                            String uri = "http://maps.google.co.in/maps?q=" + loc;
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                            //intent.setData(Uri.parse(uri));
                            //intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                            startActivity(intent);

                        }
                    });
                }
                j++;
            }
        }
        String l = "" +j;
        Toast.makeText(Information.this, "Long pressed event: " + optionals[0] , Toast.LENGTH_SHORT).show();
        for(int c = j; c < ids.length; c++) {
            TextView set = ids[j];
            set.setVisibility(View.INVISIBLE);

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_information, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void onClick(View v) {
        switch (v.getId()) {
            //what occurs when ok button clicked
            case R.id.ok_button:
                Intent iNew = new Intent(this, MainActivity.class);
                startActivity(iNew);
                finish();
                break;
            case R.id.edit_button:
                Intent iNew3 = new Intent(this, NewEvent.class);
                Bundle bundle = new Bundle();
                bundle.putInt("type", 1);
                bundle.putString("name", name);
                bundle.putString("location", location);
                bundle.putString("notes", notes);
                bundle.putInt("sHour", sHour);
                bundle.putInt("sMinute", sMinute);
                bundle.putInt("eHour", eHour);
                bundle.putInt("eMinute", eMinute);
                bundle.putInt("month", month);
                bundle.putInt("year", year);
                bundle.putInt("day", day);
                bundle.putString("color", color);
                Long id = b.getLong("id");
                bundle.putLong("id", id);
                Long profileId = b.getLong("profileID");
                bundle.putLong("profileID", profileId);
                if(bundle != null) {
                    iNew3.putExtras(bundle);
                }
                startActivity(iNew3);
                finish();
                break;
            case R.id.delete_button:
                AlertDialog.Builder builder3 = new AlertDialog.Builder(Information.this)
                        .setTitle("Are you sure you want to delete this event?")
                        .setMessage("This action is permanent.")

                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                DBeventAdapter dbe = MainActivity.dbe;

                                Log.i("DOCDELETE", "getID");
                                long id = b.getLong("id");
                                Log.i("DOCDELETE", "going to try deleting");

                                dbe.deleteEvent(id);
                                Log.i("DOCDELETE", "doc deleted " + Long.toString(id));

                                Intent iNew2 = new Intent(Information.this, MainActivity.class);
                                Log.i("DOCDELETE", "intent created");
                                iNew2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

                                startActivity(iNew2);
                                Log.i("DOCDELETE", "activity started");

                                finish();

                            }
                        })
                        .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                builder3.show();
                break;

        }
    }

}
