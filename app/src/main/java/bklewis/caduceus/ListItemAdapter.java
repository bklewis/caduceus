package bklewis.caduceus;

/**
 * Created by bryce_000 on 4/21/2015.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;


/**
 * Created by bryce_000 on 4/15/2015.
 */
public class ListItemAdapter extends BaseAdapter {

    private ArrayList<ItemEvent> items;
    private Context context;
    MainActivity main;
    View view;


    public ListItemAdapter(ArrayList<ItemEvent> myList, Context context){
        this.items = myList;
        Collections.sort(items);
        this.context = context;
    }

    @Override
    public int getCount(){
        return items.size();
    }

    @Override
    public ItemEvent getItem(int position) {
        if (position < getCount()){
            return items.get(position);
        }else {
            throw new IllegalArgumentException("Invalid position: "
                    + String.valueOf(position));
        }
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).id; //Normally this would be the id of your data if it has an ID field
    }



    private class ViewHolder {
        TextView title;
        //TextView details;
        //TextView dayDate;
        //TextView dayOfWeek;
        //ImageView myImage;
        //LinearLayout square;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        View row = convertView; //The old view to reuse, if possible. Note: You should check that this view is non-null and of an appropriate type before using. If it is not possible to convert this view to display the correct data, this method can create a new view. Heterogeneous lists can specify their number of view types, so that this View is always of the right type
        if (row == null) {
            //If there is no old ConvertView to recycle we need to make a view from scratch
            LayoutInflater vi = (LayoutInflater)context.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.calendar_list_row, parent, false);

            //Set up Views in Viewholder so that we can store tbem as a single object
            holder = new ViewHolder();
            holder.title = (TextView)convertView.findViewById(R.id.eventTitle);
            //holder.details = (TextView)convertView.findViewById(R.id.eventDetails);
            //holder.dayDate = (TextView)convertView.findViewById(R.id.date);
            //holder.dayOfWeek = (TextView)convertView.findViewById(R.id.weekDay);
            //holder.square = (LinearLayout)convertView.findViewById(R.id.square);

            //holder.myImage = (ImageView)convertView.findViewById(R.id.imageView);
            convertView.setTag(holder);

            view = convertView;
        }
        else{
            //If we have already made a view we stored it in the holder with convertView.setTag(holder)
            holder = (ViewHolder) convertView.getTag();
        }



        //Get the person for this row and set UI to match it
        ItemEvent currentItem = items.get(position);

        holder.title.setText(currentItem.title);
        //holder.details.setText(details);
        //holder.dayDate.setText(Integer.toString(currentEvent.day));
        //holder.dayOfWeek.setText(currentEvent.weekDay);

        //holder.dayDate.setTextColor(col);
        //holder.dayOfWeek.setTextColor(col);


        //if(currentEvent.color.equals("event_color_1"))
        //int col = R.color.event_color_01;
        //holder.square.setBackgroundResource(col);

        //Set imageview to the image based on the number
        /*switch (events.getImageNumber()){
            case 0:
                holder.myImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_launcher));
                break;
            case 1:
                holder.myImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_launcher));
                break;
            case 2:
                holder.myImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_launcher));
                break;
        }*/
        //holder.myImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_launcher));

        //Save the person in the imageview via a tag so we can retrieve it
        //holder.myImage.setTag(currentEvent);
        //comment

        return convertView;
    }

}


