package bklewis.caduceus;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class NewDoctor extends ActionBarActivity implements View.OnClickListener {
    //buttons in activity
    private Button ok;
    private Button cancel;
    Bundle bundle;
    private String firstName = null;
    private String lastName = null;
    private String hospital = null;
    private String speciality = null;
    private String workPhone = null;
    private String personalPhone = null;
    private String fax= null;
    private String email = null;
    private String address = null;
    private String notes = null;
    private Long id = -1l;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_doctor);
        getSupportActionBar().setTitle("New Doctor Profile");
        //find buttons and set listener
        ok = (Button) findViewById(R.id.ok_button);
        cancel = (Button) findViewById(R.id.cancel_button);
        ok.setOnClickListener(this);
        cancel.setOnClickListener(this);
        //actionBar.setDisplayHomeAsUpEnabled(true);
        //actionBar.setHomeButtonEnabled(true);
        bundle = getIntent().getExtras();
        if(bundle.getInt("type") == 1) {
            getEdits();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_newdoctors, menu);
        return true;
    }



    @Override
    public void onClick(View v) {
        //set up cases for different buttons that are clicked
        switch (v.getId()) {
            case R.id.ok_button:
                getStrings();
                if(!(lastName.equals(""))) {
                    if(bundle.getInt("type") != 1) {
                        //creating event
                        ItemDoctor doctor = new ItemDoctor(-1l, firstName, lastName, speciality, workPhone, personalPhone, email,fax, hospital, address, notes);
                        Log.i("hospital", hospital);
                        DBdocAdapter dbd = MainActivity.dbd;
                        dbd.createDoc(doctor);
                    } else {
                        //updating event
                        Long id = bundle.getLong("id");
                        ItemDoctor doctor = new ItemDoctor(id, firstName, lastName, speciality, workPhone, personalPhone, email,fax, hospital, address, notes);
                        DBdocAdapter dbd = MainActivity.dbd;
                        dbd.updateDoc(doctor);
                    }

                    Intent iNew = new Intent(this, Doctors.class);
                    iNew.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(iNew);
                    this.overridePendingTransition(0, 0);
                    finish();
                    break;
                } else {
                    AlertDialog.Builder builder3 = new AlertDialog.Builder(NewDoctor.this)
                            .setTitle("Required Information Missing")
                            .setMessage("For your event to be saved, you need to enter the doctor's last name.")

                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                    builder3.show();
                }
                break;
            case R.id.cancel_button:
                AlertDialog.Builder builder3 = new AlertDialog.Builder(NewDoctor.this)
                        .setTitle("Are you sure you want to leave this page?")
                        .setMessage("If you leave this page, your information will not be saved.")

                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                Intent iNew2 = new Intent(NewDoctor.this, Doctors.class);
                                startActivity(iNew2);
                                overridePendingTransition(0, 0);
                                finish();

                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                builder3.show();
                break;
        }
    }
    public void getEdits() {
        id = bundle.getLong("id");
        firstName = bundle.getString("firstName");
        EditText firstNametitle = (EditText) findViewById(R.id.doctorFirstName);
        firstNametitle.setText(firstName);
        lastName = bundle.getString("lastName");
        EditText lastNametitle = (EditText) findViewById(R.id.doctorLastName);
        lastNametitle.setText(lastName);
        hospital = bundle.getString("hospital");
        if(hospital != null) {
            EditText hospitaltitle = (EditText) findViewById(R.id.doctorHospital);
            hospitaltitle.setText(hospital);
        }
        speciality = bundle.getString("specialty");
        if(speciality != null) {
            EditText specialityTitle = (EditText) findViewById(R.id.speciality);
            specialityTitle.setText(speciality);
        }
        personalPhone = bundle.getString("personalPhone");
        if(personalPhone != null) {
            EditText personal = (EditText) findViewById(R.id.personalPhone);
            personal.setText(personalPhone);
        }
        workPhone = bundle.getString("workPhone");
        if(workPhone != null) {
            EditText personal = (EditText) findViewById(R.id.workPhone);
            personal.setText(workPhone);
        }
        fax = bundle.getString("fax");
        if(fax != null) {
            EditText text = (EditText) findViewById(R.id.faxPhone);
            text.setText(fax);
        }
        email = bundle.getString("email");
        if(email != null) {
            EditText text = (EditText) findViewById(R.id.email);
            text.setText(email);
        }
        address = bundle.getString("address");
        if(address != null) {
            EditText text = (EditText) findViewById(R.id.address);
            text.setText(address);
        }
        notes = bundle.getString("notes");
        if(notes != null) {
            EditText text = (EditText) findViewById(R.id.notes);
            text.setText(notes);
        }

    }

    public void getStrings() {
        EditText text1 = (EditText) findViewById(R.id.doctorFirstName);
        firstName = text1.getText().toString();
        EditText text2 = (EditText) findViewById(R.id.doctorLastName);
        lastName = text2.getText().toString();
        EditText text3 = (EditText) findViewById(R.id.speciality);
        speciality = text3.getText().toString();
        EditText text4 = (EditText) findViewById(R.id.doctorHospital);
        hospital = text4.getText().toString();
        EditText text5 = (EditText) findViewById(R.id.workPhone);
        workPhone = text5.getText().toString();
        EditText text6 = (EditText) findViewById(R.id.personalPhone);
        personalPhone = text6.getText().toString();
        EditText text7 = (EditText) findViewById(R.id.faxPhone);
        fax = text7.getText().toString();
        EditText text8 = (EditText) findViewById(R.id.email);
        email = text8.getText().toString();
        EditText text9 = (EditText) findViewById(R.id.address);
        address = text9.getText().toString();
        EditText text10 = (EditText) findViewById(R.id.notes);
        notes = text10.getText().toString();


    }


}
